#!/bin/sh

tmp_start_issue() {
	ISSUE=$1
	if [ -z "$ISSUE" ]; then
		echo "Usage: tmp_start_issue XXX"
		return 1
	fi
	if [ $(expr length "$ISSUE") != "3" ]; then
		echo "Issue must be 3 digits"
	fi
	git checkout main
	git pull
	git checkout -b "issue-$ISSUE"
	cp content/template.txt "content/issues/issue-$ISSUE.md"
	git add .
	git commit -m "Start issue $ISSUE"
	git push -u origin HEAD
}

tmp_publish() {
	(cd /scratch/bec/ops &&
		ansible-playbook -i production all.yml --limit tmpdir --tags newsletter.tmpdir.org)
}
