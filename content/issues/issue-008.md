+++
title = "TMPDIR Weekly - #8, September 24th, 2021"
date = 2021-09-24
+++

Welcome to the 8th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Linux

The next Generation GNU profiler was
[announced in August to GNU binutils mailing lists](https://sourceware.org/pipermail/binutils/2021-August/117665.html).
This is a new tool covering profiler functionality and plugging gaps in existing
implementations. It can profile multithreaded programs and shared libraries as
well. The biggest change is that it can profile production binaries, which means
there is no need to build programs with -pg to get a good profile. This will
reduce the usage friction in a huge way. It supports C/C++ and Java. There was a
[good talk](https://www.youtube.com/watch?v=zUw0ZVXCwoM&t=2824s) at the Linux
Plumbers Conference GNU track on this.

The [Speedy PaSh shell compiler project](https://binpa.sh/) tries to speed up
POSIX compliant shell scripts by providing automatic parallelism to the script,
can be effective especially on multicore systems. Its been adopted by Linux
Foundation and thusly expects to receive more development and user time.

[`systemd-oomd`](https://www.freedesktop.org/software/systemd/man/systemd-oomd.html)
is a user space daemon to manage OOM conditions, it was done at facebook and has
been integrated into systemd about a year ago, its has been coming along nicely
ever since, there was a
[nice update](https://www.youtube.com/watch?v=ACkhSgmA7P8) on it at LPC.

A patch to reduce number of native dependencies
[has been posted](https://lists.openembedded.org/g/openembedded-core/message/156185?p=%2C%2C%2C20%2C0%2C0%2C0%3A%3Arecentpostdate%2Fsticky%2C%2Cpkgconfig%2C20%2C2%2C0%2C85739585)
to OpenEmbedded core mailing list. This patch should reduce amound of shared
state required to build packages as it decouples some of default dependencies
from native sysroot and should improve overall build times also. This however is
not all rosy as it has unveiled a lot of packages requiring these default
dependencies which would need to explicitly call them in the recipes. A common
overlooked dependency is pkgconfig. This is not expected to go into upcoming
October release but is more slotted for next LTS in April 2022.

---

# IoT

Continuing to experiement with NATS AuthN/AuthZ concepts and
[refine an approach](https://github.com/simpleiot/simpleiot/issues/268) to
Simple IoT Authentication.

When I compare the SIOT block diagram to the complexity of other IOT systems, I
conclude we must be doing something really right or really wrong.

![siot diagram](https://raw.githubusercontent.com/simpleiot/simpleiot/master/docs/images/simple-iot-nats-connects.jpg)

---

# Other

## [A couple articles on working with time series data](https://community.tmpdir.org/t/working-with-time-series-data/310).

There are so many good tools available, but we must understand the data we have,
and use techniques that accurately convey the information we need -- displaying
raw points is not always good enough.

## [Lead with documentation](https://community.tmpdir.org/t/lead-with-documentation/311)

As I’ve been refining my approach to work, a phrase has been sticking in my
mind:

Lead with documentation

What this means in practice is when I start working on anything that requires a
little up front thought, architecture, or planning, the first thing I do is open
up the documentation for the project and start writing. If
collaboration/discussion is required, I open a pull request to facilitate
discussion. However, the approach is still documented in the repo files, not in
the PR, Github issue, etc.

This is very different than how most people work, but I’m finding it enjoyable
and productive. It is taking effort to switch, as I still tend to put too much
documentation in Github issues. Old habits die hard and change takes time.

For this approach to be effective, documentation needs to be frictionless. For
me, this means Markdown – preferably in the same repo as the design/source
files. Then it is easy to edit code and docs in the same editor.

Some of the benefits to this approach:

1. when you are done with a task, the documentation is already done. Doing
   documentation after the fact is a rather distasteful task. However, if it is
   done before the task, it is very engaging.
1. it helps you think. As
   [Leslie Lamport says](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/leslie_lamport.pdf),
   To think, you have to write. If you’re thinking without writing, you only
   think you’re thinking.
1. documentation does not become stale, outdated, and useless.

## FOSS Engineering jobs

The Linux Foundation
[has published a job report](https://www.linuxfoundation.org/resources/publications/open-source-jobs-report-2021/)
which says that industry is looking for FOSS engineers more than ever before.

> Interest in staff who have experience contributing to  
> open source projects increased this year; 44% of hiring managers stated this
> to be the most important technical skill or experience when looking for new
> team members, higher than any other.

Participating in OSS projects and building up your Github profile seems like a
prudent move in this technology age.

---

# Quote for the week

There are only two kinds of opensource projects, the most hated ones and ones no
one uses.

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
