+++
title = "TMPDIR Weekly - #20, January 15, 2022"
date = 2022-01-15
+++

Welcome to the 20th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Yoe
[v2021.12 - "masai"](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.12)
is released. This released includes ready to boot images for Raspberry PI4, and
BeagleBone Black platforms. This is also the first release that is completely
automated -- we push a tag, and then Github actions kicks off a build on our CI
server, builds, tests, and uploads images.

Reading through this
[patchset](https://lore.kernel.org/lkml/Ydm7ReZWQPrbIugn@gmail.com/) -- seems to
be a header cleanup which results in whopping 88% compile time improvement for
linux kernel. Its not mentioned but I am sure Ingo might have used
clang-include-fixer tool to support the cleanup effort.

---

# IoT

Simple IoT
[v0.0.41 is out](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.41)
with support for NATs over websockets for upstream support. This is very useful
when using SIOT on networks where all outgoing connections are blocked except
for HTTP(S). This also makes handling TLS certs easier on servers as this can
all be relegated to a webserver like Caddy.

---

# Podcast

We published two new episodes:

## [#13 -- A Day in the Life of a Go Developer](https://tmpdir.org/013/)

In this episode, Cliff describes how he develops Go code. We cover tooling,
testing, deployment, and all the things that make Go development a great
experience.

## [#14 -- Do You Own Your Platform?](https://tmpdir.org/014/)

In this episode we describe what a platform is and the importance of owning it.

Available on your favorite podcast [platform](https://tmpdir.org/about/).

---

# GitPLM

We recently added support for subassemblies in GitPLM where you can roll up
multiple BOMs into one BOM for purchasing parts. Get the
[latest release](https://github.com/git-plm/gitplm/releases).

---

# Other

There are some
[new alternatives](https://community.tmpdir.org/t/new-alternatives-to-well-known-unix-linux-commands/392)
to well known unix/linux commands -- check them out!

---

# Quote for the week

> The most amazing achievement of the computer software industry is its
> continuing cancellation of the steady and staggering gains made by the
> computer hardware industry. -- Henry Petroski

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
