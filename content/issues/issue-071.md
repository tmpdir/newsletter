+++
title = "📆 TMPDIR Weekly - #71, Yocto communication now easier, Finding your IoT data"
date = 2024-03-30
+++

Hello,

Welcome to the 71st issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Good decisions pay dividends. Bad decisions create debt. -- Jonathan Graham

---

# Yocto project 5.0 ( Scarthgap ) Update

Upcoming spring yocto release ( scarthgap ) is only few weeks away, it has
shaped up well. A good set of package upgrades and tooling upgrades is in the
offering, It is tested using 6.6 LTS kernel with a plan to support another LTS
during it lifespan of 4 years. A new machine `genericarm64` is being added which
will help in testing a baseline image on aarch64 based systems which support ARM
SystemReady IR specs. This is quite significant development for bringing up
aarch64 based platforms on yocto as it will be able to extend the well tested
`genericarm64` machine. A call for help in adding documentation and release
notes is sent out to community mailing lists.

# Yocto mailing list updates

Communications is hard and organising it is even harder, for long there has been
catch-all mailing list yocto@lists.yoctoproject.org, being used for patches new
user questions, release updates and other activities. A recent effort has been
to make this more focussed discussion group, as a result two new mailing lists
have been created

- [yocto-patches@lists.yoctoproject.org](https://lists.yoctoproject.org/g/yocto-patches):
  This is for patches for Yocto layers and components that do not have their own
  list
- [yocto-status@lists.yoctoproject.org](https://lists.yoctoproject.org/g/yocto-status):
  This will have weekly status and other high level messages, so if you are
  interested in these activities this might be good to subscribe

As a result, yocto mailing list is for new users to ask questions and help and
bring up discussion points.

---

# Making Graph Data Easier to Find and Organize

Is setting up graphs in Grafana dashboards tedious because it is difficult to
find the data you are looking for? Simple IoT Tags
([recently added](https://github.com/simpleiot/simpleiot/releases/tag/v0.15.0))
makes it easier to correlate a SIOT node with graph data.

![graph](https://community.tmpdir.org/uploads/default/original/2X/1/1be44ba4bf7e0ebc2b871b7301ab8fc21541f3a6.jpeg)

See:

- [SIOT Graphing Docs](https://docs.simpleiot.org/docs/user/graphing.html)
- [SIOT Database Docs](https://docs.simpleiot.org/docs/user/database.html)

Demo video:

[![Video](https://img.youtube.com/vi/f4D6MS6bq5U/0.jpg)](https://youtu.be/f4D6MS6bq5U)

---

# Does your Kiosk solution handle network interrupts?

Recent improvements to the
[Yoe Kiosk Browser](https://github.com/YoeDistro/yoe-kiosk-browser) improves the
user response when there are network interrupts.

Demo video:

[![Video](https://img.youtube.com/vi/XBgSKoWdppE/0.jpg)](https://youtu.be/XBgSKoWdppE)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
