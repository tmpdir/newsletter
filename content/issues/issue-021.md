+++
title = "TMPDIR Weekly - #21, January 23, 2022"
date = 2022-01-23
+++

Welcome to the 21st issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

openssh recently dropped support for `ssh-rsa`, which is used by older systems
running Dropbear. You'll need
[this workaround](https://community.tmpdir.org/t/ssh-into-old-embedded-linux-systems-running-dropbear/395)
to ssh into them.

The Yoe Distribution has started using a recently added feature in
Yocto/Openembedded that helps ensure offline builds will work. Now bitbake
disables network access by default on all tasks except the `do_fetch` task.
Fetching tasks are therefore the only place where network download is allowed by
default. This has found several packages failing to build since they were
silently downloading extra sources or other artifacts during compile or
configure tasks. Most of the packages in core layers have already been
adddressed. However, this can still be an issue for recipes using Go, where Go
modules download content as well which may not be the invoked during `do_fetch`.
This can be enabled for these recipes by adding the following in the compile
task:

```
do_compile[network] = "1"
```

LLVM/Clang is now defaulting to use DWARF5 debug format, Yoe distro has been
using DWARF5 since 2021, now this change is available upstream as well. More
details are
[here](https://www.phoronix.com/scan.php?page=news_item&px=LLVM-Clang-DWARFv5-Default).

---

# Other

Typora, an excellent Markdown editor,
[has reached 1.0!](https://community.tmpdir.org/t/typora-markdown-editor/77/2).

Some
[notes on SQLite](https://community.tmpdir.org/t/richard-hipp-sqlite-etc/389)
and the author behind it -- definitely a storage technology you should consider
for embedded/edge systems.

[Interesting podcast](https://community.tmpdir.org/t/rust-notes/360/2) where
Ryan Dahl talks about Deno.

---

# Quote for the week

> All variation stems from design ... and the first design is never the
> simplest. -- Sandy Munro

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
