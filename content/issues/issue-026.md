+++
title = "TMPDIR Weekly - #26, February 26th, 2022"
date = 2022-02-26
+++

Welcome to the 26th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Python PEP-0517 in the Yocto Project

[PEP-0517](https://www.python.org/dev/peps/pep-0517/) has been around for few
years and adds a build-system independent format for source trees. As part of
this, `setup.py` files go away in python modules, and wheels will be preferred
where eggs was used in past. This change in core means that python packages in
other OE layers need to adjust to use the new format. This is a good change in
general, however many Yocto Python module recipes will need porting to the new
packaging requirements. There is open call for help sent to OE developers
mailing list to help with fixing these packages, currently there are
approximately
[50 packages](https://errors.yoctoproject.org/Errors/Build/141804/?page=2) in
meta-python failing

## Project Zero Metrics

[Project zero](https://googleprojectzero.blogspot.com/), a team of security
researchers at Google who study zero-day vulnerabilities in the hardware and
software systems, has published a recent report detailing on how various
companies and projects are doing. The report has seen encouraging trends in
reduction of time to fix in past three years from 80 days to 52 days, but there
still is room for improvement The full report is
[here](https://googleprojectzero.blogspot.com/2022/02/a-walk-through-project-zero-metrics.html).

## Linux kernel and C11 standard

A recent
[proposal](https://lwn.net/ml/linux-kernel/20220217184829.1991035-1-jakobkoschel@gmail.com/)
to make speculative safe list iterators spurred a discussion which highlighted
the need to use a C99 feature such that an iterator must be declared outside the
list traversing macro loops. So essentially "declare variable inside loops in
local context" feature could make it easy. This perhaps will motivate the move
to use either C99 or newer C standard for kernel programming. We might see
kernel adopt C11 in 5.18+ who knows !!

---

# Other

With some technology advances, things that used to be hard
[are now easy](https://community.tmpdir.org/t/things-that-used-to-be-hard-are-now-easy/431).

On the
[topic of documentation site generation](https://community.tmpdir.org/t/do-we-need-a-new-static-site-generator-for-project-documentation/429/2),
[mdBook](https://rust-lang.github.io/mdBook/) looks like a great tool for
project documentation, and appears to be very compatible with extracting
documentation from Git repos. It is part of the Rust project, but is generally
useful, and [attempts](https://github.com/rust-lang/mdBook/pull/1759) are being
made to make the playground feature useful for other languages.

---

# Quote for the week

> In programming, if someone tells you “you’re overcomplicating it,” they’re
> either 10 steps behind you or 10 steps ahead of you. -- Andrew Clark ( React
> Core Team )

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
