+++
title = "TMPDIR Weekly - #30, March 26, 2022"
date = 2022-03-26
+++

Welcome to the 30th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Linux load averages

Brenden has an
[excellent post](https://www.brendangregg.com/blog/2017-08-08/linux-load-averages.html)
on Linux load averages. This article goes into the history of uninterruptible
tasks and how its meaning has evolved and how it has impacted the metric itself.
Understanding Linux Load averages has always been challanging, especially when
it comes to multicore CPUs, this is also explained well. A few other tools are
also presented that may be more useful in some situations.

## Rolling ubuntu with 'Rolling Rhino'

It has been [announced](https://rollingrhinoremix.github.io/docs-install/) that
the Rolling Rhino remix edition of Ubuntu will be a rolling release. General
Ubuntu releases are time based where a major release is made every 6 months
(April/October) complemented with LTS release every couple of years. It has
served well so far but distributions such as Arch Linux have been finding
success with a rolling release model so this model is extending to more
distributions. It also means that a devops mindset is gaining traction in
distribution building.

## Support of the Nezha Allwinner D1

A BSP port for the Allwinnner D1 (RISCV based) has been summitted and is under
review on [meta-riscv](https://github.com/riscv/meta-riscv/pull/327). This adds
another SBC possibility for RISCV-64 architectures.

---

# Other

[How to set up](https://community.tmpdir.org/t/do-you-need-a-simple-way-to-share-large-files/455)
a simple file server using Caddy to share large files.

A [minor GitPLM](https://community.tmpdir.org/t/gitplm-releases/365/10) that
organizes the BOM output columns a little better for importing into various
distributor BOM tools (like Mouser). We've also updated the
[documentation](https://github.com/git-plm/gitplm#readme) and polished some
thoughts on a
[part number format](https://github.com/git-plm/gitplm/blob/main/partnumbers.md).

The CGo-free port of SQLite
[now supports riscv64](https://community.tmpdir.org/t/cgo-free-port-of-sqlite-for-go/294/2).

Some
[rambings about Business Contracts](http://bec-systems.com/site/1563/business-contracts).
-- cb

Some [notes on](https://community.tmpdir.org/t/go-workspaces/456) Go 1.18
workspaces.

---

# Quote for the week

> The first 90 percent of the code accounts for the first 90 percent of the
> development time. The remaining 10 percent of the code accounts for the other
> 90 percent of the development time. -- Tom Cargill

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
