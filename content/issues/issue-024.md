+++
title = "TMPDIR Weekly - #24, February 12th, 2022"
date = 2022-02-12
+++

Welcome to the 24th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Gstreamer major release 1.20 is out. It is one of favourite multimedia
frameworks for Linux based Operating Systems. This change brings a load of
[changes](https://gstreamer.freedesktop.org/releases/1.20/)

Linux 4.4 release has come to an end with
[this](https://lwn.net/Articles/883684/) announcement. 4.4 was a long term
kernel and deployed on many embedded Linux devices including Android.

Restartable sequences are new in glibc
[2.35](https://sourceware.org/pipermail/libc-alpha/2022-February/136040.html)
introduced to speed up per-CPU data structures from userspace without locking.
This has existed in kernel for long since 4.15 release but there was no
userspace APIs till now Rich Felker (musl author) came with interesting
[shortcomings](https://sourceware.org/pipermail/libc-alpha/2022-February/135971.html)
in the glibc implementation,

---

# IoT

Currently
[testing Home Assistant](https://community.tmpdir.org/t/home-assistant-notes/415).
Thus far, it seems pretty slick and works well with
[Shelly devices](https://community.tmpdir.org/t/shelly-home-automation-products/259).

To get more familiar with Shelly products, I did a few small projects around
home:

- [Using Shelly Bulbs for an Alarm Clock](https://community.tmpdir.org/t/using-a-shelly-bulb-for-an-alarm-clock/423)
- [Remote garage door opener](https://community.tmpdir.org/t/opening-a-garage-door-using-a-shelly-relay/422)

Overall, Shelly products work great -- they are open and easy to use. My only
complaint is the cloud and phone apps are a little fussy and have trouble
discovering devices that are already on the network as well as attaching the
devices to a WiFi network. Hopefully the newer Bluetooth enabled (Plus/Pro)
devices will work better in this regard.

The Make IoT Simple Podcast from Shelly
[describes how their products](https://community.tmpdir.org/t/shelly-home-automation-products/259/3?u=cbrake)
are different than most IoT products today.

---

# Quote for the week

> When you catch a sideways glance in the mirror of your mind’s eye, and ask
> yourself why you do this software thing, and whether it really matters,
> "**who**" tends to make a satisfying answer. "**Why**" only rarely does, and
> only for a time. Serve. Other people. Other devs if you like. Now. Life’s just
> better that way. -
> [Kyle Mitchell](https://writing.kemitchell.com/2022/02/12/Devs-Use-Closed-Software.html)

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
