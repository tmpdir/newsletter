+++
title = "TMPDIR Weekly - #32, April 16th, 2022"
date = 2022-04-16
+++

Welcome to the 32nd issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Beagleboard DT improvements

The Beagleboard team
[continues to improve](https://community.tmpdir.org/t/beaglebone-and-device-tree/410/3?u=cbrake)
their device tree story and now support Linux 5.10.

## Static Analyzer with GCC 12

GCC has added static analyser capabilities beginning with gcc-10 and have been
improving slowly. Recent patches for the GCC 12 pre-release were posted for
OpenEmbedded, and we do see additional warnings being reported in few packages
which are clearly due to the new static analyser capabilities. Building the
Linux kernel with `-fanalyzer` enabled could be a good exercise for
OpenEmbedded. This
[article](https://developers.redhat.com/articles/2022/04/12/state-static-analysis-gcc-12-compiler)
delves into more details.

# Support of the Nezha Allwinner D1 in OE

Recently a [pull request](https://github.com/riscv/meta-riscv/pull/327) has been
merged to add support for Nezha based SBC to meta-riscv. The Nezha SBC is
implemented using the RISCV64 ISA and has been getting good amount of RISCV
upstreaming work into various projects. We now have beagleV, Unmatched, and
Nezha Allwinner D1 BSPs available in meta-riscv along with QEMU BSPs which are
available in OE-Core.

---

# Other

Grafana is a very useful tool to view data over time. Here are
[some tips](https://community.tmpdir.org/t/using-annotations-in-grafana/477) on
how to use annotations in Grafana.

The Changelog has done a
[podcast episode with Brian Kernighan](https://community.tmpdir.org/t/podcast-review-brian-kernighan-unix-c-awk-ampl-and-go-programming/284/2?u=cbrake).
Always interesting to learn from history and why some things may have been so
successful.

Why are blogs so effective? In
[this thread](https://community.tmpdir.org/t/why-are-blogs-so-effective/479), we
explore this topic and reference several cases where others have stopped
producing a newsletter and podcast to focus on other forms of media.

---

# Quote for the week

> Debugging is anticipated with distaste, performed with reluctance, and bragged
> about forever. -- Boston Computer Museum

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
