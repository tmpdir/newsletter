+++
title = "📆 TMPDIR Weekly - #81, Yoe 2024.06 release 🚀, Looking for a better way to build/test C?"
date = 2024-07-15
+++

Hello,

Welcome to the 81st issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> The degree to which a person can grow is directly proportional to the amount
> of truth he can accept about himself without running away. - Leland Val Van De
> Wall

---

# Looking for a better way to build/test C code?

Building and testing C/C++ code is fraught with complexity. Typically you have a
tool like CMake that generates Make files, which are then processed by Make.
Meson is similar in that it generates build files and a backend system
(typically Ninja) is used to perform the actual compilation. Testing requires
adding a 3rd party test framework. As we gain experience with languages like Go,
Rust, and now Zig, all this seems rather pointless. Why does it have to be this
complex?

[Zig](https://ziglang.org/) is a new language that has the ability to build C
code, as well as Zig code. Zig is interesting for many reasons but one reason is
the Zig language is used in build files, is run at compile instead of using
macros, and or course is used to generate runtime code for the application. One language for everything.

I started a [new project](https://github.com/YoeDistro/mgr) to experiment with
this. Testing and cross-compilation is next. Time will tell if this is a better
option. Thoughts? Discuss
[here](https://community.tmpdir.org/t/yoe-mgr-an-exercise-in-zig/1348)

# Yoe 2024.06 release 🚀

[June release of Yoe Distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/39)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components.

New project templated added for Jetson-agx-orin-devkit and BeagleV-Ahead

Other changes

- GCC 14.1 porting of recipes continues, in some recipe
  -Wincompatible-pointer-types is disabled for now.
- Libtool was upgraded to 2.5, which contains lot of Cross compilation aids
  which OE carried for many years. Several recipes were adjusted to buld with
  libtool 2.5
- QT6 recipes upgraded to 6.7.3
- Fix kernel build for Jetson orin-nano with GCC-14
- Clang is upgraded to 18.1.8, latest minor releases in LLVM-18 series
- Golang compiler is upgraded to 1.22.4

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
