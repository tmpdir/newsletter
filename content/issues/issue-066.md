+++
title = "📆 TMPDIR Weekly - #66, C++ in the Kernel, GitPLM parts, Living on the edge"
date = 2024-01-12
+++

Hello,

Welcome to the 66th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Health lies in action, and so it graces youth. To be busy is the secret of
> grace, and half the secret of content. Let us ask the gods not for
> possessions, but for things to do; happiness is in making things rather than
> in consuming them. -- Will Durant

---

# Linux

[C++ in Linux Kernel](https://lore.kernel.org/lkml/CAEg-Je_P6-3PWNxM4HCzcgM=H4Y1vmywaM3gbwK0gAe0UVoZGw@mail.gmail.com/T/#mdf3ab39bf12002c23586bf1c56b68cf6a93e8593)
An almost 6 year old thread got revived on Linux kernel mailing lists. This
thread is basically about letting C++ be an option for kernel code. In the past,
there has been resistance to allowing C++ in kernel, this post urges to
reconsider the situation in current times. The case for Macros vs inline
functions, and onboarding on new kernel programmers, which is supported by a lot
of responses to that thread are interesting. The Linux Kernel does a bit of
meta-programming using macros, which could also be done using templates in C++
improving debugging and make the 32 bit user-space type to 64bit kernel type
handling easier. The learning curve from C to C++ is not as steep as C to Rust,
and the Kernel has recently added rust support. I think allowing a subset of C++
(kernel C++) could be a good thing for Linux kernel.

---

# [GitPLM Parts](https://github.com/git-plm/parts)

GitPLM is experiment in using the KiCad database feature.

Maintaining eCAD parts libraries is a lot of work. Are you:

- tired of the tedious work of maintaining tradition KiCad libraries or manually
  adding MFG information to each part in a design?
- duplicating symbols in KiCad just because a parameter needs changed?
- duplicating information in your parts database and in schematic symbols?
- duplicating a lot of work in each design and occasionally make mistakes like
  specifying the wrong MPN?
- afraid of of the complexity of setting up a database for KiCad libraries?
- having trouble tracking who made what changes made in a parts database?
- struggling to find a CAD library process that will scale to large teams?

The GitPLM Parts project is a collection of best practices that allows you to:

- easily set to a database driven parts libraries without a central database and
  connection (Git friendly).
- leverage a common library of parts across multiple designs.
- easily specify part parameters in table format.
- not duplicate part parameters.
- leverage the standard KiCad symbol library for most common parts.
- easy add variants with just a line in a CSV file.
- track all database changes.
- scale to multiple designers.

[Check it out ...](https://github.com/git-plm/parts)

---

# [Discourse, living on the edge](https://community.tmpdir.org/t/discourse-living-on-the-edge/1222)

We've been running the TMPDIR community site for several (3.5) years now with no
significant problems. Perhaps every month I'll get an email something like this:

> Hooray, a new version of [Discourse](https://www.discourse.org) is available!
>
> Your version: 3.2.0.beta4-dev New version: **3.2.0.beta4**
>
> - Upgrade using our easy
>   **[one-click browser upgrade](https://community.tmpdir.org/admin/upgrade)**
> - See what’s new in the
>   [release notes](https://meta.discourse.org/tag/release-notes) or view the
>   [raw GitHub changelog](https://github.com/discourse/discourse/commits/main)
> - Visit [meta.discourse.org](https://meta.discourse.org) for news, discussion,
>   and support for Discourse

I was curious why Discourse always wants me to run "beta" or worse yet,
"beta-dev" software. The following threads shed some light:

https://meta.discourse.org/t/is-discourse-always-in-beta/198215

https://meta.discourse.org/t/why-does-discourse-always-install-beta-versions-by-default/264400

Some quotes:

> Our nomenclature is a bit different than other software companies, but what it
> means when we release a _beta_ is we’re releasing a new incremental version.
> We’ve said, “That’s enough changes for now. Let’s notify sites about new
> updates.”
>
> So for us, a _beta_ is a minor version bump, and a _version_ is a major
> version bump. They’re checkpoints we give ourselves to celebrate the work
> we’ve done. We tend to release two major versions a year, but it all depends
> on feature development and the like. We’re not really into fake deadlines.
>
> ### Regarding the branches
>
> Stable/beta are not necessarily any more “stable” than tests-passed. It’s more
> the idea that the bugs are known. With tests-passed there may be new bugs
> introduced then fixed a few commits later.
>
> Tests-passed is not much different than most other software releases out
> there, which usually release small changes every two weeks. We commit new
> changes almost daily instead, and they’re available via tests-passed.

> We run tests-passed in production on our hosting. It’s 100% meant for
> production sites.

So for Discourse, their definition of "production ready" is "tests pass." Stable
release numbers are largely irrelevant anymore. This is the "new" best practice
in software development but requires some investment in testing and thinking a
little differently.

Ask yourself the question, if your tests pass, are you confident enough to
deploy it to production without any manual fiddling or second-guessing? If not,
then perhaps a little more work in testing is in order. If something breaks,
write a test that detects it so it will not happen again, and move on. This is
the way to move fast.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
