+++
title = "📆 TMPDIR Weekly - #70, Yoe release, Dev workflow/productivity"
date = 2024-03-04
+++

Hello,

Welcome to the 70th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Competency is bigger than "can they do it". Competency is "how they do it."
> Can is clean and binary, how is messy and analog. -- Jason Fried

---

# Linux

# [🎙️Developer Workflow and Productivity](https://tmpdir.org/028/)

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss workflow ideas that can help you improve your
developer productivity.

Outline:

- How to find the right people?
- How to accelerate the rate of development?
- How to be more efficient?
- How will AI impact me?
- How do we adapt to change?

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).

# Yoe 2024.02 release 🚀

[February release of Yoe](https://community.tmpdir.org/t/yoe-distribution-releases/239/35)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components. This release is coming a month or so before next yocto LTS release
scheduled for End of April, this release therefore has major components upgrades
e.g. glibc 2.39, binutils 2.42, OpenSSH 9.6p1, OpenSSL 3.2.1, mesa 24.x linux
yocto 6.6.18 kernel,

RISCV package tests ( ptests ) images have received quite a bit of fixes where
they are now runnable on QEMU machines.

`meta-elm` has been removed from yoe distro default layer setup, it was needed
for simpleIOT frontend, which is now pre-packaged into simpleIOT build into
yocto.

`meta-clang` has received Yocto Compatible Certification.

Per-recipe ptest support which allows ptest enabled recipes to build and run a
minimal image with just the package and its ptests, has been added for few more
meta-openembedded layers e.g. meta-networking, meta-multimedia,
meta-filesystems. This should allow runtime tests to be performed in CI runs and
thereby improve the recipe quality and maintain it over development in future.

master branches are in soft-freeze status in preparation for Yocto 5 LTS release
and stabilizing for it, the next release 2024.03 will contain less amount of
major changes.

---

# [Think Horses, not Zebras (Part 2)](https://bec-systems.com/2233/think-horses-not-zebras-part-2/)

There is a [popular quote](<https://en.wikipedia.org/wiki/Zebra_(medicine)>) in
medical circles:

When you hear hoofbeats, think of horses, not zebras. — Dr. Theodore Woodward

I recently
[posted several debugging](https://bec-systems.com/2138/think-horses-not-zebras/)
experiences where it was beneficial to examine simple scenarios before complex
ones. Here are a few more …

[_read more ..._](https://bec-systems.com/2233/think-horses-not-zebras-part-2/)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
