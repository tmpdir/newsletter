+++
title = "📆 TMPDIR Weekly - #64, What is new in the Yoe Distribution, FFMpeg in LVGL"
date = 2023-12-16
+++

Hello,

Welcome to the 64th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Live as if you were to die tomorrow. Learn as if you were to live forever. --
> Mahatma Gandhi

---

# 🎙️[What Is New in the Yoe Distribution](https://tmpdir.org/025/)

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss the latest features in the Yoe Distribution that can
help you build products more efficiently using Yocto.

- New platforms:
  - Nvidia Jetson Orin Nano
  - VisionFive2 RISCV64 SBC
- Yoe Projects - a better way to configure your project
- Qt 6.6
- switch to LLD linker
- ptests can not be run inside Docker

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).

---

# [Using FFMpeg in LVGL to decode videos](https://youtu.be/3TqKS11__wc)

This video demonstrates how to use FFMpeg in LVGL to decode and play a video.

LVGL is a lightweight graphics library originally designed for microcontrollers,
but can also be used in Linux systems. It might be a good option for developers
who are coming from a C programming background.

[![image-20231216110529205](https://i.ytimg.com/an_webp/3TqKS11__wc/mqdefault_6s.webp?du=3000&sqp=CMzurKwG&rs=AOn4CLAda-YtIL2plBgWvbmeSEeLf5svPA)](https://youtu.be/3TqKS11__wc)

# Nvidia Jetson support in Yoe Distro

Progress on supporting Nvidia Jetson Orin Nano reference platform has made some
progress. Offline SD card creation `dosdcard` and also flashing SD card Image
[flashing to NVME SSD card](https://community.tmpdir.org/t/nvidia-jetson-orin-nano-devkit-notes/1079/10?u=khem)
is also work in progress and changes are in progress using `doflash` is working
an
[documented](https://community.tmpdir.org/t/nvidia-jetson-orin-nano-devkit-notes/1079/9?u=khem.)
for upstreaming.

# [Simple IoT v-1.14.3 and v0.14.4 releases](https://github.com/simpleiot/simpleiot/releases)

- UI: display unknown nodes as raw type and points
- UI: add raw view button to node expanded view. This allows us to view the raw
  points in any node which is useful for debugging and development. (see
  [docs](https://docs.simpleiot.org/docs/user/ui.html#raw-node-view) for more
  information)
- UI: in node raw view, you can now edit/add/delete points (#676)
- UI: add custom node types

Video Demo:

[![raw edit video](https://i.ytimg.com/vi/SoXYVS6vT2o/hqdefault.jpg?sqp=-oaymwE2CNACELwBSFXyq4qpAygIARUAAIhCGAFwAcABBvABAfgB_gmAAtAFigIMCAAQARgfIFsocjAP&rs=AOn4CLDXpZCDmx8HYT1CErBq0Y2YI4QG7Q)](https://youtu.be/SoXYVS6vT2o)

# Simple IoT and Yoe-kiosk-browser in Alpine Linux

Simple IoT was upgraded to 0.14.1 in alpine linux ports and yoe-kiosk-browser
was accepted as new package in `testing` feeds.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
