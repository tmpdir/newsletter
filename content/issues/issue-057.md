+++
title = "📆 TMPDIR Weekly - #57, Yoe/SIOT Releases🚀, The hard parts of IoT"
date = 2023-06-15
+++

Hello,

Welcome to the 57th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Relying on complex tools to manage and build your system is going to hurt the
> end-users. [...] "If you try to hide the complexity of the system, you'll end
> up with a more complex system". Layers of abstraction that serve to hide
> internals are never a good thing. Instead, the internals should be designed in
> a way such that they NEED no hiding. — Aaron Griffin (previous Arch Linux
> leader)

---

# Yoe 2023.05 release 🚀

The latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/25),
is available. This has many changes which are expected when trunk opens for
development for different layers. The Clang compiler is upgraded to 16.0.4 which
is a bugfix release in 16.x series. GCC has been upgraded to 13.1.1 -- this is a
major release of gcc which happens once a year. The Go compiler is upgraded to
1.20.4. Busybox 1.36.1 is also part of this release.

One impactful change is that OLDEST_KERNEL is now set to 5.15 by default,
therefore the BSPs which are using older than 5.15 kernel needs to make a note
of this. Since this impacts glibc and kernel interface, it's important that this
is set to at least the version of the kernel the BSP is using. e.g. if a machine
is using 5.4 kernel, it might enable additional sycalls on glibc which may fail
during runtime as they may not be available in 5.4 kernel .e.g. systemd on ARM64
machines fails

```
[    7.157399] systemd[1]: Failed to create /init.scope control group: Function not implemented
[    7.167850] systemd[1]: Failed to allocate manager object: Function not implemented
[!!!!!!] Failed to allocate manager object.
```

To fix this in Yoe based projects, the project specific `config.conf` can be
used to set it

```diff
diff --git a/sources/meta-yoe/conf/projects/coral-dev/config.conf b/sources/meta-yoe/conf/projects/coral-dev/config.conf
index e729bf9..bac850a 100644
--- a/sources/meta-yoe/conf/projects/coral-dev/config.conf
+++ b/sources/meta-yoe/conf/projects/coral-dev/config.conf
@@ -37,6 +37,9 @@ DEFAULTTUNE:virtclass-multilib-lib32 = "armv7athf-neon"
 # Use GNU objcopy for kernel with clang
 OBJCOPY:pn-linux-fslc-lts:toolchain-clang = "${HOST_PREFIX}objcopy"

+# OE has moved to demand 5.15 minimum kernel but we are at 5.4 for coral-dev
+OLDEST_KERNEL = "5.4"
+
 # Use gcc for coral u-boot
 TOOLCHAIN:pn-u-boot-coral = "gcc"
```

---

# Simple IoT 🚀

There have been a number of Simple IoT
[releases](https://github.com/simpleiot/simpleiot/releases) to fix issues with
Shelly device support, improvements for high-rate Serial MCU data, and fixes for
the Rule Condition Schedule support.

---

# [The hard parts of IoT Systems](https://community.tmpdir.org/t/the-hard-parts-of-iot-systems/1068)

By far, the hardest part of IoT systems is that they are distributed systems,
which typically include a browser, cloud, and edge instances.

![iot parts](https://community.tmpdir.org/uploads/default/original/2X/1/197e15246c34b8a0ef13882e25fb4d6c6a458a9c.png)

These are all distinct and geographically separated computing systems connected
by networks. And distributed systems
[are difficult to get right](https://community.tmpdir.org/t/fallacies-of-distrbuted-computing/110).

The 2nd hardest part of IoT systems is
[lifecycle management](https://community.tmpdir.org/t/life-cycle-management-is-hard/835).
When you are interacting with the real world, things come and go. Devices are
added and removed. Humans may add/remove entities through configuration changes.
Processes may also automatically add or remove entities. And through all this,
everything must keep working, processes must be created and cleaned up, and
leaks cannot occur. Lifecycle management typically involves a good bit of
concurrency, and like distributed systems,
[concurrency is also hard](https://community.tmpdir.org/t/the-problem-with-threads/678).

Recently, lifecycle management in Simple IoT
[was improved](https://github.com/simpleiot/simpleiot/pull/556) while adding
support for
[Shelly IoT devices](https://docs.simpleiot.org/docs/user/shelly.html). The
Shelly management client periodically sends out mDNS requests looking for new
devices. This in turn creates new nodes. The SIOT Client Manager then
instantiates new clients for new nodes as they are created. However, this was
crashing and was recently fixed. Because the Client Manager manages the
lifecycle for all clients, all clients now benefit from this bug fix (and there
are now 17 or so clients).

In SIOT, we try to do the hard things once:

- Distributed: use NATS and simple/common/granular
  [data structures](https://docs.simpleiot.org/docs/ref/data.html?highlight=data%20struct#data-structures).
- Lifecycle: Everything
  [is a Client](https://docs.simpleiot.org/docs/ref/client.html) and the Client
  Manager manages the lifecycle.

Abstractions are useful as they reduce the number of times we need to write and
debug hard stuff. They establish patterns that are easier to read and reduce
errors. But for abstractions to be useful, we must identify the hard parts, and
prioritize these. There are tradeoffs. As an example, in every feature you add
to a system, would you rather:

1. Write a little boilerplate?
1. Deal with distributed, lifecycle, and concurrency issues?

As an example, in SIOT, we transfer and store most data using a single point
datastructure. This requires some boilerplate to convert everything into a point
before sending it and deserializing points into useful structs. But the benefit
is the synchronization and transport layers never have to change when we add new
features. The hard part is stable.

As an example, in SIOT, we transfer and store most data using a single point
data structure. This requires some boilerplate to convert everything into a
point before sending it and deserializing points into useful structs. But the
benefit is the synchronization and transport layers never have to change when we
add new features. The hard part is stable.

Another example is typed languages – typically these require a little more
typing, but prevent many run-time bugs and save a lot of time when it comes to
refactoring. In this case, preventing run-time bugs and code maintenance are the
hard parts.

Many developers shun boilerplate and other distasteful aspects of programming,
but compared to distributed and concurrency challenges, boilerplate is easy. Too
often I think we view abstraction as a way to reduce typing (boilerplate)
instead of simplifying the hard parts. Again, often there are often tradeoffs --
chose wisely.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
