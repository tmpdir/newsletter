+++
title = "TMPDIR Weekly - #5, August 28th, 2021"
date = 2021-08-28
+++

Welcome to the 5th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

The 3.4 release of the Yocto Project will have a new overrides syntax. As an
example, the following `DISTRO_FEATURES_remove` will be replaced with
`DISTRO_FEATURES:remove`

Rust support has been added to OpenEmbeeded/Yocto Core layer. This support is
migrating from meta-rust layer into core layer and will be helpful in getting
ever increasing rust support in opensource components addressed at core level.
There has been long standing issue with librsvg which required rust and is a
core component for rendering SVG onto to cairo surface.

Glibc 2.34 has merged several libraries into libc.so this includes libpthread.so
libdl.so etc. much like what musl has done from its inception. While this change
will help in long run, it has caused some unwanted issues in yocto project which
uses its own tools like pseudo to emulate super user like conditions ( fakeroot
) and unique way of supporting modern toolchains on ancient distributions by
using buildtools tarball which contains needed shims to run latest OE on such
distributions. These tools work closely with system C library and provide a shim
C library to overcome version differences, with structural changes in glibc
packaging this needed extra work in these areas.

Yocto project provides specific tune files to oprimize builds for a given
processor or architecture. This has been restructures to better organize these
files by architectures. Some BSP layers have to change the machine
configurations to use the files from new locations.

Yocto project has started making changes to support zstd compression as core
element of the build infrastructure which means that now zstd and zstd tools is
expected to be installed on build hosts. This will pave way for enabling
compression in shared state eventually, speeding up shared state use in builds
and reducing build time.

---

## IoT

Focus this past week in Simple IoT has been on fixing performance issues. We
recently deployed SIOT in a building automation project and the system was
becoming unresponsive. Various metrics were added to instrument how long it took
to process points and associated operations. Eventually I learned that a Windows
machine used to host a InfluxDB instance (used as a historian) was set to sleep
after 30m of inactivity. This caused InfluxDB calls in a NATS handler to block
for long periods of time. Since
[NATS dispatches messages](https://docs.nats.io/developing-with-nats/receiving/async)
serially, everything got backed up bringing the system to a crawl. Along the
way, I learned how the NATS and InfluxDB client libraries buffer and batch data
-- pretty neat stuff. As a result, there are some
[good performance improvements](https://github.com/simpleiot/simpleiot/pull/246)
that will be merged soon.

---

## Other

- Go 1.17 has been released, improving go modules detailed notes are
  [here.](https://golang.org/doc/go1.17)
- [Discussions](https://community.tmpdir.org/t/discussion-on-generics-and-the-overuse-of-channels-etc-in-go/293/2)
  on generics, architecture, and other Go topics.
- [CGo-free port](https://community.tmpdir.org/t/cgo-free-port-of-sqlite-for-go/294)
  of SQLite for Go and a C to Go compiler.
- An
  [excellent presentation](https://community.tmpdir.org/t/go-performance-and-testing-tools/292)
  on using some of the Go tools to optimize performance.
- [Backup data, not systems.](https://community.tmpdir.org/t/backups-are-for-data-not-system-files/291)

---

## Quote for the week

_The best way to complain is to make things -- James Murphy_

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
