+++
title = "📆 TMPDIR Weekly - #63, Yoe Release, Why OSS?"
date = 2023-12-04
+++

Hello,

Welcome to the 63rd issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Technology is best when it brings people together -- Matt Mullenweg

---

# Yoe 2023.11 release 🚀

The latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/32)
is available now. It has incremental updates some of which are worth noting from
Yoe distro's point of view. QT6 is upgraded to 6.6.2 and clang compiler is
upgrade to 17.0.5, ODROID-C4 project is refreshed and fixed to work with latest
Yoe. A major problem was corrupted SD cards from wic images which was fixed --
it was an issue due to dropping `IMAGE_NAME_SUFFIX` in core layer. `PE` and `PR`
are dropped from `/usr/src/debug` paths which should improve reuse. Systemd
`tmpfiles.d` files are put under `noarch_libdir` across multiple packages.
busyboxinit based Yoe profile was fixed to boot complex images e.g.
`yoe-kiosk-image`. `recipetool` can now create recipes for packages written in
Go language. A new project to build for Nvidia Orin Nano platform is added. This
should expand yoe-distro to support Nvidia jetson based systems in Yoe distro.

---

# [Why Open Source for Product Development](https://bec-systems.com/2014/why-open-source-for-product-development/)

One of the paradoxes in product development is Open Source. How can you
personally or as a company benefit from participating in Open Source projects?
Why should you share your great ideas and code? How can you build a business or
a career by giving things away? How can an open-source project be a reliable
supplier without contractual guarantees? Most successful technology companies
today participate in open-source projects. Why do they do this?

Open source is difficult to fully understand and much has been said about it.
There are many approaches to licensing and funding. Many question the equity and
sustainability. Many get bogged down in ethical, moral, and political arguments.
There are messy problems that don’t seem to have good answers. But we need to go
deeper and examine the core issues of why companies are turning to open-source.

We will consider several product development perspectives:

Our motivation is to produce something of value that society can use and benefit
from. Software technology is advancing at a rapid rate of change. Technology in
modern systems is increasingly complex. How can we maintain some level of
control over the resources we use to ensure they will meet our needs? And how
does organizational culture impact our ability to deal with these issues and
build modern systems?

![product dev perspectives](https://bec-systems.com/wp-content/uploads/2023/10/image.png)

[Read More >>](https://bec-systems.com/2014/why-open-source-for-product-development/)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
