+++
title = "TMPDIR Weekly - #29, March 19, 2022"
date = 2022-03-19
+++

Welcome to the 29th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Yocto release bumps major version to 4.0

The upcoming yocto project release will be a LTS release, and since it has many
breaking changes (support for inclusive language, override syntax separator
change, etc), its apt to call it
[4.0](https://git.yoctoproject.org/poky/commit/?id=c2416fd91c6eb85e1bea18b961a1d0fa5a56320a).

Bitbake is also
[bumping](https://git.openembedded.org/bitbake/commit/?id=9a13bf8e20b1841ec99281d45be4c4fc1118438c)
its major release to 2.0

## Understanding Linux Load Averages

If you are interested to dissect load averages and objectively understand what
they represent
[this](https://community.tmpdir.org/t/understanding-linux-load-averages/450)
article, gives key insights.

---

# GitPLM

A minor
[bug fix release](https://community.tmpdir.org/t/gitplm-releases/365/9?u=cbrake)
for GitPLM.

---

# Other

KiCad design rules
[modify copper pours](https://community.tmpdir.org/t/kicad-design-rules-modify-copper-pours/449).
This is very useful for high voltage designs.

Go 1.18 is [now released](https://community.tmpdir.org/t/go-releases/233/3).
Generics, 20% improvement for ARM64, etc – good stuff.

[EditorConfig](https://community.tmpdir.org/t/unified-editor-configs/447) is an
effort to solve age old problem of uniform coding style in a project. It allows
developers to use their favorite editors and yet comply to coding conventions a
project might use.

Arch Linux
[is 20 years old](https://community.tmpdir.org/t/arch-linux-is-20-years-old/448).
We've used this for a number of years now and it works really well.

---

# Quote for the week

> When you are stuck in a traffic jam with a Porsche, all you do is burn more
> gas in idle. Scalability is about building wider roads, not about building
> faster cars. - Steve Swartz

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
