+++
title = "📆 TMPDIR Weekly - #59, Yoe release, GUI Platforms for Embedded Linux"
date = 2023-08-05
+++

Hello,

Welcome to the 59th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

The most valuable asset in the software industry is the synthesis of programming
skill and deep context in the business problem domain, in one skull. -- Denis
Čahuk

---

# Yoe 2023.07 release 🚀

The latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/27)
is available now. This release contains GCC 13.2 upgrade, which is a bugfix only
release in GCC 13 release series. Aarch64 now uses
`-mbranch-protection=standard` for all packages, this enables both Arm Pointer
Authentication (PAC) & Branch Target:work Enablement (BTI) this should improve
security but hardening code from Return Oriented Programming (ROP) attacks.
Autoconf is upgraded to 2.72c, which should help configuring packages using
64-bit `time_t` on 32bit architectures. Testing is improved for glibc and rust.

# QT6 in Yoe

Starting with the 2022.06 release, Yoe has been migrating to using QT6, A new
image `yoe-kiosk-image` is added which packages
[yoe-kiosk-browser](https://github.com/YoeDistro/yoe-kiosk-browser) which is
based on QT6Webengine and is optimized for Web based UI's. The migration to use
QT6 will be ongoing for 2-3 releases. CI/CD has started using QT6 images as
well. `yoe-kiosk-image` will work on arm/arm64/x86/x86_64 architectures since
these are supported architectures for QT6Webengine

# Yoe/SimpleIoT Screenshots with various GUI platforms

We are continuing to improve the integration of Yoe, yoe-kiosk-browser, and
Simple IoT on touchscreen devices where the system boots and shows a browser UI
with the Simple IoT UI.

Below are some recent screenshots.

## Boot splash

![bootup](https://community.tmpdir.org/uploads/default/original/2X/5/5baf45986aece11b4485dbc59613cd31939aec68.jpeg)

## Weston/Wayland

Weston is a good choice if you need to run multiple GUI applications at the same
time and need a window manager.

![wayland](https://community.tmpdir.org/uploads/default/original/2X/3/3baf74de13b25bd1632aedd723582489c12df1e4.jpeg)

## EGLFS

![eglfs](https://community.tmpdir.org/uploads/default/original/2X/c/c766973387d070fdadfe861b7e2e52f5aa9fdf54.jpeg)

EGLFS is a good choice if you want to run a single GUI application full screen.
We sometimes call this Kiosk mode.

This page provides a good overview of the various UI options for embedded Linux
devices:

[Qt for Embedded Linux](https://doc.qt.io/qt-6/embedded-linux.html)

## Selecting a GUI platform

With Yoe, it is simple to select the GUI platform using the
[`YOE_PROFILE`](https://github.com/YoeDistro/yoe-distro/blob/master/docs/yoe-profile.md)
variable:

```
YOE_PROFILE = "yoe-glibc-systemd-eglfs"
YOE_PROFILE = "yoe-glibc-systemd-wayland"
YOE_PROFILE = "yoe-glibc-systemd-x11"
```

Yoe does the hard work of setting up all the necessary options to make each of
these profiles work.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
