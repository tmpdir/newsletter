+++
title = "TMPDIR Weekly - #9, October 2nd, 2021"
date = 2021-10-02
+++

Welcome to the 9th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Linux

The Yoe Distribution
[v2021.09 is now released](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.09)
and includes many upstream changes.

LLVM 13.0 was released this week, it added fortran (flang) support and has
official
[binaries](https://github.com/llvm/llvm-project/releases/tag/llvmorg-13.0.0).

---

# IoT

Having deployed a number of IoT systems running Go applications on Embedded
Linux,
[here are a few thoughts](https://community.tmpdir.org/t/the-pure-go-experience/319)
about the benefits of this approach.

---

# Other

The TMPDIR Handbook is now [available online](https://handbook.tmpdir.org).
Having had good success with [Zola](https://www.getzola.org/) on
[other sites](https://docs.simpleiot.org/), converting to this format makes
sense and provides the information in a format that is easy to reference.

---

# Quote for the week

The height of sophistication is simplicity. — Clare Boothe Luce

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
