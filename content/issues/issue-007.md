+++
title = "TMPDIR Weekly - #7, September 18th, 2021"
date = 2021-09-18
+++

Welcome to the 7th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Linux

Kernel [is trying to enable](https://github.com/simpleiot/simpleiot/issues/268)
`-Werror` by default starting 5.15+ and while it is a good poor man's static
analyzer that comes for free with compiler, it is not a consistent beast with
different compiler versions you may have to fix a lot of code to make compiler
happy, secondly different compiler's have different level of tolerance, so now
increasingly folks are using clang to compile the kernel along with gcc, this
could be quite a bit of work but its a step in right directions.

OpenSSL 3.0
[has been released](https://www.openssl.org/blog/blog/2021/09/07/OpenSSL3.Final/)
after 3 years of development with over 7500 commits and has seen an uptick in
contributions compared to previous releases. It does say that its a major
release and introduces new FIPS module and the license has been changed to
Apache-2.0 which could be a good change and foster more contribution and
adoption it also says that its not fully backward compatible, thats a bit
worrisome.

A
[new discussion](https://lists.openembedded.org/g/openembedded-architecture/topic/disruptive_changes_and_the/85488159?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,85488159)
on Next Yocto LTS ( 3.5 ) with quite a few disruptive changes is started on
OpenEmbedded architecture mailing list.

---

# IoT

Mostly just small changes in Simple IoT in recent weeks. Also thinking through
some major architectural issues such as if we should
[change the point data structure](https://github.com/simpleiot/simpleiot/issues/254),
and
[how to handle authorization](https://github.com/simpleiot/simpleiot/issues/268)
so that users/devices only have access to their own data.

---

# Other

As Arch Linux fans, we are interested in distributions that build on Arch.
[Garuda Linux](https://community.tmpdir.org/t/garuda-linux/306) looks
interesting and showcases a lot of neat Linux teachnology.

---

# Quote for the week

For a successful technology, reality must take precedence over public relations,
for nature cannot be fooled. -- Richard Feynman, Rogers Commission Report (1986)

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
