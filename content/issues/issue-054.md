+++
title = "📆 TMPDIR Weekly - #54, LLVM/Clang 16, Key to Extensibility"
date = 2023-03-25
+++

Hello,

Welcome to the 54th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

We've recently switched from Tinyletter to ConvertKit for sending this
newsletter. ConvertKit is configured to watch the RSS feed on our
[static site](https://newsletter.tmpdir.org/) and automatically send a
newsletter when we push new content to the site.

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Premature optimization is the root of all evil (or at least most of it) in
> programming. -–Donald Knuth

---

# Linux

## [LLVM/Clang 16.0.0](https://discourse.llvm.org/t/llvm-16-0-0-release/69326)

LLVM 16 is a major release, Clang has been pushing the envelope for modernizing
C/C++ usage and in this effort, a bunch of warnings have been turned into
errors. `-Wimplicit-function-declaration` and `-Wimplicit-int` warnings now
default to an error as well as `-Wincompatible-function-pointer-types`. Packages
authors need to pay attention to fix these diagnostics, especially with
[autoconf](https://git.savannah.gnu.org/gitweb/?p=autoconf.git;a=commit;h=bf5a75953b6d504f0405b1ca33b039b8dd39eef4)
default tests which emit test functions that do not compile with clang 16. These
issues have been fixed in this release cycle which would be fine for builds
regenerating configure scripts during build. The Yoe distribution will be
switching to using clang 16.0.0 in the upcoming 2023.03 release.

---

# IoT

## Simple IoT long-term testing

We now have a month of test metrics collected from a handful of systems running
Simple IoT:

![metrics](https://community.tmpdir.org/uploads/default/original/2X/1/16c4214c83b9ed3acd327a311704d99e88fa34a5.png)

Things seem stable so far.

---

# Other

## The key to extensibility

... is loosely coupled design. What does loosely coupled mean? Lots of things
but a few ideas:

- You can change one part without affecting another part
- There are simple/standard ways to interface between parts

There are many examples, but few:

- Go [interfaces](https://dave.cheney.net/2016/08/20/solid-go-design)
- Simple IoT's [client model](https://docs.simpleiot.org/docs/ref/client.html)
- Message-bus based systems
- SQL Databases
- REST APIs

And examples that are not:

- SOAP APIs
- OOP Inheritance

Sometimes coupling is needed to get performance. But otherwise, strive for
loosely coupled designs.

"Loose coupling" may sound free and undisciplined, but it's the opposite -- it
is the result of careful design, refactoring, and
[constraints](https://community.tmpdir.org/tag/constraints).

See our
[community discussion](https://community.tmpdir.org/t/the-key-to-extensibility/980)
for more examples.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
[Subscribe Here](https://tmpdir.ck.page/196d1fb480)
