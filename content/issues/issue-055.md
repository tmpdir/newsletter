+++
title = "📆 TMPDIR Weekly - #55, Yoe 2023.03 release, Zephyr 🚀"

date = 2023-04-08
+++

Hello,

Welcome to the 55th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Problems that remain persistently insoluble should always be suspected as
> questions asked in the wrong way. --Alan Watts

---

# [Yoe 2023.03 release](https://github.com/YoeDistro/yoe-distro/releases/tag/2023.03) 🚀

This release has some major updates, e.g. base clang compiler is updated to the
16.0.1 release which is a major release; therefore, it is flagging issues in a
lot of old code, which required fixing quite a few packages. OpenSSL has been
updated to 3.1.

The Yoe updater keeps getting better -- it can now support both tmpfs and
persistent /data overlay and uses xz compressed ext4 rootfs for updates to
reduce the size of update. We now have a `yoe-installer-image` image that
creates a SD card installer which would provision the eMMC on boot
automatically. This will help installing/reinstalling factory images. Support
for the VAR-SOM-MX8 symphony evaulation kit is added to the updater, which
should make it easy to port it to MX8 based SOM products.

Systemd is upgraded to 253.1. It brings basic support for loongarch64
architecture but there is no machine support for it in yoe yet.

binutils now defaults to preferring DT_RUNPATH and is compiled with
--enable-new-dtags. This should help tighten up security on systems with shared
libs a bit.

# [Zephyr](https://community.tmpdir.org/t/zephyr-notes/1009)

Zephyr is a small RTOS for connected and resource-constrained embedded systems
and typically runs on an MCU. We just
[started experiementing](https://community.tmpdir.org/t/zephyr-notes/1009) with
it on a STM32H7 MCU, and so far are very pleased with it. It is a game changer
in the MCU space -- especially as MCUs become more capable and connected.

With these capabilities, I think Zephyr based systems will start to displace
some Embedded Linux devices at the edge.

Zephyr has a really nice shell:

![shell](https://community.tmpdir.org/uploads/default/original/2X/2/27959c8f23223d63aeb55f20bf11ebce559b3f4b.png)

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
