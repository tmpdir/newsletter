+++
title = "TMPDIR Weekly - #14, November 6th, 2021"
date = 2021-11-06
+++

Welcome to the 14th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Linux 5.15 has been released and while going through changes it seems MIPS
support is almost done, there were very few patches and even in past few year
main contributor has been Loongson and now they have dumped MIPS for their own
new architecture called
[loongarch](https://loongson.github.io/LoongArch-Documentation/README-EN.html).
This is an indicator that when deciding on new SOCs, one should pay attention to
SOC CPU architecture and if it is not ARM, RISCV, or x86 then take a double
look.

A nice summary of changes is
[provided by CNX Software](https://www.cnx-software.com/2021/11/02/linux-5-15-lts-release-main-changes-arm-risc-v-and-mips-architectures/)

Yocto’s git fetcher falls back to using `master` for branchname if it is not
specified by the user in the recipe when defining `SRC_URI`. Recently a lot of
OSS packages have started moving away from calling the default branch master;
therefore, it is no longer a common default. Yocto now warns about such usage:

```
Parsing recipes...
WARNING:  /home/jenkins/oe/world/yoe/sources/meta-freescale/recipes-bsp/mxsldr/mxsldr_git.bb:
URL: git://git.denx.de/mxsldr.git does not set any branch parameter.
The future default branch used by tools and repositories is uncertain  and we will
therefore soon require this is set in all git urls.
```

There is detailed discussion about this on oe-architecture mailing list
[openembedded-architecture@lists.openembedded.org | Default branch names in git urls](https://lists.openembedded.org/g/openembedded-architecture/topic/default_branch_names_in_git/86675927?p=,,,20,0,0,0::recentpostdate/sticky,,,20,2,0,86675927,previd=1635900279548163299,nextid=1622050867607689111&previd=1635900279548163299&nextid=1622050867607689111)

Yoe release 2021.10 (karakul) is now available. See
[release notes](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.10)
for a list of changes. This is the first Yoe release that includes Yocto 3.4,
which has many changes.

Github has
[stopped supporting](https://github.blog/2021-09-01-improving-git-protocol-security-github/)
the unencrypted 'git' protocol, which may impact Yocto recipes. We have fixed up
a few of [our recipes](https://github.com/YoeDistro/yoe-distro/pull/611/files).

---

# IoT

In Simple IoT we are
[finishing ADR-1](https://github.com/simpleiot/simpleiot/blob/cbrake/adr-1/docs/adr/1-consider-changing-point-data-type.md)
and have been pulling
[CRDT theory](https://community.tmpdir.org/t/crdts-conflict-free-replicated-data-types/348/4)
into the discussion. This has been very helpful as it gives us a vocabulary and
framework for thinking about data synchronization problems. It is shaping up
nicely and will give us a good data framework going forward. Still a lot of
things to explore (like issues with distributed clocks), but we're making
progress. It is neat when you discover others are using similar approaches to
solve problems.

With the availability and low cost of networks, the future in computing is
distributed. Distributed data requires a different mindset than the central
database mentality. For many distributed systems, it works best to handle the
data synchronization and merging at the application layer. And with these new
methods (like CRDTs) the result is more simple and reliable. When things get
simpler, this is a good indication we are heading in the right direction. -- cb

---

# Other

Sometimes its handy to know what the compiler might be doing with a code
snippet. At times when I am working on upgrading a compiler or debugging a
performance issue, I want to look at different versions of compiler to generate
code for a given source and compare. This tool called the
[Compiler Explorer](https://godbolt.org/) is priceless for me. I believe it
could also help folks who are learning computer programming -- especially at the
system level. It supports all the well known programming languages e.g. C/C++,
Go, Rust, JAVA, Python etc. You can choose difference compilers and different
versions. You can also choose different cross compiler architectures e.g.
ARM/x86/RISCV/PPC/MIPS etc. -- kr

---

# Quote for the week

> Simplicity is a prerequisite for reliability. -- Edsger W. Dijkstra

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
