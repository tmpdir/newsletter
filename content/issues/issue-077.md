+++
title = "📆 TMPDIR Weekly - #77, Embedded Linux Challenges, Update systems, and more! 🎙️"
date = 2024-05-24
+++

Hello,

Welcome to the 77th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Programmers are always surrounded by complexity; we cannot avoid it. Our
> applications are complex because we are ambitious to use our computers in ever
> more sophisticated ways. Programming is complex because of the large number of
> conflicting objectives for each of our programming projects. If our basic
> tool, the language in which we design and code our programs, is also
> complicated, the language itself becomes part of the problem rather than part
> of its solution. --
> [Tony Hoare](https://community.tmpdir.org/t/the-emperors-old-clothes/344)

---

# 🎙️Discussion with Drew Moseley from Toradex

We had a great discussion with Drew Moseley from Toradex about Embedded Linux,
System Update mechanism, challenges, and the future.

[Listen here](https://tmpdir.org/031/) or on your favorite
[podcast platform](https://tmpdir.org/about/).

---

# RISCV meetup

Bay Area RISC-V Group resumed its activities after a long hiatus with a local
meetup
[held this week](https://www.meetup.com/bay-area-riscv-meetup/events/301075929/)
There was a good presence and enthusiasm in the meetup, Khem presented a
refreshed OverView on
[state of OpenSource Software on RISC-V](https://drive.google.com/file/d/1ZKAsC2lcFFaFwBswN9AwgnpuyiKlRKdW/view?usp=drive_link),
second talk was on
[gcc-14 release: what's latest and greatest for RISC-V](https://drive.google.com/file/d/1F6lX9AayOAZMG6TWkfP5okORZxlU5Amu/view?usp=drive_link)
The big highlight on GCC-14 release for RISC-V is Autovectorization works well
there are other interesting optimizations e.g. constant synthesis and
sign-extention elimination improvements, overall this could result in
significant improvements on code generation and optimizations. SPEC shows
improved performance over GCC-14 in almost all benchmarks.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
