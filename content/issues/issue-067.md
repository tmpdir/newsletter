+++
title = "📆 TMPDIR Weekly - #67, Riding the JetStream"
date = 2024-01-27
+++

Hello,

Welcome to the 67th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> If you don't continually improve, you soon lose the ability to do so …

---

# Upcoming and new major Core component upgrades in Yocto 5.0

Upcoming yocto release 5.0 in April 2024, is also a LTS release, therefore it
becomes important to note various components going into it. Recently, systemd
255 was
[merged](https://git.yoctoproject.org/poky/commit/?id=74f0fd144f73cf658f5018ab8f4140a4a2dc99b7).
Python 3.12 is also
[merged](https://git.yoctoproject.org/poky/commit/?id=78e30d940d1a931c65a14d864494d92f0889035e).

There are other components close to release which are very likely to make into
the LTS release.

Glibc [2.39](https://sourceware.org/glibc/wiki/Release/2.39) is in freeze mode
in release process as it is due to be released in early February. Clang-18 is
branched out and scheduled to release in
[early march](https://discourse.llvm.org/t/llvm-18-release-schedule/76175).
Binutils, which provides low-level tools for compiler code generation is
[branched out](https://sourceware.org/pipermail/binutils/2024-January/131859.html)
for upcoming 2.42 release.

For C/C++ compiler, GCC-14 is major annual release upcoming in Early May. It is
done with features and currently fixing regressions and documentation. Sadly, it
will be missed in yocto 5.0 release.

---

# RFC: Riding the JetStream

The initial [experiments](https://github.com/simpleiot/nats-exp) with NATS
JetStream have been successful, so working on an
[architecture document](https://github.com/simpleiot/simpleiot/blob/feature-adr-7/docs/adr/7-jetstream-store.md)
for using it as the SIOT store:

Comments welcome!

![diagram](https://community.tmpdir.org/uploads/default/optimized/2X/3/324062080fabae7a58aa134abd81cf150a772563_2_748x748.png)

---

# The Secrets Behind Toyota’s Bullet-Proof Reliability

This
[article](https://www.topspeed.com/the-secrets-behind-toyotas-bullet-proof-reliability/)
about Toyota is interesting:

Some quotes:

> Compared to traditional manufacturing paradigms, Toyota empowers employees to
> halt production to address possible improvements or existing issues.

> As we distill the essence of Toyota’s philosophy, we find wisdom that applies
> to our daily lives.
>
> - The principles of Kaizen teach us the value of continuous improvement,
>   reminding us to seek and implement beneficial changes, however small, into
>   our daily routines
> - Jidoka highlights the importance of recognizing problems at their earliest
>   stages and addressing them immediately rather than escalating them
> - Focusing on practicality reminds us to prioritize sustainability and
>   long-term benefits over short-term gains.
> - Finally, the pursuit of innovation, as showcased by the Woven City project,
>   encourages us to explore and embrace new technologies
>   [such as the use of solid-state batteries in EVs](https://www.topspeed.com/toyotas-solid-state-batteries-up-to-932-miles/)
>   for a better tomorrow
>
> The story of Toyota is a roadmap to progress and sustainability - not just in
> the automotive world but in our daily lives as well. As we navigate our paths,
> it’s worth keeping these principles close to heart.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
