+++
title = "📆 TMPDIR Weekly - #58, Yoe 2023.05 release, Yoe Kiosk Browser, SIOT schedules"
date = 2023-07-29
+++

Hello,

Welcome to the 58th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to
anyone else you think might be interested. Suggestions and feedback are welcome at
✉️  [info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Innovation is the outcome of a habit, not a random act. – Sukant Ratnakar

---

# Yoe 2023.05 release 🚀

The latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/26),
is available. Some notable changes are, Clang compiler is upgraded to 16.0.6 which
is a bugfix release in 16.x series. The Go compiler is upgraded to
1.20.5 and rust to 1.70.0. Continue using LLD linker for more recipes as they get
fixed to work with lld linker. Yoe distro is using lld linker as default system linker
which should provide some speed improvments on large components e.g. qtwebengine
and overall speedup as well. QT6 layer is added to mx8 based projects and updated to
6.5.3 LTS release. Weston/wayland now works with pvr graphics on visionfive2 SBCs.
Support for Variscite VAR-SOM-MX8M-NANO and VisionFive2 RISCV SBC is added to yoe distro.

64-bit time_t and largefile support is now turned on by default for 32bit platforms in
yoe distro.

# Yoe Kiosk Browser

We have been working on a new [Kiosk Browser](https://community.tmpdir.org/t/yoe-kiosk-browser-a-browser-for-embedded-systems/1101) for embedded systems that 
has the following features:

- designed to run fullscreen
- no URL bar
- embedded touchscreen virtual keyboard
- keyboard width can be configured
- supports 0°, 90° or 270° screen rotation in the application. No messing around with window managers or external environments.

![kiosk browser](https://community.tmpdir.org/uploads/default/original/2X/1/10a5429b28987c79b7e42bb31854192a0a5b3fd9.jpeg)


---

# Simple IoT v0.12.1 release, Rule Schedule/Shelly improvements

We recently added support for Dates in Rule schedules as well as as
a number of small improvements such as making rules more responsive
to configuration changes, and enabling Shelly lights and switches
to be controlled by rule actions. Full changelog:

- support Dates in Rule schedule conditions
- Rules are re-run if any rule configuration changes
- Display error conditions in Rule nodes
- hide schedule weekday entry when dates are active
- hide schedule date entry when weekdays are active
- support deleting (tombstone points) in NodeDecode and NodeMerge functions
- remove index field from Point data structure. See #565
- add support for Shelly Plus2PM
- change Shelly client to use Shelly API Component model

![schedule](https://github.com/simpleiot/simpleiot/assets/402813/2bd25037-5fc7-45b6-b47d-1d94916ccbba)

---

# Organizing tools with Kaizen Foam

Are you looking for a good way to organize the tools in your lab?
Check out [Cliff's review](https://community.tmpdir.org/t/fastcap-kaizen-foam/1099) of Kaizen Foam.

![foam](https://community.tmpdir.org/uploads/default/original/2X/a/ae32ab7b29f33faedbb09df8f65457d920968389.jpeg)
---

Thoughts, feedback? Let us know: ✉️  [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰 [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
🎙 [https://tmpdir.org/](https://community.tmpdir.org/).
