+++
title = "TMPDIR Weekly - #25, February 19th, 2022"
date = 2022-02-19
+++

Welcome to the 25th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

A
[good article](https://bootlin.com/blog/using-device-tree-overlays-example-on-beaglebone-boards/)
covering Device Tree overlays. It appears the device tree overlay mechanism is
getting cleaned up and formalized some to better support the BeagleBone Black
and BeagleBone AI, which have a
[common pinout](https://elinux.org/Beagleboard:BeagleBone_cape_interface_spec).

---

# IoT

Lately in the Simple IoT project, I've been
[working through](https://github.com/simpleiot/simpleiot/pull/334) some node
life-cycle management issues. These are tricky as there can be conflicting
goals. The
[ADR approach](https://community.tmpdir.org/t/architecture-decision-records/334)
continues to serve me well with the clarity of thought that comes through
writing things out in a structured way.

---

# Other

[Putting Ideas into Words](http://www.paulgraham.com/words.html). Paul Graham
describes a common experience – we discover a lot when we write. This is one
reason I try to dedicate an hour every morning to writing. Real writing
(something more than responding to an email) is hard, thus we tend to avoid it.
Establishing a writing habit is probably the only way for most of us to make
this happen. Even this newsletter, while helpful to me for examining my
thinking, is not at the same level as a paper or blog post. -- cb

[fq](https://community.tmpdir.org/t/fq-tool-language-and-decoders-for-working-with-binary-data/428) -
a tool, language and decoders for working with binary data.

Do we need a new static site generator for project documentation? Over and over,
I find myself needing a static site generator to extract documentation from a
Git repo, and put up a static site. There are dozens of static site generators
available, but almost all of them suffer a fatal flaw that they you can’t have
images next to the Markdown files in the source files. Most require them to be
in a separate “public” directory with other static assets. This ends up
resulting in most projects have separate documentation repositories from the
source repositories.
[Join the discussion.](https://community.tmpdir.org/t/do-we-need-a-new-static-site-generator-for-project-documentation/429)

---

# Quote for the week

> Since new developments are the products of a creative mind, we must therefore
> stimulate and encourage that type of mind in every way possible. -- George
> Washington Carver

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
