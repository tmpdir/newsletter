+++
title = "📆 TMPDIR Weekly - #62, SIOT & Yoe releases, Living on main"
date = 2023-11-27
+++

Hello,

Welcome to the 62nd issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Let the dogs bark, Sancho. It's a sign that we are moving forward. -- Miguel
> De Cervantes, Don Quixote

---

# [Yoe Release 2023.10 - "Hampshire"](https://github.com/YoeDistro/yoe-distro/releases/tag/2023.10)

## Changed

- Upgrade clang to 17.0.3
- Fix libcgroup and kernel-selftest to build with lld linker
- Fix ptest images to match core layer naming convention
- Fix ptest reporting issues in meta-oe packages
- Upgrade mason to 1.2.2
- Upgrade u-boot to 2023.10
- Add 6.5 kernel and switch to using it as default for qemu machines
- Add kernel-selftest to slow ptest image for x86 and enable bpf tests
- Upgrade cryptodev to build with kernel 6.5
- Use ttyrun to get getty on busybox init and sysvinit init systems
- Add static-passwd and static-group files for meta-openembedded layers
- Fix ptest failures in poco, libtevent, and libnet-idn-encode
- Add ptests for libldb, cjson, re2
- Upgrade linux-yocto recipes to v6.1.57, v6.5.7
- Upgrade go compiler to 1.20.10
- Upgrade curl to 8.4.0
- Fix ptests for python3-py-cpuinfo and python3-pytest-mock
- Add ptest for libtext-diff-perl
- Update QT6.5 to latest
- Upgrade openSSH to 9.5p1
- Update dtb path for qemuarmv5 in linux-yocto 6.5 onwards
- Fix pvr drivers to work with musl on visionfive2
- Fix pvr visionfive2 drivers on musl
- Update nodejs to 20.8.1
- Make systemd packaging more granular
- Replace RUNTIME variable with TC_CXX_RUNTIME
- Do not set powersave as the default CPU governor in linux-raspberrypi
- Update simpleIOT to 0.13.1
- Switch to using QT 6.6
- Update ostree to 2023.7 and add ed25519-openssl support

## Added

- Added recipes - tayga, ttyrun, python3-arrow, python-git-pw
  - libexosip2, libosip2, pcapplusplus
- Add bblock feature to core
- Add Yoe updater support for VisionFive2 board

## Removed

- Dropped recipes for linux-yocto/6.4, po4a, debsums
- Remove bitbake-whatchanged bitbake utility
- Drop DEPLOY_DIR_TAR variable

---

# [Simple IoT release v0.14.1](https://github.com/simpleiot/simpleiot/releases)

- update to nats-server to v2.10.4
- update to nats client package to v1.31.0
- development: `envsetup.sh` sources `local.sh` if it exists
- Go client API for export/import nodes to/from YAML
- `siot` CLI
  [export and import](https://docs.simpleiot.org/docs/user/configuration.html#configuration-export)
  commands
- simpleiot-js improvements
- Network Manager Client (WIP)
- NTP Client
- serial client: allow configuration of HR point destination
- serial client: add "Sync Parent" option
- Signal generator client: add support for square, triangle, and random walk
  patterns. See
  [docs](https://docs.simpleiot.org/docs/user/signal-generator.html)
- fix issue with batched points of the same type/key (#658)

Note, use [v0.14.1](https://github.com/simpleiot/simpleiot/releases/tag/v0.14.1)
with updated frontend assets.

![image](https://github.com/simpleiot/simpleiot/assets/402813/4c83be2b-5ad8-4ea1-a57b-b5ece82a9b02)

---

# [Living on Main](https://tmpdir.org/024/)

Available on your favorite podcast [platform](https://tmpdir.org/about/).

In this episode, we discuss the “living on main” philosophy so that you can
track the latest software developments more effectively, reduce technical debt,
and participate in the software projects you use.

Outline:

- What do we mean by living on main?
- Traditional view of stability
- Advantages
- Examples
- How to do it

Discuss this episode at our
[community site](https://community.tmpdir.org/c/podcast/19).

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
