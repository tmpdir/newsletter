+++
title = "📆 TMPDIR News - #83, Yoe releses, Podcasts, and Newsletters"
date = 2024-12-23

+++

Hello,

Welcome to the 83rd issue of TMPDIR 📰 helping teams consistently innovate by
exploring the latest open source Linux and IoT technologies and workflows.
Please pass this on to anyone else who is interested in unlocking the power of
open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

We now have two other newsletters:

- [Platform Thinking](https://daily.bec-systems.com/) - Cliff explores ideas for
  helping you build better products by building YOUR own Platform.
- [Exploring Your Mindset](https://daily.himvis.com/) - Khem explores a broad
  range of topics from open-source, productivty, automation with the aim to
  improve both personally and organizationally.

We will likely be posting less industry news to this newsletter and using this
more for OSS project/podcast announcements. If you would like to to keep up with
general news and developments in our industry, please consider creating an
account at our [community site](https://community.tmpdir.org/) and Discourse
will email you a nice summary about once a week (this can be configured in your
Discourse settings).

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Good design adds value faster than it adds cost. -- Thomas C. Gale

---

# Yoe releases 🚀

Yoe Releases have been rolling on schedule and latest of the releases available
is
[November release of Yoe Distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/44)
As is usual with rolling release, useful features and fixes keep rolling in
along with important security fixes implicitly fixed in newer components.

Among usual major package upgrades, there are important features

- L4T R36.4.0/JetPack 6.1 support, this also unlocks hardware features on Jetson
  platforms which
  [improves performance](https://blogs.nvidia.com/blog/jetson-generative-ai-supercomputer/)
- Yoe now uses QT 6.8 by default for Kiosk browser
- Clang compiler major version has been upgraded from 18.x to 19.1 series
- An option to use swupdate OTA on tegra platforms has been added
- Default python is now 3.13, recipes needing porting has also been fixed

# LTS Kernels

Lately, LTS kernels have gone through some changes, where newer kernel are
reducing the 6 year LTS cycle to 2 years. With
[4.19 coming to EOL](https://lkml.iu.edu/hypermail/linux/kernel/2412.0/06587.html)
there still are 5.4, 5.10, 5,15 and 6.1 kernels being serviced for longer that 2
years period ending in December 2026. Starting with 6.6 the cycle has been
reduced to 2 years Latest LTS is 6.12 and it has also been picked by Civil
Infrastructure Platform (CIP) as their
[next CIP LTS kernel](https://lists.cip-project.org/g/cip-dev/topic/linux_4_19_325/110021965),
please note the CIP supports the kernel for 10+ years.

Having shorter LTS cycles might help promote bringing users to consume closer to
trunk kernels and use rolling release models for distributions which might also
help in complying to EU Cyber Resilience Act (CRA).

# Podcasts

There are a number of podcast episodes since we last sent out a newsletter:

- [Matt Madison, meta-tegra, and what makes a good Yocto BSP](https://tmpdir.org/034/)
- [Matthew Rassi - an overview of Lean](https://tmpdir.org/035/)
- [Bob Dotterer -- 3D Printing in Manufacturing](https://tmpdir.org/036/)
- [Recent experiences building a connected device with Zephyr](https://tmpdir.org/037/)
- [Open Hardware with Jason Kridner](https://tmpdir.org/038/)
- [Unintended open source forks](https://tmpdir.org/039/)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
