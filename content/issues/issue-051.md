+++
title = "📆 TMPDIR Weekly - #51, Yoe 2023.02, Particle in SIOT 🚀"
date = 2023-03-04
+++

Hello,

Welcome to the 51st issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Whenever you find yourself on the side of the majority, it is time to pause
> and reflect. --Mark Twain

---

# Linux

## [Yoe 2023.02 release](https://github.com/YoeDistro/yoe-distro/releases/tag/2023.02) 🚀

Glibc upgraded to 2.37. More recipes are fixed to build with the upcoming GCC13
and Clang16 compiler. The Go compiler is upgraded to 1.20.1. Static linking of
binaries with LLVM runtime is improved by bundling the `libc++abi` archive into
static `libc++.a`, Control zstd parallism for package generation in Yoe. When
timesyncd is enabled, then systemd-time-wait-sync.service is enabled by default,
which can be used to get a notification on the system time sync event.

# Empty packages and bogus dependencies - what to do?

Yocto has an interesting way of computing runtime dependencies and mostly it
works but sometimes it fails and one common reason is a dependent package is
empty and the packager removed it. There is some thoughts around this issue on
the
[architecture list](https://lists.openembedded.org/g/openembedded-architecture/message/1701).

# Development Stats for Kernel 6.2

Recently, Kernel 6.2 was released and [LWN](https://lwn.net/Articles/923410/)
has interesting stats where over 2000 developers contributed to kernel.
Reviewed-by tags are steadily growing over releases which could be a factor
contributing to code quality over time. Usual contributing organizations are
still the highest contributors for a long time, which is good for the overall
health of the project

---

# IoT

## [Simple IoT v0.9.0 released](https://github.com/simpleiot/simpleiot/releases/tag/v0.9.0)

This release has some cleanup and adds support for
[Particle.io](https://www.particle.io/) devices, such as the
[SIOT Particle Gateway](https://github.com/simpleiot/hardware).

![gw](https://community.tmpdir.org/uploads/default/optimized/2X/6/613279233b4b64b663f320f9a66b55c2949eafce_2_1035x742.jpeg)

# [Monitoring systems with Simple IoT](https://community.tmpdir.org/t/monitoring-systems-with-siot/940)

We've been using SIOT more to monitor servers and other systems we manage. Using
the [metrics client](https://docs.simpleiot.org/docs/user/metrics.html), we can
easily monitor a number of system and application parameters. SIOT is an easy
way to get data from remote systems into Grafana.

![monitoring](https://community.tmpdir.org/uploads/default/original/2X/d/dc9594c18fbb1eb3a2f4a3868b1e35ed71982d12.png)

---

# Other

## [Leaving the cloud](https://community.tmpdir.org/t/why-were-leaving-the-cloud/697/8)

[37 Signals](https://37signals.com/) continues to document their exodus from the
cloud. The basic premise is that if you need an entire server and your load is
fairly constant, then you are probably overpaying for cloud resources vs owning
them.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
