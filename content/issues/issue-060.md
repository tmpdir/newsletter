+++
title = "📆 TMPDIR Weekly - #60, Yoe Updater overlay strategies, Qt6"
date = 2023-08-19
+++

Hello,

Welcome to the 60th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Time is a created thing. To say 'I don’t have time,' is like saying, 'I don’t
> want to'. -- Lao Tzu

---

# Yoe Updater updates

Yoe updater has added a new mode for root file system layout making it third
option to layout the flash memory map. It is controlled via `OVERLAY_TYPE` knob,
it has following settings

- `tmpfs` - A read-write overlay on top read-only rootfs is made out of tmpfs
  file system which will be reset on reboot.

- `persistent` - A read-write overlay on top of read-only rootfs is made using a
  directory located in data partition which is r/w and since its on flash/emmc
  it persists across reboots. It will also persist across updates to image.

- none - This avoids adding any overlay. The rootfs is made read-write and
  changes are made in place to files and like option 2 above persists across
  reboots however it will be wiped away when image is updated

# QT5 and QT6

- The rpi4-64 and the x86_64 emulator project are able to build and run
  `yoe-kiosk-image` on the EGLFS Yoe distro profile. The included browser is the
  QT6 based yoe-kiosk-browser. QT5 has been dropped completely from Yoe distro.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
