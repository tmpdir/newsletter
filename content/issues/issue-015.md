+++
title = "TMPDIR Weekly - #15, November 13th, 2021"
date = 2021-11-13
+++

Welcome to the 15th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Have you ever wondered how a Linux program starts?
[This article](http://dbp-consulting.com/tutorials/debugging/linuxProgramStartup.html)
provides a nice overview of what all your Linux system does before your program
reaches your `main()`.

There is
[proposal to simplify](https://lists.openembedded.org/g/openembedded-architecture/topic/proposed_bitbake_syntax/86933270?p=,,,20,0,0,0::recentpostdate/sticky,,,20,2,0,86933270,previd=1636496543225794091,nextid=1623257913990759922&previd=1636496543225794091&nextid=1623257913990759922)
the Bitbake syntax for adding to variables. This would simplify longstanding
undefined behavior where `foo_append += "YYY"` would prepend a space before
`"YYY"` automatically giving an illusion of a legitimate operation.

---

# IoT

This week, we [took at look](https://github.com/simpleiot/simpleiot/issues/294)
at how we can resolve loops in the SIOT data. We store data as nodes and edges,
which form a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph).

![DAG](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Tred-G.svg/330px-Tred-G.svg.png)

However, there can be cases where users on two different instances make changes
to the tree structure which cause a loop to be formed. A loop cannot be allowed,
as it will form an infinite loop when rendering the tree, or walking back up the
tree when processing new points. When a loop is detected, it must be broken and
all instances need to converge to the same solution. To develop an algorithm, I
started
[sketching out](https://github.com/simpleiot/simpleiot/issues/294#issuecomment-967485407)
various scenarios and a solution emerged which seems fairly simple and robust.
This is a complex problem syncing a DAG between distributed systems with very
constrained network bandwidth (cellular CAT-M), but with some effort, **complex
problems can have simple solutions**. This type of analysis is great fun --
amazing what can happen when you get out some paper and a pencil and start
drawing.

This completes
[our analysis](https://github.com/simpleiot/simpleiot/blob/cbrake/adr-1/docs/adr/1-consider-changing-point-data-type.md)
of CRDT properties of the SIOT data structures. It seems we have a fairly solid
base for moving forward. -- cb

---

# Other

This [article](https://go.dev/blog/12years) on the Go blog claims Go performance
should improve soon:

> In February, the Go 1.18 release will expand the new register-based calling
> convention to non-x86 architectures, bringing dramatic performance
> improvements with it.

Interesting presentation on
[Rethinking Time](https://community.tmpdir.org/t/rethinking-time/358). A slide:

![time and causality](https://community.tmpdir.org/uploads/default/original/1X/81776b1ec19daa50d13e5a02d7b51a7cc4a251d7.jpeg)

---

# Quote for the week

> There is nothing a mere scientist can say that will stand against the flood of
> a hundred million dollars. But there is one quality that cannot be purchased
> in this way — and that is reliability. **The price of reliability is the
> pursuit of the utmost simplicity.** It is a price which the very rich find
> most hard to pay. -- Tony Hoare

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
