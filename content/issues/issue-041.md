+++
title = "TMPDIR Weekly - #41, September 24th, 2022"
date = 2022-09-24
+++

Hello,

Welcome to the 41st issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## Rust is coming to Linux Kernel

At the recently concluded kernel summit, Linus said "Unless something odd
happens, it [Rust] will make it into 6.1." which will widen the options for
programming the kernel. Initially, it will just be infrastructure perhaps that
will show up but it will be interesting to watch how it gets used in coming
months and years that will define its effective success and benefit to
programming kernel. Being able to build Linux with Clang has helped Rust's case
as well since the Rust compiler uses the LLVM infrastructure. Linus said "Clang
does work, so merging Rust would probably help and not hurt the kernel."

There is already a work in progress to implement
[NVMe driver in rust](https://blog.desdelinux.net/en/western-digital-already-works-on-an-nvme-driver-written-in-rust/).

I do expect some interesting issues to crop up due to kernel usecases in Rust
compiler or language design itself e.g.
[The perils of pinning](https://lwn.net/Articles/907876/) which talks about data
being stable in memory. Moving data without kernel programmer's knowledge may
bring up interesting problems to front as describe in this thread. However,
bringing rust to kernel could mean another life for Linux kernel, and new
developer infusion in coming future.

# 128-bit Linux (Zettalinux)

While we are still fresh from migrating from 32bit to 64bit and Linux
distributions are still in process of going fully 64bit, there are thoughts
already collecting around 128bit systems. The [Zettalinux]
(https://lpc.events/event/16/contributions/1223/attachments/1063/2029/2022-09_KS_Zetta.pdf)
was an interesting discussion since RISCV already is discussing 128-bit
extentions to it's ISA. It might be the first architecture to have support in
qemu to support 128-bit and can be used to do the heavy lifting.

# Yoe distro and zstd compression by default in opkg

Yoe distro uses opkg for it's package management needs. When building images,
the system first builds the preferred choice of package feeds e.g. rpm, deb or
ipk, and then uses it to build final images. Opkg defaults are to use xz
compression which is great at generating smaller archives but can be slower, A
recent
[patch to opkg](https://git.openembedded.org/openembedded-core/commit/?id=a0892c3209e3892d79b97dcd4ec0e5a89057258c)
added an option to enable it via a packageconfig. Yoe distro built upon that and
[switched to using zstd distro defaults](https://github.com/YoeDistro/yoe-distro/pull/720).
This should speed up build times, but it will increase the feed sizes; however,
it makes sense to optimize for build times as disk space is relatively cheap.

---

# IoT

This week, we've improved the InfluxDB client to better handle changes, and
fixed a race condition in the HTTP API code. I'm amazing at
[how simple](https://github.com/simpleiot/simpleiot/commit/da2345eba4984257b5dae9065fa6ffef9276a805)
lifecycle and concurrent code becomes once we apply the
[Start()/Stop() pattern](https://docs.simpleiot.org/docs/ref/architecture-app.html#application-lifecycle).

We're also working on handing of high-rate data as we currently have a project
that needs to handle sampling a number of signals at 15KHz. This is very fast
for an IoT system, so experimenting with processing these types of data
differently than most data as it will only be stored in InfluxDB or processed by
analysis algorithms.

---

# Other

## [Caddy v2.6.0 Release](https://github.com/caddyserver/caddy/releases/tag/v2.6.0)

The release notes are very well done -- the events feature looks interesting.
I've used Caddy in the past to fetch SSL certs for other applications, and it
would be useful to be able to restart them automatically when the cert changed.

The Virtual file systems is also interesting -- simple abstractions like this
leads to so many opportunities.

## [Logseq: A privacy-first, open source notes system](https://community.tmpdir.org/t/logseq-a-privacy-first-open-source-knowledge-base/656)

This looks like an interesting note taking application. Some observations:

- stores files locally in Markdown, which
  [is important people who want to own their content](https://community.tmpdir.org/t/habit-note-taking-while-working-on-a-task/601/3?u=cbrake).
- stores images and files you add to notes along with your markdown files (that
  is very nice!)
- outline note system like workflowy or Roam. However, things are still
  organized by pages, instead of an open ended hiearchy like Workflowy.
- can open PDFs in the app, and you can highlight sections and store a list of
  these annotations
- uses linking extensively to organize information in a graph
- written in Clojure
- most of the authors are Chinese

Overall, I'm fairly impressed.

https://logseq.com/

Here is a short video:

https://youtu.be/IRp2zS8CAEc

## [How to cut down on email distractions in Thunderbird](https://community.tmpdir.org/t/how-to-filter-emails-in-thunderbird-to-a-newsletters-folder/652)

Creating filters in Thunderbird is super easy -- right click on the "From"
field, and select "Create Filter From ...". This only takes a few seconds and if
you do this for a month or so, pretty soon all your newsletters and other
non-critical mail can be easily filtered to a folder.

You can then look at your Newsletter folder periodically, read what looks
interesting, and then right click on the folder and mark the rest as read. This
is much quickly that clicking on each email in your inbox and deleting it. All
too often I see something interesting in the email and end of spending time
reading when I don't have time.

So two key aspects:

- filter all newsletter type email to a folder
- periodically, look at the folder and mark it as read

This will save you time!

See video below for a short demo:

https://youtu.be/WZDpk73X5Vs

---

# Quote for the week

> One of the great challenges (difficulties) in this world is knowing enough
> about a subject to think you're right, but not enough about the subject to
> know you're wrong. -- Neil deGrasse Tyson

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
