+++
title = "📆 TMPDIR Weekly - #48, Technology Trajectory 🚀"
date = 2023-01-20
+++

Hello,

Welcome to the 48th issue of TMPDIR, a weekly newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## [Yoe 2022.12 release](https://community.tmpdir.org/t/yoe-distribution-releases/239/20)

Last release of the year. This release has minor updates to Clang C/C++ compiler
to 15.0.6, Rust is updated to 1.66.0 and major fixes to support musl without
LFS64 APIs. It also exposed casses where we needed python3targetconfig to use
target python instead of host python. XFCE is upgraded to 4.18 and systemd to
252.4

## [Chromium Allows Rust](https://security.googleblog.com/2023/01/supporting-use-of-rust-in-chromium.html)

This will allow third-party Rust libraries to be used from its C++ codebase.
Right now, the Rust toolchain is being added to its build system, so it might be
few months before rust code starts to trickle in for chromium. This is being
done to support simpler and safe code. Only one-way interop c++ -> rust is
planned to be supported.

---

# IoT

## [Simple IoT v0.7.1 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.7.1)

This release improves both the user and developer experience in that log-in is
preserved so if you reload your browser or the SIOT backend, you don't have to
re-login. We've also integrated `elm-watch` so when changes are made in the UI
Elm code, the UI is updated in real-time without a full page reload. This is
especially important with the SIOT UI as the node tree maintains state and you
don't have to navigate to the node to see the changes. This allows you to
quickly iterate on changes and see the effect immediately. This is state of the
art UI tooling which is made possible in part by the simplicity of the Elm
architecture.

- upgrade frontend to elm-spa 6 (#197)
- apply elm-review rules to frontend code and integrate with CI (#222)
- changes so user does not have to log in if backend or browser is restarted
  (#474)
  - frontend: store JWT Auth token in browser storage
  - frontend: store JWT key in db
- use [air](github.com/cosmtrek/air) instead of entr for watching Go files
  during development. This allows `siot_watch` to work on MacOS, and should also
  be useful in a Windows dev setup.

See the [Hot reloading the Simple IoT UI video](https://youtu.be/_Nrs2_l62_Q)
for a demo of these changes.

---

# Other

## The Technology Trajectory

Technology follows a trajectory from inception, growth, maturity, decline, and
finally obsolescence. Understanding where a technology is on this curve is
important when deciding when to adopt a technology. We discuss this topic more
in a [blog post](http://bec-systems.com/site/2084/the-technology-trajectory) and
[podcast episode](https://tmpdir.org/017/).

![trajectory](http://bec-systems.com/site/wp-content/uploads/2022/12/image-1.png)

## [Variscite i.MX93 SoM for Energy Efficient Machine Learning Edge Devices](https://community.tmpdir.org/t/new-i-mx93-based-som-for-energy-efficient-machine-learning-edge-devices/861)

We have used Variscite SoM's in a number of products, so exiting to see a new
one based on the i.MX93 CPU which includes a machine learning NPU (Neural
Processing Unit). NXP and the various SoM suppliers do an excellent job of
supplying compute for lower volume applications. Throughout the component
shortage, when rPI CM4 modules were not available, Variscite was still quoting
lead times in the ~20 week range. (this is a sample of one part, so not sure if
consistent across their entire product line.) I also checked status of i.MX
parts in Digi-key/Mouser, and many were still stocked throughout the shortages.
It appears we may be nearing the end of the tunnel on component shortages but it
reminds us of the maxim -- if you are building an industrial, long-lived
product, you are probably best off buying from companies who are industrial
suppliers. Buying from companies that are primarily in the consumer space is
risky.

---

# Quote for the week

Perfection is achieved not when there is nothing more to add, but rather when
there is nothing more to take away. – Antoine de Saint – Exupery

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
