+++
title = "TMPDIR Weekly - #33, April 23rd, 2022"
date = 2022-04-23
+++

Welcome to the 33rd issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## [Linux File Heirarchy](https://community.tmpdir.org/t/arch-linux-notes/256/11?u=khem)

Ever wonder why `/bin`, `/sbin`, `/usr/bin`, and `/usr/sbin` exist? The
structure is based on traditional UNIX systems; however, it has had evolution of
its own. The
[`/usr` merge](https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/)
has been implemented in various Linux distributions. Archlinux and
[Fedora](https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/)
have switched to using usrmerge heirarchy some time ago.
[Rob's post](http://lists.busybox.net/pipermail/busybox/2010-December/074114.html)
on busybox mailing list elucidates the history behind it. We are also exploring
switching the Yoe Distro to use merged /usr file heirarchy for future releases.

## [Adventures Setting up a NixOS server](https://community.tmpdir.org/t/setting-up-a-nixos-server-on-linode-to-run-wordpress/485)

The Nix package manager is fascinating and a quite unorthodox Linux distro
(NixOS) is based on it. There is quite a bit to learn; therefore, we have
started some experiments setting up a NixOS instance and playground for creating
a system to our liking. Eventually this could be a good template for deploying
services; however, we have ambitious plans to also try it as default OS on
embedded distributions.

---

# IoT

Making progress on the
[v0.1.0 milestone](https://github.com/simpleiot/simpleiot/milestone/1). Also
[discussing ways](https://community.tmpdir.org/t/embedding-assets-into-go-executable/482)
to embed the compiled frontend into the Go package so that we can directly use
SIOT as a Go package -- looks like go:embed will make this practical.

A
[detailed article](https://community.tmpdir.org/t/building-data-centric-apps-with-a-reactive-relational-database/490)
about ideas on building data-centric applications that can interoperate by
sharing data directly instead of defining verb-based APIs. This is similar to
where I've landed in
[my explorations of IoT architecture](https://docs.simpleiot.org/docs/ref/architecture.html).
Every instance should have a copy of the data it is interested in and simply
read and write that data. Everything else happens automatically in the
background.

---

# Other

[Have you tried Tailscale?](https://community.tmpdir.org/t/have-you-tried-tailscale/493).
This looks like a very promising VPN technology, based on Wireguard that is
extremely easy to set up, and its mesh model is very interesting.

---

# Quote for the week

> If it's not written, it never happened. If it is written, it doesn't matter
> what happened. ― Sercan Leylek

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
