+++
title = "TMPDIR Weekly - #12, October 22nd, 2021"
date = 2021-10-22
+++

Welcome to the 12th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Linux

Rust
[1.56 was released](https://github.com/rust-lang/rust/blob/master/RELEASES.md#version-1560-2021-10-21).
The important piece is that it stabilizes the _2021 Edition_ as its mechanism to
define opt-in changes which may be backward incompatible. Last was 2018 edition.
This release also uses the LLVM 13 release, which is also default compiler in
Yoe distro as well.

At the [Linley conference](https://www.linleygroup.com/events/event.php?num=52)
SiFive announced the fastest RISCV core, which is rubbing shoulders with the Arm
Cortex-A78. This comes on top of the current P550 core. This is quite exciting
for a new ISA and CPU architecture to showcase these kinds of numbers.

---

# IoT

Thinking on SIOT data types is continuing in
[ADR-1](https://github.com/simpleiot/simpleiot/pull/279). The good news is we
are learning how to simplify things, yet provide ways to represent more complex
data in SIOT `Points`, such as arrays and maps. I think there are inherent
advantages in simple/flexible data structures for storage and wire formats --
especially for distributed systems in that core layers can remain the same as
you add new features to the system. -- cb

---

# Other

Espressif is
[now using RISC-V](https://community.tmpdir.org/t/espressif-is-now-using-risc-v-in-their-mcus/337)
in their ESP32 WiFi/Bluetooth MCUs.

This [podcast](https://elm-radio.com/episode/lamdera/) is an interesting listen.
In it the author of Lamdera discusses his solutions to some of the fundamental
problems of Web applications and architecture -- might be interesting to anyone
interested in computer/web architecture in general. See more notes
[here](https://community.tmpdir.org/t/lamdera-a-delightful-platform-for-full-stack-web-apps/338).
Mario made a comment that really resonated with me:

> when you actually use it (Elm) and feel the ergonomics, its not just the fact
> that Elm has that type safety, it’s instant, it’s fast, one of the banes of
> having to build a platform is you never get to use the platform, I feel 90% of
> my time is writing Haskell in the Elm compiler – that’s where most of
> Lamdera’s stuff is – it’s Haskell. And, it’s not nice – I love Haskell – it
> has so many cool things and was my gateway drug into FP, but man – waiting
> 30-40 seconds just to type things is like – yeah – it’s so understated how
> Evan’s focus on making that performance and that speed fast – it just changes
> – fast type inference and slow type inference may as well be two completely
> different things in my eyes. The way that you build things and the way you
> approach stuff when you have it be that quick is completely different, and the
> way I approach full stack apps with Lamdera is just completely different – the
> confidence you have to charge into wide sweeping changes …

I feel the same way. Programming in Elm and Go is a much more than just the
language paradigm – both compilers are fast and efficient – the tooling is
excellent. It is a completely different experience. -- cb

We
[did a little looking](https://community.tmpdir.org/t/julia-package-system/339)
into the Julia programming language. Julia is a high performance dynamically
typed language that compiles to efficient native code using LLVM. It is a
general purpose language, but is typically used for numerical analysis and
computational science. Its performance sets it appart from other languages in
this space like Python and R. Additionally, its package system is very advanced
and allows you to easily compose solutions. If you like Jupyter notesbooks, give
the [Pluto](https://www.juliapackages.com/p/pluto) package a try.

---

# Quote for the week

> A complex system that works is invariably found to have evolved from a simple
> system that works. The inverse proposition also appears to be true: A complex
> system designed from scratch never works and cannot be made to work.
>
> John Gall, Systemantics (1975)

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
