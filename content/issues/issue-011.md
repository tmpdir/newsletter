+++
title = "TMPDIR Weekly - #11, October 16th, 2021"
date = 2021-10-16
+++

Welcome to the 11th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Starting with this edition, we will append our initials at the end of comments
that are opinionated. We may not always agree on everything and opinions
(especially strong ones) should always be owned.

---

## Linux

Yocto Honister (3.4) release is planned for the end of this month. The upcoming
Yoe 21.10 release will include these changes.

KDE Plasma 5.23 (KDE's [25th anniversay](https://25years.kde.org/) edition) has
been released and is making steady progress with many small updates making the
user experience better. These improvements include multi-screen, Wayland, and
Breeze theme updates. I am excited to try this release! Check out
[Nate's post](https://pointieststick.com/2021/10/01/this-week-in-kde-getting-plasma-5-23-ready-for-release/)
on this release. --kr

Python setup tools will
[now fail](https://github.com/pypa/setuptools/issues/2769) if there is
dependency on a packages that requires Python 2.x. This caused us to discover
stale packages in Yocto that have not been updated in 10 years.

We started adding support for the
[HiFive Unmatched RiscV board](https://www.sifive.com/boards/hifive-unmatched).
This is a very capable board that contains the fastest RiscV SOC available now.
More news to follow about Yoe/SIOT support for this board.

---

# IoT

In the Simple IoT project, I have
[been experimenting](https://github.com/simpleiot/simpleiot/tree/cbrake/adr-1/docs/adr)
with a new process for exploring and capturing architecture decisions. I first
encountered this in the one of the
[NATs repos](https://github.com/nats-io/nats-architecture-and-design), which
links back to
[this article](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions).

The
[first SIOT ADR](https://github.com/simpleiot/simpleiot/blob/cbrake/adr-1/docs/adr/1-consider-changing-point-data-type.md)
is an exploration of whether we should change the `Point` data structure type.
In the process of adding system metrics, there is some data we can’t easily
express in the current types. Some obserervations about the ADR process:

- you think better when you write
- fits with the
  [Lead with Documentation](https://handbook.tmpdir.org/documentation/lead-with-documentation/)
  approach to work
- every product feature or architectural change should start with the question:
  _what real-world problem does this solve?_ The ADR process helps clarify this.
- one thing I’ve noticed in
  [Kleppmann’s book](https://community.tmpdir.org/t/book-review-designing-data-intensive-applications/288)
  is the extensive use of examples to discuss various concepts. I think examples
  are important to include in an ADR as it really helps clarify thinking.
- in the future, an ADR should be a useful reference to document and understand
  why something is they way it is.

I’m very pleased with how this first ADR is working out and hope to continue
using them to think through difficult/major project decisions.

Still a bit torn whether ADR discussions should happen in Github issues or PRs.
An issue has the benefit that it can span multiple PRs. A PR has the benefit
that commends can be inlined with code. --cb

---

# Other

Started a topic for
[Rust OS/hardware/embedded programming](https://community.tmpdir.org/t/rust-os-hardware-embedded-programming/333).
There is a lot of activity in this space. In one sense, I am really interested
in the use of Rust for embedded/system/hardware programming, but at the same
time I'm wary of language complexity as expressed in
[this article](https://community.tmpdir.org/t/selecting-a-programming-language/98/5).
--cb

---

# Quote for the week

_I have made this letter longer than usual, only because I have not had the time
to make it shorter. -- Blaise Pascal, Lettres provinciales no. 16 (1657)_

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
