+++
title = "📆 TMPDIR Weekly - #76, Embedded Open Source Summit Recap 🎙️"
date = 2024-05-17
+++

Hello,

Welcome to the 76th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Simplicity is the art of hiding complexity. -- Rob Pike

---

# 🎙️ Podcast Embedded Open Source Summit Recap

Khem attended EOSS and shares some of his thoughts about the event.

[Listen here ...](https://tmpdir.org/030/)

---

# What is in Simple IoT, and how we can make it smaller

Using a [neat tool](https://github.com/Zxilly/go-size-analyzer), we can see what
is taking up space in the SIOT Go binary:

![space](https://community.tmpdir.org/uploads/default/optimized/2X/2/2bd8fc56b37bd1af8d155a87b39cdfc7006db737_2_1035x483.jpeg)

The good news is if we can get the store moved to Jetstream, the Influx client,
sqlite, and protobuf can all be removed (yellow), which is a significant chunk
of the current binary:

![svg](https://community.tmpdir.org/uploads/default/optimized/2X/4/44c15938b4299220fccc13feea9584604f5baef5_2_1035x615.png)

Perhaps pprof (cyan) can also be removed from distributed binaries.

Small is important when distributing updates to edge systems.

The SIOT code is adding up (green).

[discuss here ...](https://community.tmpdir.org/t/what-is-in-simple-iot-and-how-we-can-make-it-smaller/1298)

---

# Leave the code better than you found it

Some helpful thoughts from Khem on why upstreaming fixes makes your life easier
in the long run:

[read here ...](https://himvis.com/2024/05/08/leave-the-code-better-than-you-found-it/)

---

# [Principles of simplicity](https://community.tmpdir.org/t/principles-of-simplicity-go-time-296/1301)

A discussion about one of my favorite topics:

Listen here:
[Principles of simplicity Go Time episode](https://changelog.com/gotime/296)

> Rob Pike had this great talk, “Simplicity is complicated”, where he says
> “Simplicity is the art of hiding complexity.” He goes on to give garbage
> collection as an example, where under the hood, the garbage collector is
> extremely complex, but to the users it’s really simple.

[Rob's original talk on YouTube](https://youtu.be/rFejpH_tAHM)

> Go is often described as a simple language. It is not, it just seems that way.
> Rob explains how Go's simplicity hides a great deal of complexity, and that
> both the simplicity and complexity are part of the design.

This is the essence of good software design -- we do hard stuff so someone's
life can be simpler. This does not always mean that we make things simple for
ourselves. In some cases, we build a complicated subsystem that makes other more
frequent tasks easy. One example of this is the Simple IoT store -- it is fairly
complicated and took a lot of work to create, but creating new SIOT clients is
pretty simple because the store does a lot of the hard work.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
