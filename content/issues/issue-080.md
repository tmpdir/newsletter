+++
title = "📆 TMPDIR Weekly - #80, ESP32-POE Ethernet, Eystein Stenberg 🎙️"
date = 2024-06-29
+++

Hello,

Welcome to the xxth issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Focus on being productive instead of busy. -- Tim Ferriss

---

# Ethernet is now working in Zephyr on Olimex ESP32 POE

We have been working on getting Zephyr/Ethernet working on a Olimex ESP32 POE
device. Olimex supports Arduino, but not Zephyr, so it took a little bit of work
to pull this together.

![olimex](https://community.tmpdir.org/uploads/default/optimized/2X/7/76e524e022a6041a3a38b9d76eeb778e313f4ccf_2_1332x1000.jpeg)

See
[discussion here](https://community.tmpdir.org/t/zephyr-on-the-esp32/1310/8?u=cbrake)

---

# Discussion with Eystein Stenberg from Mender.io 🎙️

Great discussion with Eystein on updating systems, how Mender has evolved over
the last ~10 years, and more:

- Eystein has a background in Math and cryptography
- Company started as CFEngine
- Things people tend to not think about with system update:
  - security
  - reliability (power loss, does it resume downloads when there are network
    issues, etc.)
  - future products – is the solution general
  - scheduling
  - updating multiple devices in the system
- Mendor milestones
  1. OSS release
  2. SaS service
  3. advanced environments, complex deployments, orchestration, etc
- OSS foundation
  - open-core model
  - product led growth
  - is working well
- Programming languages
  - Switching the Mender client from
    [Go to C++](https://hub.mender.io/t/mender-to-rewrite-client-using-c-and-retain-go-for-its-backend/5332)
    so the client is more portable.
- Platform vs Product
  - Product – narrow set of problems, pick what you need, less vendor lock-in
  - Platform – wide focus and tends toward locked in
- Interesting in the technology space
  - Bootloader standardization would help a lot
  - [ARM System Ready](https://www.arm.com/architecture/system-architectures/systemready-certification-program)
    looks interesting.
- Advice for young engineers

[Listen to podcast here](https://tmpdir.org/032/)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
