+++
title = "TMPDIR Weekly - #35, May 14, 2022"
date = 2022-05-14
+++

Welcome to the 35th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## [GCC 12 Release](https://lwn.net/ml/gcc/p3p5oo2-3221-p179-5547-o250q2p5r8sn@fhfr.qr/)

GCC community announced a major release this week and the OpenEmbedded community
has been actively trying the GCC 12 snapshots. Since it supports a wide variety
of architectures and uses cross compilation, several issues were found and
quickly fixed in GCC 12 before release. Static analysis component have been
coming along well in past GCC releases since GCC 10 -- there were few issues in
OE found due to the improved static analyser.

## Adding 1-wire support in Yoe

1-wire support on Raspberry Pi platforms was added to the Yoe distro with
relevant knob being
[proposed in upstream BSP layer](https://github.com/agherzan/meta-raspberrypi/pull/1059).

## Nvidia is [open sourcing](https://community.tmpdir.org/t/nvidia-is-open-sourcing-more-of-their-software-stack/514) more of their software stack.

---

# IoT

v0.1.0 of Simple IoT
[is released](https://github.com/simpleiot/simpleiot/releases/tag/v0.1.0) with
1-wire temperature sensor support.

---

# Other

This
[Developer Productivity podcast](https://community.tmpdir.org/t/podcast-developer-productivty/509)
is excellent -- full of good ideas.

Book review:
[The Phoenix Project](https://community.tmpdir.org/t/book-review-the-phoenix-project/516).
Great book, highly recommend.

[Several articles](https://community.tmpdir.org/t/when-will-we-learn/512) that
discuss the security trade-offs with distro and language packages.

Ben Johnson (author of BoltDB and Litestream) is now at Fly.io and
[discusses](https://community.tmpdir.org/t/ben-johnson-and-litestream-are-now-at-fly-io/508)
why SQLite might be a good db choice for many applications.

---

# Quote for the week

> The belief that complex systems require armies of designers and programmers is
> wrong. A system that is not understood in its entirety, or at least to a
> significant degree of detail by a single individual, should probably not be
> built. -- Niklaus Wirth

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
