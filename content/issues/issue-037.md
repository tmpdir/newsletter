+++
title = "TMPDIR Weekly - #37, July 23rd, 2022"
date = 2022-07-23
+++

Welcome to the 37th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

The newsletter has been in hiatus for the past month or so due to our travel
schedules.

---

# Linux

## [Yoe Distro 2022.06 - “Rygja” Release](https://community.tmpdir.org/t/yoe-distribution-releases/239/13)

This is relatively light release due to summer holidays and time-off -- we could
still pull off a release so that's a good thing. Few noteworthy changes are:

- Networkmanager defaults to nftables instead of iptables
- systemd is bumped to 251.2 release
- ptest ( package tests ) can now be compiled with parallelism turned on and a
  knob is provided to disable it for offending recipes.
- Go compiler got updated to 1.18.3 (latest point release in 1.18 release)
- Polkit now uses mozjs again instead of duktape due to regressions observed
  with duktape defaults.

## Linux distributions and rolling Releases

We have been using rolling release for Yoe distro, and we also use rolling
distributions e.g. ArchLinux as our default workstation Operating systems for
quite a while. It has been a good experience -- especially when you are a small
team, it's generally good to get latest updates and less work to integrate as
you run into current issues seen with fresh releases of packages. Upstream
developers are more responsive to the latest releases or tip of trunk of their
projects. An interesting post about
[google's internal distro turning to rolling release process](https://community.tmpdir.org/t/how-google-got-to-rolling-linux-releases-for-desktops-google-cloud-blog/557)
is another distro turning towards this process.

---

# IoT

[v0.3.0](https://github.com/simpleiot/simpleiot/releases/tag/v0.3.0) of Simple
Iot has been released with many internal improvements. The application lifecycle
has been simplified so that the entire app can easily be started and stopped in
a unit test. This allows us to do end-to-end testing of the entire application.
We've also started using Generics (available in Go 1.18) in various
[places](https://github.com/simpleiot/simpleiot/blob/master/client/manager.go)
-- that is working out really well. Another shift is the focus on "clients".
Eventually most of Simple IoT will be clients where they simply subscribe and
publish information as needed. Clients can live
[inside the application or be external](https://docs.simpleiot.org/docs/ref/architecture-app.html).
We are also working on support for
[serial connections](https://docs.simpleiot.org/docs/ref/serial.html) to
Microcontrollers. This will allow MPUs and MCUs to easily share information in a
system.

---

# Other

Started
[experimenting](https://community.tmpdir.org/t/raspberry-pi-rp2040-mcu-platform/206/8?u=cbrake)
with a rPI PICO -- the build system is nice.

Also did some [experimenting](https://community.tmpdir.org/t/tinygo-on-mcus/553)
with Tinygo.

A nice
[article](https://community.tmpdir.org/t/writing-the-most-misunderstood-activity/556)
on writing. Writing is a privilege and a great opportunity.

---

# Quote for the week

> Fools ignore complexity. Pragmatists suffer it. Some can avoid it. Geniuses
> remove it. - Alan Perlis (1st recipient of the Turing Award)

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
