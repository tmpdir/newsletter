+++
title = "TMPDIR Weekly - #6, September 4th, 2021"
date = 2021-09-04
+++

Welcome to the 6th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

- Yoe Distro 2021.08 is released few days late due to last moment issues in
  compiling inside docker needed timely fixes in OE-Core and bitbake and
  ODROID-C4 hardkernel machine being broken due to BSP clean up in meta-odroid,
  it has many changes as described in
  https://github.com/YoeDistro/yoe-distro/releases/tag/2021.08
- Linux Kernel 5.14 was released with features like secret memory areas support
  with MEMFD_SECRET, more details can be found
  [here](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.14-Features).
- Linux 5.15 development/merge window is open and there are interesting changes
  lined up in filesystem areas which are of interest for OE/Yocto users too as
  they do large builds and improvements in filesystem performance are
  appreciated.
  - [ext4](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.15-EXT4)
  - [xfs](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.15-XFS-EROFS)
  - [btrfs](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.15-Btrfs)
  - here is a nice filesystem
    [performance chart](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.14-File-Systems)
    on SSD using 5.14 kernel

---

## IoT

The
[latest release v0.0.34](https://community.tmpdir.org/t/simple-iot-releases/214/23?u=cbrake)
of Simple IoT implements caching to improve performance of handling node points.

---

## Other

- Docker has
  [recently announced](https://www.docker.com/blog/updating-product-subscriptions/)
  that Docker Desktop will soon require a subscription and, based on the size of
  your company, may require a paid subscription. (It remains free for personal
  use), this has caused some uproar and documents around migrating from docker
  to podman are starting to appear.
- [Writing well documented code](https://codecatalog.org/2021/09/04/well-documented-code.html)
- [Programs Vs portability](https://utcc.utoronto.ca/~cks/space/blog/unix/ProgramsVsPortability)
- [Discussion on using Dropbear vs OpenSSH](https://community.tmpdir.org/t/using-dropbear-vs-openssh/302)

---

## Quote for the week

_Simplicity is hard work. But, there’s a huge payoff. The person who has a
genuinely simpler system — a system made out of genuinely simple parts, is going
to be able to affect the greatest change with the least work. He’s gonna spend
more time simplifying things up front and in the long haul he’s gonna wipe the
plate with you because he’ll have that ability to change things when you’re
struggling to push elephants around. -- Rich Hickey_

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
