+++
title = "TMPDIR Weekly - #34, April 30th, 2022"
date = 2022-04-30
+++

Welcome to the 34th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## [Yocto 4.0 LTS Released](https://lists.yoctoproject.org/g/yocto-announce/message/243)

Yocto's latest release codenamed 'kirkstone' came out with lot of changes of
which some are not backward compatible, e.g. replacing override separate literal
( \_ to : ) and many of the changes implement inclusive language related
changes. License types have been improved to be more accurate e.g. BSD linceses
are now specified with sub-categories e.g BSD-3-clause. All recipes now follow
SPDX naming convention for LICENSE field. Reproducible builds are turned on by
default. Here are
[migration](https://docs.yoctoproject.org/migration-guides/migration-4.0.html)
notes which are quite handy when upgrading from a prior version of the project.

## Yoe Distro switches to using usrmerge distro feature

`usrmerge` is denoting a file heirarchy where `/bin` and `/lib` etc. are merged
into their `/usr` counterparts. This ensures that `/usr` contains everything
that a base OS needs and thusly can be made read-only or manupulated easily for
other kind of policies. There have been extensive debates about it in various
Linux distribution communities. OpenEmbedded has made this as a feature which is
not turned on by default but can cleanly switch distributions to use it. The Yoe
distro has started using this feature and 2022.04 will be first release to use
this feature. Further, we would like to use read-only rootfs with r/w overlay
from `/data` partition, this will help in ensuring that OS remains immutable and
can be factory resetted easily, but also give flexiblity to have config files
kept peristent over OS upgrades, since in Yoe we use the Yoe updater which does
full system upgrades.

---

# IoT

Simple IoT
[v0.0.45 has been released](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.45).
This version includes NATS API improvements, store and view app and OS version
in root node, package upgrades, and addition of a user auth NATS API.

---

# Other

Caddy
[v2.5.0 has been released](https://community.tmpdir.org/t/caddy-notes/116/5?u=cbrake).
The release notes mention Tailscale integration, which is interesting.

---

# Quote for the week

> Documentation is the castor oil of programming. Managers think it is good for
> programmers and programmers hate it!. - Gerald Weinberg

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
