+++
title = "📆 TMPDIR Weekly - #61, Yoe release, SIOT schedules, Don't use localhost"
date = 2023-09-04
+++

Hello,

Welcome to the 61st issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> ...if you're afraid to change something it is clearly poorly designed. --
> Martin Fowler

---

# Yoe 2023.08 release 🚀

The latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/29)
is available now. This release contains a major rework of the projects structure
where projects are now moved into the toplevel conf directory and corresponding
envsetup changes are also made to detect projects. This release also has
toolchain component upgrades e.g. binutils 2.41, Go - 1.20.7, glibc 2.38. The
rPI4 reference image `yoe-kiosk-image` is now using QT6 instead of QT5. The Go
runtime has been fixed such that Go applications do not crash the Go linker
during build on ARM64. A spurious rebuilt of Clang compiler when multilib is
enabled is fixed as well.

---

# Creating an Alarm Clock with Simple IoT schedules

The following video demonstrates how SIOT can be used to automate things on a
schedule.

[![siot-video](https://community.tmpdir.org/uploads/default/original/2X/4/4c419965742a1d986c493d952c7d9406134c0225.jpeg)](https://community.tmpdir.org/t/creating-an-alarm-clock-with-simple-iot-schedules/1130)

# [Don't use localhost](https://community.tmpdir.org/t/dont-use-localhost/1137)

This article describes how not using localhost will make your applications
simpler and more robust.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
