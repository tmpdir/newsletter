+++
title = "TMPDIR Weekly - #43, October 15, 2022"
date = 2022-10-15
+++

Hello,

Welcome to the 43rd issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## [Yoe Distribution 2020.09 Release](https://community.tmpdir.org/t/yoe-distribution-releases/239/17)

Yoe Distribution lives on master branch of various layers and releases with a
monthly release cycle. 2020.09 was released at the beginning of October, with
lot of new stuff. It can now build weston based graphical images using musl C
library for i.MX8 based images using the gcompat porting library that allows
some glibc APIs to translate to musl APIs. It has also switched to using ZSTD
for compression technology used inside ipk packages, which speeds up image build
times. Optimized version of VLC and ffmpeg for rPI was added. Clang-15 was
updated to latest minor release on 15.x release series.

---

# IoT

# Simple IoT [v0.5.1](https://github.com/simpleiot/simpleiot/tree/v0.5.1)

We have been focused on a use case where we need to stream high rate (15KHz)
data over USB from a MCU device, so the focus has been improving the Serial MCU
client, figuring out ways to handle high rate data, and a number of bug fixes.

- handle config changes in influx db client
- lifecycle improvements
  - fix race condition in http api shutdown
  - shutdown nats client after the rest of the apps
  - store: close NATS subscriptions on shutdown
- Added Signal generator -- can be used to generate arbitrary signals
  (currently, high rate Sine waves only)
- add NATS subjects for high rate data (see
  [API](https://github.com/simpleiot/simpleiot/blob/v0.5.1/docs/ref/api.md))
- add
  [test app](https://github.com/simpleiot/simpleiot/blob/v0.5.1/cmd/point-size/main.go)
  to determine point protobuf sizes
- fix synchronization problem on shutdown -- need to wait for clients to close
  before closing store, otherwise we can experience delays on node fetch
  timeouts.
- fix issue when updating multiple points in one NATS message (only the first
  got written) (introduced in v0.5.0)
- Serial MCU Client:
  - added debug level for logging points and
    [updated what logging levels mean](https://docs.simpleiot.org/docs/user/mcu.html).
  - don't send rx/tx stats reset points to MCU
  - support high-rate MCU data (set message subject to `phr`).

---

# Other

## [Knowledge management systems](https://community.tmpdir.org/t/notion-productivity-software/664/2)

We have been using various note taking systems for reminders, remembering key
points, meeting minutes and so on. There are many systems out there, from simple
text files stored locally, to cloud hosted, feature rich systems offering not
only storing and logging key points, but also organising information in certain
ways that makes it easy to recall relevant portions easily or compose ideas into
larger ideas by defining relationships between notes. It's as if creating an
offline brain system. These tools can be the difference between a successful
person and a not so successful person in future as humans become increasingly
knowledge workers. However, it does have
[cognitive overhead](https://sashachapin.substack.com/p/notes-against-note-taking-systems);
therefore, it becomes a matter of balancing act, where optimum time is spent

---

# Quote for the week

> Go as far as you can see; when you get there, you’ll be able to see further.
> -Thomas Carlyle

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
