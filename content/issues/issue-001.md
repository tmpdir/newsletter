+++
title = "TMPDIR Weekly - #1, July 24th, 2021"
date = 2021-07-24
+++

Welcome to the 1st issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

Going forward, we have merged our Yoe Distribution and TMPDIR newletters into
one newletter. If you were previously subscribed to the Yoe Distribution
newsletter, you will now receive the TMPDIR newsletter instead, which will
include information about the Yoe DIstribution and Yocto in general.

### Yoe [2021.06 (galician)](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.06) release

- Added support for new RISCV based SBC beagleV
  (https://beagleboard.org/beaglev)
- Switch default C/C++ compiler to be Clang and use LLVM C/C++ runtime by
  default.
- Fixed EGLFS QT builds for rockchip rk3399.
- Enable MTE (memory tagging extension) support for Aarch64 in libc. This
  assists in detecting memory problems. See https://lwn.net/Articles/834289/ for
  more information.
- See [release](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.06)
  for a more extensive list of changes.

---

## IoT

### Simple IoT [v0.0.30](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.30) release

It has been awhile since our last release (3mo), but we've been working hard on
some core features. This release adds support for distributed Simple IoT
instances and enables scenarios such as running a Simple IoT instance at the
edge that is connected to another instance in the cloud. Data is efficiently
synchronized in both directions. See [this video](https://youtu.be/6xB-gXUynQc)
for a short demo to see this in action.

---

## Other

### Podcast: [A Day in the Life of a Yoe Developer](https://tmpdir.org/012/).

In this episode, Khem describes how he develops and tests the Yoe distribution.

### [KiCad v6 is getting closer to release](https://community.tmpdir.org/t/kicad-v6-is-getting-closer-to-release/278)

### [Book review: The Lean Startup](https://community.tmpdir.org/t/book-review-the-lean-startup/275)

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
