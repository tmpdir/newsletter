+++
title = "📆 TMPDIR Weekly - #69, Elon Musk book review, Yocto 5.0 LTS updates"
date = 2024-02-16
+++

Hello,

Welcome to the 69th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> If everyone is moving forward together, then success takes care of itself. --
> Henry Ford

---

# Yocto project 5.0 LTS updates

Next release for yocto project is 5.0, codenamed 'scarthgap' is progressing
well, major upgrades have landed in past week enabling glibc-2.39, there were
issues discovered with clone3 on mips32 which has been amicably resolved.
Binutils has been upgraded to 2.42 as well, LLVM/Clang 18.1 is also in testing
and will make into the release. An interesting discussion on
[bitbake performance regression](https://lists.openembedded.org/g/bitbake-devel/topic/103680528#15880)
is ongoing, some profiling has resulted in interesting information which led to
bunch of patches to improve the situation, the patches are still being worked on
and hopefully help in resolving some of the performance issues.

---

# [Book review: Elon Musk](https://community.tmpdir.org/t/book-review-elon-musk/1232)

Walter Isaacson's [recent book](https://isaacson.tulane.edu/) on Elon Musk is an
interesting look into a complex character who is shaping technology today.
Walter shadowed Elon for two years, sitting in on meetings and personal
conversations. He also talked with many people (both personal and business) Elon
has interacted with over his life.

https://isaacson.tulane.edu/

This review is not an endorsement of all things Elon, but rather an attempt to
learn. Elon's approach has elements that are similar to others. Tesla's "you
design it, you manufacture it" reminds me of Amazon's
["you build it, you run it"](https://queue.acm.org/detail.cfm?id=1142065)
philosophy. Elon's conclusion that automation should be introduced late in the
process reminds me of
[Toyota's approach](https://community.tmpdir.org/t/the-secrets-behind-toyotas-bullet-proof-reliability/1227/2?u=cbrake).

Quotes from the book:

[read more ...](https://community.tmpdir.org/t/book-review-elon-musk/1232)

---

# [How to npm install global not as root](https://community.tmpdir.org/t/how-to-npm-install-global-not-as-root/1230)

https://stackoverflow.com/questions/18088372/how-to-npm-install-global-not-as-root

create `~/.npmrc`:

```
prefix=/home/cbrake/.npm-global
```

in `.bash_profile`:

```
export PATH=~/.npm-global/bin:$PATH
```

Then, when you run `npm install -g`, packages are installed in your home dir
instead of asking you for the root password to install in a system directory.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
