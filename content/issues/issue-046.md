+++
title = "TMPDIR Weekly - #46, Yoe 2022.11, SIOT v0.6.1, Y2038, writing"
date = 2022-12-03
+++

Hello,

Welcome to the 46th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## Y2038 in Yocto

As many desktop and server distributions are getting rid of 32bit architectures,
it's not happening in the embedded space any time soon. The Yocto project
supports ARM, MIPS, PPC32, x86 which are all 32bit. There are discussions and
proposals on how to tackle the Y2038 problem. Y2038 problem in nutshell is an
issue when UNIX time will overflow on machines storing time in 32bit integer.
64-bit `time_t` support has been added to kernel and glibc well in time, it's
time to start enabling it for building full systems, for that a discussion on
proposal to fix this in OE is happening on
[architecture mailing lists](https://lists.openembedded.org/g/openembedded-architecture/topic/95354042#1683).
We will need to enable largefile support ( LFS64 ) especially 64bit `off_t` as a
pre-requisite for this some patches as been proposed to enable
`"-D_TIME_BITS=64 -D_FILE_OFFSET_BITS=64"` via configuration metadata globally
in TARGET_CFLAGS. This is fine however it will be beginning of fixing packages
to support 64-bit time_t for a preview of what these
[problems](https://github.com/mkj/yocto-y2038) can look like.

## [Yoe Distro 2022.11 Release](https://community.tmpdir.org/t/yoe-distribution-releases/239/19)

Yoe released 2022.11 monthly release. Python 3.11 needed several packages fixed
which are available in this release. Nodejs, clang and rust compiler has been
upgraded. Sip3 was removed because it's not portable to python 3.11 this is used
in meta-qt5 package python3-pyqt5 which will not work anymore with this release.

---

# IoT

## [Simple IoT release v0.6.1 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.6.1)

- fix bug in influx db client due to recent API changes
- fix bug in client manager where Stop() hangs if Start() has already exited
- don't allow deleting of root node
- allow configuring of root node ID, otherwise, UUID is used
- sync:
  - add option to configure sync period (defaults to 20s).
  - if upstream node is deleted on the upstream, it is restored
  - don't include edge points of root node in hash calculation. This allows node
    to be moved around in the upstream instance and other changes.

## SIOT CAN Client

Collin has been
[working on a CAN client](https://github.com/simpleiot/simpleiot/pull/442) for
Simple IoT, and a [canparse](https://github.com/simpleiot/canparse) library for
parsing CAN Database files (KCD, etc). This is a great addition to SIOT and will
allows us to use SIOT in all kinds of vehicles where CAN is prevalent.

---

# Other

## [Paul Graham on Writing](https://community.tmpdir.org/t/paul-graham-on-writing/773)

This is a collection of Paul Graham's essays that address the topic of writing,
and why this is such a valuable activity.

---

# Quote for the week

> Programs must be written for people to read, and only incidentally for
> machines to execute. -- Abelson & Sussman, SICP, preface to the first edition

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
