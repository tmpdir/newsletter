+++
title = "TMPDIR Weekly - #13, October 30th, 2021"
date = 2021-10-30
+++

Welcome to the 13th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Linux

[Yocto 3.4 ( honister )](https://lists.yoctoproject.org/g/yocto/topic/announcement_yocto_project/86592579?p=,,,20,0,0,0::recentpostdate/sticky,,,20,2,0,86592579,previd=1635369242097860464,nextid=1635208395419621271&previd=1635369242097860464&nextid=1635208395419621271)
has been released. It is massive amount of recipe updates in core layer
approximatetely 280 out of 800 odd packages. Next release 3.5 is codenamed
'kirkstone' is scheduled in April 2022 and will be second LTS in series. Support
for Rust has been added in this release. Software bill-of-materials (SBOM) based
on SPDX has also been integrated.

---

# IoT

Simple IoT is now even easier to embed in your Go application as there is now a
[single entry point](https://pkg.go.dev/github.com/simpleiot/simpleiot@v0.0.37#Start)
for starting the SIOT server. This entry point returns a NATS connection which
you can use to interact with the SIOT store via convenience functions in the
SIOT
[nats package](https://pkg.go.dev/github.com/simpleiot/simpleiot@v0.0.37/nats).
At this point, the preferred API for SIOT is NATS. There are several advantages
to this:

- we have visibility into all data changes
- clients can easily subscribe to data changes
- there are NATS
  [client packages/libraries](https://docs.nats.io/nats-protocol/nats-protocol/nats-client-dev)
  for many languages

We been integrating Simple IoT into a customer's Go application to replace the
data store and add distributed data sync, so the question crossed my mind -- how
many hours do I have in this project. Since, I track all my time, it was simple
to run a report -- currently totals 1020.5 hours. Not a huge amount as it's
mostly one guy's spare time, but I've been at it since 2018-11, so little bits
add up. -- cb

---

# Other

There are now serious ARM based server/workstation
[available](https://community.tmpdir.org/t/arm-based-desktop-server/343) with 32
cores. This machine uses an Ampere CPU, as does the CI machine we use to test
the Yoe Distribution.

In 1980, Tony Hoare (recipient of the Turing award) delivered a lecture titled
[The Emperor’s Old Clothes 7](https://dl.acm.org/doi/pdf/10.1145/358549.358561).
Tony discusses some of his successes and failures, and what he has learned from
them. His conclusions ring true – the importance of simplicity, documentation,
etc. Find notes/discussion about this lecture
[here](https://community.tmpdir.org/t/the-emperors-old-clothes/344).

[Moving Past the Cloud](https://youtu.be/ogOEEKWxevo) is an excellent talk by
Brooklyn Zelenka from the fission project and is a good view into the future of
distributed data and computing. Some things from the talk that stood out:

> - by 2025, 75% of data will be processed outside the traditional data center
>   or cloud
> - Sensor data explosion will kill the cloud – existing infrastructure will not
>   be able to handle the volumes or the rates
> - permissions and encryption are easy to handle with a tree of data – you can
>   give permissions at any point in the tree.
> - For syncing data, a set is a lot more efficient than a log or graph.
> - some things like authentication actually get simpler and the parties
>   involved are more in control

It seems with the Simple IoT project, we are somewhat aligned with this vision
as we are focusing on a distributed, edge centered architecture, handling
permissions in a tree, etc. However, our data syncing is graph based, so there
may be better ways to do this with sets. We also don’t have a good peer model
yet – distributed operation is only upstream to larger instances, etc.

It seems clear that the future is not heavy, cloud-centered architectures that
are dependent on one vendor, but rather flexible architectures where you can own
the platform, and can organize it any way you like.

---

# Quote for the week

> I gave desperate warnings against the obscurity, the complexity, and
> over-ambition of the new design, but my warnings went unheeded. I conclude
> that there are two ways of constructing a software design: One way is to make
> it so simple that there are obviously no deficiencies and the other way is to
> make it so complicated that there are no obvious deficiencies.
>
> The first method is far more difficult. It demands the same skill, devotion,
> insight, and even inspiration as the discovery of the simple physical laws
> which underlie the complex phenomena of nature. It also requires a willingness
> to accept objectives which are limited by physical, logical, and technological
> constraints, and to accept a compromise when conflicting objectives cannot be
> met. -- Tony Hoare

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
