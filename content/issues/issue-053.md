+++
title = "📆 TMPDIR Weekly - #53, Yoe i.MX8 support, Go lint, CAN"
date = 2023-03-20
+++

Hello,

Welcome to the 53rd issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> The key to performance is elegance, not battalions of special cases. –-Jon
> Bentley and Doug McIlroy

---

# Linux

## [Yoe Updater on iMX8 platforms](https://github.com/YoeDistro/yoe-distro/pull/767)

NXP iMX and iMX8 in particular is one of the best-supported SOC families in Yoe
distro. Recently, we have added support to the Yoe updater as well to support
MX8 platforms, beginning with
[VAR-SOM-MX8](https://www.variscite.com/product/system-on-module-som/cortex-a72/var-som-mx8-nxp-freescale-i-mx8/#evaluation-kit).
It can be easily configured to update either eMMC or SD card based upon the need
to use either of them as default boot media for system. Updater also has an
installed which can partition the eMMC into BOOT/ROOT/DATA partitioning scheme
and provision initial factory
[image](https://github.com/YoeDistro/yoe-distro/pull/768). The factory image
`yoe-installer-image` can be built and the resulting wic image flashed into SD
card which is used to provision the eMMC storage media. Once provisioned, normal
OTA functionality is available where it can update the system while keeping
`/data` parition intact. It can use updater artifacts from `/data` parition
which is primarily used for network-based updaters (FOTA) or from physical media
e.g. USB stick or SD card formatted with VFAT or any other file systems readable
by Linux. The Yoe updater can also install/update default recovery images in
eMMC hardware partitions `mmcblk0boot0` and `mmcblk0boot1` if needed; however,
that is disabled by default.

## [Yocto switch to using DT_RUNPATH](https://git.yoctoproject.org/poky/commit/?id=ce71bb3d08de649b9099cf45af372fb575b08b8b)

While this might look like a one-liner, this is a significant change in
toolchain defaults. The search order for libraries is such that first DT_RPATH
is searched and then LD_LIBRARY_PATH and finally it looks at DT_RUNPATH. With
this change, the toolchain will emit DT_RUNPATH whenever it was emitting
DT_RPATH, making it harder for malicious shared libraries to inject themselves.
This, however, could also impact some assumptions in applications if they rely
upon DT_RPATHs priority, therefore it will be good to check that in your
applications with the upcoming 4.2 Yocto release where this change will be
available. If reproducible builds are important to you then you might need to
regenerate the shared state cache.

---

# IoT

Work continues with Shelly Device support and reworking of the Serial MCU
[high-rate data](https://github.com/simpleiot/simpleiot/pull/520).

We also
[switched to `golangci-lint`](https://community.tmpdir.org/t/introduction-golangci-lint/963)
for checking/linting in SIOT. This tool is a wrapper around many of the
available code checkers for Go and provides a convenient way to run the most
useful ones in one easy tool. The tooling for Go is very good.

---

# Other

## [CANable CAN adapters](https://community.tmpdir.org/t/canable-can-adapters/962)

While the original CANable device is no longer available, there are numerous
clones available from China. I purchased two of them and they seem to work and
may be a good option for doing CAN development with Linux. Simple IoT already
has [support](https://docs.simpleiot.org/docs/user/can.html) for watching a CAN
bus and capturing data in InfluxDB to graph in Grafana -- it is a quick way to
get CAN data into graphs during development.

![can](https://community.tmpdir.org/uploads/default/optimized/2X/7/7593e0ce2eb83e2804d70a549186b6738202d7df_2_1035x519.jpeg)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
