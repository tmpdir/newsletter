+++
title = "TMPDIR Weekly - #18, December 11th, 2021"
date = 2021-12-11
+++

Welcome to the 18th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

An
[interesting article](https://developers.facebook.com/blog/post/2021/12/09/drgn-how-linux-kernel-team-meta-debugs-kernel-scale/)
about how the Facebook team debugs the Linux Kernel at scale. With the `drgn`
tool, they can obtain stack traces from crash dumps, etc.

There is a lot of cleanup activity (see
[example](https://git.openembedded.org/openembedded-core/commit/?id=2511e937f4454813ab11a59057c29ae3f224ab5e))
in the Yocto project to either upstream or remove old patches. If you know of
patches that need upstreamed, please help out!

The Yoe Distro has updated support for the Graphics drivers for the BeagleBone
Black. Qt5 and other apps now work again. See several relevant PRs:
[622](https://github.com/YoeDistro/yoe-distro/pull/622/files),
[623](https://github.com/YoeDistro/yoe-distro/pull/623/files), and
[627](https://github.com/YoeDistro/yoe-distro/pull/627/files).

Quickemu is an excellent tool for launching a wide variety of different
operating systems quickly, including most Linux distributions, Windows, and
MacOS.
[Try it out](https://community.tmpdir.org/t/quickemu-the-quick-vm-solution/371)
-- it is very neat.

We did a
[quick comparison](https://community.tmpdir.org/t/embedded-linux-image-size/370)
of `yoe-qt5-image` and the Debian Buster IoT image sizes. Tradeoffs to consider.

---

# Other

Discovered some
[more tools for the terminal](https://community.tmpdir.org/t/terminal-tools/374).
All written in Rust, which seems to be a trend.

---

# Quote for the week

> If I had asked people what they wanted, they would have said faster horses. --
> Henry Ford

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
