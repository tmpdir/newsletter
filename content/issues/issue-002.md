+++
title = "TMPDIR Weekly - #2, July 31st, 2021"
date = 2021-07-31
+++

Welcome to the 2nd issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

Yoe Distro 2021.07 release is ready for release next week.

A major change in bitbake syntax is coming which alters the ways how overrides
are specified this change would be included in 2021.08 release at least for
OpenEmbedded-core layer. More details are
[here](https://lists.openembedded.org/g/openembedded-architecture/topic/overrides_conversion_plan/84508148?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,84508148).

GNU C library 2.34 is out, this includes quite a few notable features, this
[article](https://www.phoronix.com/scan.php?page=news_item&px=GNU-C-Library-Glibc-2.34)
is a good read.

Yoe Distro has disabled llvm-pipe for x86/x86_64 qemu builds, as it is requiring
target clang builds which takes a long time to build, this speeds up CI a bit.

---

## IoT

Working on
[schedule conditions](https://github.com/simpleiot/simpleiot/pull/214) for rules
so that we can run rules at specific times, weekdays, or dates. Dealing with
time is always a little complex as you need to take into account time zones. The
best way I've found is to always store time in UTC, but
[convert to local time](https://github.com/simpleiot/simpleiot/blob/feature-schedule/frontend/src/UI/Form.elm#L186)
in the browser before displaying and then back to UTC when saving. This gets
more complex if you have a weekday or date attached because you then need to
check if the time crossed a day boundary, and then adjust the day.

---

## Other

Recently
[set up a Mikrotik router](https://community.tmpdir.org/t/selecting-a-small-office-router/250/9)
for my home office -- highly recommend them if you are looking for something a
little more capable than the typical consumer router.

Microsoft is working on their own Linux distribution called
[CBL-Mariner](https://community.tmpdir.org/t/microsoft-cbl-mariner/283). Having
worked through the [Balmer days](https://en.wikipedia.org/wiki/Steve_Ballmer)
when MS promoted a great deal of Linux/OSS FUD, this is quite something to see

Podcast review:
[Brian Kernighan: UNIX, C, AWK, AMPL, and Go Programming](https://community.tmpdir.org/t/podcast-review-brian-kernighan-unix-c-awk-ampl-and-go-programming/284)

- Brian expresses in a humble way some of the experiences he has been through,
  and some of the history behind the great events in computing (UNIX, C, etc).
- At 20:12, Brian discusses the environment of Bell Labs:
  - _Bell Labs at the time was very special kind of place to work, because there
    were a lot of interesting people, and the environment was very, very open
    and free – was very cooperative environment, was a very friendly
    environment, so if you had an interesting problem, you’d go and talk to
    somebody and they might help you with the solution._
- Around 31:24, he discusses programming – is it art or science. Some of his
  thoughts:
  - **\*art**: figuring out what to do\*
  - **\*science**: how to do it well\*
  - **\*engineering**: working with constraints – not only what is a good
    algorithm, but what is the most appropriate algorithm given the amount of
    time we have to compute, the amount of time we have to program, what’s
    likely to happen in the future with maintenance, who’s going to pick this up
    in the future – all those kinds of things if you’re an engineer you get to
    worry about, whereas if you think of yourself as a scientist, maybe you can
    push those over the horizon in a way, and if you’re an artist – “what’s
    that?”\*

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
