+++
title = "TMPDIR Weekly - #47, SIOT CAN Bus support, Yocto vs Ubuntu"
date = 2022-12-10
+++

Hello,

Welcome to the 47th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480) Welcome to the 47th issue of

Thanks for reading!

Khem and Cliff

---

# Linux

## [Yocto Vs Ubuntu Core](https://canonical.foleon.com/whitepapers/yocto-vs-ubuntu-core/content-index)

This whitepaper discusses use cases of Embedded Linux, especially if developers
should build their own custom embedded Linux distributions using build
infrastructures like Yocto project or rely on commercial-supported Ubuntu-core,
the paper does not consider that there are a number of commercially
well-supported distributions based on the Yocto project e.g. WindRiver Linux,
Montavista Linux, Mentor Graphics Linux, (and of course the
[Yoe Distribution](https://www.yoedistro.org/) to name a few -- for details see
[Operating System Vendors section](https://wiki.yoctoproject.org/wiki/Project_Users).
Criticisms that the article draws about Yocto project are fair. This paper also
means that there are things that the Yocto project is doing right.

## [Using unsigned char defaults in Linux Kernel](https://git.kernel.org/pub/scm/linux/kernel/git/zx2c4/linux.git/commit/?h=unsigned-char&id=3bc753c06dd02a3517c9b498e3846ebfc94ac3ee)

A patchset has been proposed to switch the default char type to be unsigned char
in the kernel via using `-funsigned-char` compiler option explicitly via kbuild.
C standards do not mandate the defaults and leave it to architectures to decide
and sure enough, there are differences e.g. for ARM/PPC default char is unsigned
type, but for x86 it is signed. This change will cause some churn in codebase
since as said in the commit message it will fix some bugs but uncover some at
the same time. I think a better fix would have been to not use naked-char type
and qualify it with unsigned/signed as needed since this would have made it
readable at the same time independent of architecture.

---

# IoT

## [Simple IoT v0.6.2 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.6.2)

- moved the node type from node point to edge field. This allows us to index
  this and make queries that search the node tree more efficient.
- support for processing clients in groups. Previously, client nodes had to be a
  child of the root device node.
- fix issue with `siot log` due to previous NATS API change

The store and sync code is hopefully nearing production quality. At this point,
most of the know issues have been worked through.

## [Simple IoT v0.7.0 release -- CAN bus support](https://github.com/simpleiot/simpleiot/releases/edit/v0.7.0)

- add [CAN bus client](https://docs.simpleiot.org/docs/user/can.html)
- add File node (can be used to upload and store file data)

SIOT can now help make your vehicle application development easier.

---

# Other

## [Go for IIoT Systems](http://bec-systems.com/site/2048/go-for-iiot-systems)

This article explains the many benefits of using Go for IIoT systems and is
based on our experience in multiple projects over the past 6 years.

---

# Quote for the week

> The better you become at writing, the more the world conspires to prevent you
> from writing. -- Ryan Holiday

(I think this probably holds true for many things ...)

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
