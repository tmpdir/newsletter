+++
title = "TMPDIR Weekly #45, Yoe 2022.10, SIOT v0.6.0"
date = 2022-11-26
+++

Hello,

Welcome to the 45th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## [Yoe Distro 2022.10 Release](https://community.tmpdir.org/t/yoe-distribution-releases/239/18)

Yoe Distro 2022.10 was released. This release had fewer changes on average since
this release is parallel to yocto 4.1 `langdale` release. Next release should
have more changes and will have layers beyond langdale release. This release
includes the Clang 15.0.3 bugfix release. Starfive BSP is refreshed and uses
linux 6.0.0. Fixes to build Odroid-C2 with GCC 12 were added as well.
`-ffile-compilation-dir` clang compiler option is added to remove absolute
buildpaths from binaries is removed as it was not needed due to
-fdebug-prefix-map option being fixed to work properly in clang.

## [bitbake cache improvements](https://lists.openembedded.org/g/bitbake-devel/message/14102)

Now that yocto 4.1 ( langdale ) is released, time is good for making major
changes and one such change is trying to optimize bitbake cache handling. The
changes currently are work-in-progress but seems to improve the cache size held
in memory although with a little regression in parse time.

---

# IoT

## [Simple IoT v0.6.0 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.6.0)

This release completes the rewrite of the synchronization code that includes a
[rolling hash](https://community.tmpdir.org/t/hash-algorithms/668), tests, and
lots of other improvements. We've also started splitting subcommands for the
`siot` command line -- so far we we have `serve`, `log`, and `store`.

---

# Other

## [What Hunter-Gatherers Can Teach Us About the Frustrations of Modern Work](https://www.newyorker.com/culture/office-space/lessons-from-the-deep-history-of-work)

An interesting article by Cal Newport (author of Deep Work).

## [Book review: The Minimalist Entrepreneur](https://community.tmpdir.org/t/book-review-the-minimalist-entrepreneur/717)

This book provides a nice blueprint for starting a sustainable business.

Chapters:

- Profitability first
- Start with Community
- Build as Little as Possible
- Sell to your first hundred customers
- Market by being you
- Grow yourself and your business mindfully
- Build the house you want to live in

## [ OEDvM 2022.11 ](https://lists.openembedded.org/g/openembedded-architecture/message/1661)

Upcoming OE developer meet is scheduled on 02-December-2022, Its open for all.
It is an interesting event for users and developers of OE/Yocto based
distributions

---

# Quote for the week

`Any fool can criticize, condemn, and complain -- and most fools do. -- Dale Carnegie`

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
