+++
title = "TMPDIR Weekly - #40, September 17th, 2022"
date = 2022-09-17
+++

Welcome to the 40th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Yoe upgrades to clang 15.0.0

The Yoe 2023.09 release will be built with brand new clang 15.0.0. There is a
lot of work that has gone into slowly fixing packages in layers eg.
`meta-openembedded` `poky` and some of the BSP layers. `meta-freescale` has
forks of gstreamer 1.20 and weston which also needed
[fixed](https://github.com/Freescale/meta-freescale/pull/1205).

## Experiments with LVGL – Light and Versatile Graphics Library

We have been playing with LVGL lately for exploring possibilities of building
native UI elements. Good progress has been made by Cliff as noted
[here](https://community.tmpdir.org/t/lvgl-light-and-versatile-graphics-library/593/10)
Display is working nicely now and we have to get touchscreen and input subsystem
working.

---

# IoT

## [Simple IoT v0.5.0 Release](https://github.com/simpleiot/simpleiot/releases)

**NOTE, this is a testing release where we are still in the middle of reworking
the store and various clients. Upstream functionality does not work in this
release. If you need upstream support, use a 0.4.x release.**

The big news for this release is switching the store to SQLite and moving rule
and db functionality out of the store and into clients.

- switch store to sqlite
  ([#320](https://github.com/simpleiot/simpleiot/issues/320))
- rebroadcast messages at each upstream node
  ([#390](https://github.com/simpleiot/simpleiot/issues/390))
- extensive work on client manager. It is now much easier to keep your local
  client config synchronized with ongoing point changes. Client manager also now
  supports client configurations with two levels of nodes, such as is used in
  rules where you have a rule node and child condition/action nodes.
- fix bug with fast changes in UI do not always stick
  ([#414](https://github.com/simpleiot/simpleiot/issues/414))
- move rules engine from store to siot client
  ([#409](https://github.com/simpleiot/simpleiot/issues/409))
- move influxdb code from store to client package
  ([#410](https://github.com/simpleiot/simpleiot/issues/410))
- replace all NatsRequest payloads with array of points
  ([#406](https://github.com/simpleiot/simpleiot/issues/406))

## Updated Simple IoT Architecture Diagram

We continue to move more funtionality into clients. This simplifies the app and
allows it to be more modular so that you can easily build custom versions that
only include the clients you need for a particular application.

![arch diagram](https://community.tmpdir.org/uploads/default/original/2X/f/f03a29f41d1e57f9f606eb58e4579ac5a7e50802.png)

---

# Other

[Introducing LiteFS](https://fly.io/blog/introducing-litefs/) -- a neat tool
that replicates SQLite databases.

Did you know the new Roc programming language uses both Rust and Zig? Learn more
[here](https://community.tmpdir.org/t/open-source-careers-with-loris-cro/645/4?u=cbrake).

---

# Quote for the week

> Three rules of work: out of clutter find simplicity. From discord find
> harmony. In the middle of difficulty lies opportunity - Albert Einstein

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
