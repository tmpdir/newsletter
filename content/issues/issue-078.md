+++
title = "📆 TMPDIR Weekly - #78, Yoe 2024.05 release, Update UI, Zephyr on ESP32 🚀"
date = 2024-05-31
+++

Hello,

Welcome to the 78th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> It does not matter how slowly you go so long as you do not stop. -- Confucius

---

# Yoe 2024.05 release 🚀

[May release of Yoe Distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/38)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components.

- GCC has been upgraded to 14.1 which is a major release, while yoe distro uses
  clang for most of C/C++ packages, there still are some components which still
  need GCC e.g. GNU C library, Linux Kernel while can be built with LLVM these
  days, yocto still defaults to use GCC and so does Yoe.
- LLVM Linker ( LLD ) is now used as default linker for RISCV targets, LLD is
  used as default linker in yoe for other architectures, it was not used for
  RISCV until now due to few issues in RISCV related relocations and missing
  inker relaxations which are now available
- SimpleIOT is upgraded to 0.16.0 release, which implements integration with yoe
  updater eventually this can be an integrated OTA for Yoe users.
- A new versioning scheme YY.MM.VER is adopted for images e.g. 24.05.x which can
  make it easy to identify the images coming from given release
- RPI machines using systemd were impacted by network interface renaming seen
  with kernel 6.6 is fixed.
- Yocto has introduced a new UNPACKDIR for containing the output of unpack task,
  earlier it was mix of source (S) and workdir (WORKDIR), this change does mean
  that recipes needed adjusting where S or WORKDIR were used and actually were
  pointing to artifacts of unpack tasks.

---

# The Yoe release now includes an update user interface

The Yoe distro comes bundled with Simple IoT and optionally the Yoe Kiosk
Browser. There is now a user interface to manage updates:

![update ui](https://community.tmpdir.org/uploads/default/optimized/2X/9/9e9662a04eca135f638ee29dbf7df3bf57819a9d_2_602x1000.png)

You can point your device at the BEC update server (or any HTTP server) by
setting the Update server to:

[https://files.bec-systems.com/yoe/testing/](https://files.bec-systems.com/yoe/testing/)

You can manage updates locally, or if you
[sync your device to a cloud instance](https://docs.simpleiot.org/docs/user/sync.html)
of SIOT, you can manage updates from the cloud as well.

We are still in the process of deploying updates for other supported machines.

While there are other more sophisticated update projects, the
[Yoe Updater](https://github.com/YoeDistro/yoe-distro/blob/master/docs/updater.md)
in combination with the SIOT
[update client](https://docs.simpleiot.org/docs/user/update.html) provides a
simple solution that will work with any HTTP server -- no complicated sever
setup is required. This simple solution is robust and is adequate for many
products.

---

# Zephyr on the ESP32

We started exploring the use of Zephyr on the ESP32 (both Xtensa and RISC-V
variants). So far it is looking good!

[(read more ...)](https://community.tmpdir.org/t/zephyr-on-the-esp32/1310)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
