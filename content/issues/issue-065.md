+++
title = "📆 TMPDIR Weekly - #65, Yoe/SIOT releases 🚀, YAGNI"
date = 2024-01-05
+++

Hello,

Welcome to the 65th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Happiness is when what you think, what you say, and what you do are in
> harmony. -- Mahatma Gandhi

---

# Best Wishes in the New Year

![path woods](https://files.bec-systems.com/misc/path-woods.png)

May your path be full of joy, peace, and fulfillment.

Thank you for a great 2023.

Looking forward to building in 2024!

---

# Yoe 2023.12 release 🚀

[Last release of 2023](https://community.tmpdir.org/t/yoe-distribution-releases/239/33)
is available. As is usual with rolling release, useful features and fixes keep
rolling in.

- Clang compiler was updated to 17.0.6 which is a bugfix release in 17.x series.
  LLD linker was disabled for x32 ABI, it does not have support to be enabled as
  system linker.
- Odroid-C4 build is cleaned up and mesa-gl mali-g31 drivers can happily live
  together. yoe-kiosk-image is building and booting.
- GDB 14.1 bring major debugger release into distro
- Rust upgraded to 1.71.1 and go compiler to 1.20.12
- ptest runs are restored in CI, as a result several ptest related fixes went
  in, eg. c-ares now have ptests. bpftrace ptests are fixed
- Enable Nvidia tegra platforms to build with musl C library, It is limited to
  headless systems for now. Getting Nvidia GPU drivers to work with musl needs
  some work. Added a project for orin-nano with
  [SSD instead](https://community.tmpdir.org/t/nvidia-jetson-orin-nano-devkit-notes/1079/10?u=khem)
  of uSD
- RISCV reference platform VisionFive2 switched to use 6.1 kernel from 5.10

---

# [SIOT v0.14.5](https://github.com/simpleiot/simpleiot/releases/tag/v0.14.5)

- simpleiot-js: Fixed bugs and improved README
- Replace deprecated `io/ioutil` functions
  ([#680](https://github.com/simpleiot/simpleiot/pull/680))
- fixed frontend bug where only custom node types could be added

---

# [Evaluating NATS Jetstream as a store for Simple IoT](https://community.tmpdir.org/t/nats-notes-tips/340/7?u=cbrake)

The
[nats-exp](https://github.com/simpleiot/nats-exp)[GitHub - simpleiot/nats-exp](https://github.com/simpleiot/nats-exp)
repo has several experiments where we attempt to determine if we can use NATS
Jetstream as a store for Simple IoT. So far, it looks promising.

## Time-series metrics:

```
2024/01/03 14:20:10 NATS TSD experiment
2024/01/03 14:20:10 js.Publish insert rate for 10,000 points: 42028 pts/sec
2024/01/03 14:20:10 js.PublishAsync insert rate for 10,000 points: 412363 pts/sec
2024/01/03 14:20:10 nats.Publish insert rate for 10,000 points: 17937960 pts/sec
2024/01/03 14:20:10 ack: false, Get 30000 points took 0.04, 668062 pts/sec
2024/01/03 14:20:10 ack: true, Get 30000 points took 0.04, 827364 pts/sec
2024/01/03 14:20:10 ack: false, Get 30000 points took 0.03, 881960 pts/sec
```

Seems fast enough.

## Syncing streams between hub and leaf nodes:

```
[cbrake@quark nats-exp]$ go run ./bi-directional-sync/
=====================================================
       Create stream on hub and source to leaf
=====================================================
* Create stream: server:hub domain:hub stream:NODES-HUB subject:n.hub.>
* Publish: hub -> n.hub.123.value -> 12 13 14 15
* Stream count: server:hub domain:hub stream:NODES-HUB count:4
* Stream count: server:leaf domain:hub stream:NODES-HUB count:4
* Source stream: server:leaf domain:leaf stream:NODES-HUB source-domain:hub subject:n.hub.>
* Stream count: server:leaf domain:leaf stream:NODES-HUB count:4
=====================================================
       Create stream on leaf and source to hub
=====================================================
* Create stream: server:leaf domain:leaf stream:NODES-LEAF subject:n.leaf.>
* Publish: leaf -> n.leaf.456.value -> 22 23 24 25 26
* Source stream: server:hub domain:hub stream:NODES-LEAF source-domain:leaf subject:n.leaf.>
* Stream count: server:hub domain:hub stream:NODES-LEAF count:5
=====================================================
       Shut down leaf node
=====================================================
* Stream count: server:hub domain:hub stream:NODES-LEAF count:5
* Publish: hub -> n.hub.123.value -> 16 17 18 19
* Stream count: server:hub domain:hub stream:NODES-HUB count:8
=====================================================
       Start up leaf node
=====================================================
* Stream count: server:leaf domain:leaf stream:NODES-HUB count:8
* Publish: hub -> n.hub.123.value -> 20 21 22 23
* Stream count: server:leaf domain:leaf stream:NODES-HUB count:12
=====================================================
       Publish more messages to leaf
=====================================================
* Publish: leaf -> n.leaf.456.value -> 27 28 29 30 31
* Stream count: server:leaf domain:leaf stream:NODES-LEAF count:10
* Stream count: server:leaf domain:leaf stream:NODES-LEAF count:10
```

---

# [YAGNI: You Ain’t Gonna Need It and Cake models](https://community.tmpdir.org/t/yagni-you-aint-gonna-need-it-and-cake-models/1217)

[YAGNI](https://en.wikipedia.org/wiki/You_aren't_gonna_need_it) is a principle
that arose from extreme programming which states you should not add
functionality until you need it. I learned about this from a
[recent elm radio episode](https://elm-radio.com/episode/avoiding-unused-code/):

This leads us to two approaches to programming:

1. The Layered Cake Approach
2. The Sliced Cake Approach

[Read more ...](https://community.tmpdir.org/t/yagni-you-aint-gonna-need-it-and-cake-models/1217)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
