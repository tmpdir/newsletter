+++
title = "TMPDIR Weekly - #16, November 20th, 2021"
date = 2021-11-20
+++

Welcome to the 16th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

OE developers meeting is
[announced](https://lists.openembedded.org/g/openembedded-architecture/topic/openembedded_developer/87106634?p=,,,20,0,0,0::recentpostdate/sticky,,,20,2,0,87106634,previd=1637100684708982914,nextid=1623896563542799762&previd=1637100684708982914&nextid=1623896563542799762)
and it will include interesting discussions around many topics impacting
embedded linux development.

---

# IoT

Simple IoT
[v0.0.38 released](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.38)
this week. This release concludes our
[first Architectural Decision Record](https://github.com/simpleiot/simpleiot/blob/master/docs/adr/1-consider-changing-point-data-type.md)
(ADR), and lays the groundwork for a flexible and reliable system.

---

# Other

I am generally interested in energy aware computing. There are interesting
efforts around it in various places. Linux kernel also has worked on energy
aware schedulers especially for heterogeneous cores like am big.LITTLE
architecture. This is an
[interesting paper](https://community.tmpdir.org/t/save-the-planet-program-in-c-avoid-python-perl/363)
around energy used for programs written in different languages.

Julia is an interesting programming language, even though its prevalent mainly
in research communities but, I am hoping some of its strengths are worth noting.
Perhaps might cross pollinate into go, etc. This is an interesting
[article on concurrency](https://community.tmpdir.org/t/concurrency-in-julia/361)
in Julia.

[This presentation on rethinking time](https://community.tmpdir.org/t/rethinking-time/358)
is thought provoking.

The Rust compiler
[has been getting faster](https://community.tmpdir.org/t/rust-notes/360). This
is good news as the compile speed is one of the drawbacks of Rust compared to
languages like Go.

---

# Quote for the week

> Simplicity is the ultimate sophistication. -- Leonardo da Vinci

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
