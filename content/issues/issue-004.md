+++
title = "TMPDIR Weekly - #4, August 14th, 2021"
date = 2021-08-14
+++

Welcome to the 4th issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

Recent updates in the OE/Yocto ecosystem:

- Override Changes - What it means is that the separator for overrides is
  changed from '_' to ':', this would require changes in layers so support
  current master and newer releases, currently maintained releases have patched
  bitbake to accept new syntax as well, which means it will work with
  dunfell(3.1) and hardknott ( 3.3) releases as well. There is a python script
  `scripts/contrib/covert-overrides.py` in openembedd-core repo which can be
  used to convert a layer to use new syntax, this is not straightforward if
  layer is using its own OVERRIDES, they should be added to this script before
  running the conversion, so it does not miss them, nevertheless it gives a good
  starting point to begin the conversion. This change will avoid the conflict
  where '_' is an accepted character in variable names as well as its an
  override separator, this will now improve the situation in lot of areas.
- [MAINTAINERS file](https://git.openembedded.org/openembedded-core/tree/MAINTAINERS.md)
  added, this lists relevant developers primarily responsible for reviewing
  patches and contributing, this is however not exclusive -
- READMEs have started using mark down syntax
- [The future of BeagleV](https://forum.beagleboard.org/t/the-future-of-beaglev-community/30463/8)
- Core system updates - GLIBC 2.34, GCC 11.2
- Several fixes made to make LTO enabled build be reproducible.

---

## IoT

### Simple IoT release [v0.0.33](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.33)

This release provides two additions to the SIOT rules engine. 1st is the ability
to have schedule driven rule conditions:

![schedule](https://gallery.tinyletterapp.com/bbd6b58b96b191777482dd0d3d1ff970ec04b2af/images/74678bad-21b3-d89c-8e22-e12cc90d7de4.png)

Handling time is fairly challenging when dealing with time zones. SIOT display
local time in the browser, but stores UTC time in the database so that the
system operates correctly, even if configured from a remote location.

The 2nd feature is to play audio in rule actions:

![img](https://gallery.tinyletterapp.com/bbd6b58b96b191777482dd0d3d1ff970ec04b2af/images/d1617b1a-55e5-70ba-fc39-c6cfbeee1c3b.png)

This feature only works on Linux and requires alsa-utils to be installed. The
use-case that drove this feature was a building automation system where we
wanted to play sounds in the building ceiling speakers.

---

## Other

### [Analytic Framework for Cyber Security (Keynote)](https://community.tmpdir.org/t/analytic-framework-for-cyber-security-keynote/290)

Very interesting presentation about cyber security and relevant for about any
connected computer system. More notes
[here](https://community.tmpdir.org/t/analytic-framework-for-cyber-security-keynote/290).

### [Executive Order | How To Help Companies Generate SBOMs To Enhance Security](https://www.youtube.com/watch?v=eSA9pBwa6aw)

In this episode of Let’s Talk, Kate Stewart, VP of Dependable Embedded Systems
at The Linux Foundation, joins me to talk about Biden administration’s executive
order on cybersecurity that specifically mentions SBOMs (Software Bill of
Materials), which has been one of the major focus areas for many Linux
Foundation projects, including SPDX and Zephyr, to name a few.

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
