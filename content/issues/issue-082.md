+++
title = "📆 TMPDIR Weekly - #82, Running LLMs on Microcontrollers 🎙️"
date = 2024-07-22
+++

Hello,

Welcome to the 82nd issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

> You either have to be first, best, or different. -- Loretta Lynn

---

# 🎙️ [Running LLMs on Microcontrollers](https://tmpdir.org/033/)

In this episode, we discuss aspects running AI on edge systems (both
microcontrollers and embedded Linux) with Chris Cole. Chris has a lot of
experience doing this and has used AI at the edge in multiple projects to solve
real problems. This episode provides an overview of the technology and will help
you understand the steps to implementing an AI model in your system.

[Listen here](https://tmpdir.org/033/)

---

# Instead of commenting on a PR, open a branch

Gumroad has adopted an interesting idea:

> For example, instead of writing a comment on a pull request (PR), engineers
> and designers are now encouraged to commit to a branch instead of leaving
> comments.

There are often better ways to do things and this is an example of how you can
streamline the feedback process. Yes, it takes some humility to be on the
receiving end of this type of process, but all great things do.

[discuss](https://community.tmpdir.org/t/instead-of-commenting-on-a-pr-open-a-branch/1349)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
