+++
title = "TMPDIR Weekly - #22, January 29, 2022"
date = 2022-01-29
+++

Welcome to the 22nd issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

[fbdev](https://en.wikipedia.org/wiki/Linux_framebuffer) has been mainstay of
many headed embedded systems. In recent years however it has fallen off
maintenance and has been lacking development a bit as more focus has been on
other display subsystems. In our projects, we have seen constant deployment of
devices which needed fbdev since they were using some sort of small LCD
touchscreen connected over GPIO, SOC display controller, etc. It is good news at
least from embedded system vantage point. LWN has a good
[discussion](https://lwn.net/Articles/881827/) on recent fbdev developments.

---

# GitPLM

[v0.0.11](https://github.com/git-plm/gitplm/releases/tag/v0.0.11) adds support
for a checked column. This value now gets propagated from the partmaster to all
BOMs and can be used for a process where a part information is double checked
for accuracy.

---

# Other

[A fast paced podcast episode](https://podcast.console.dev/episodes/s02e04-brooklyn-zelenka)
that covers a lot of ground quickly about decentralized systems.

---

# Quote for the week

> A designer knows he has achieved perfection not when there is nothing left to
> add, but when there is nothing left to take away. -- Antoine de Saint-Exupery

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
