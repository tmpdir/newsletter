+++
title = "TMPDIR Weekly - #23, February 5th, 2022"
date = 2022-02-05
+++

Welcome to the 23rd issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

The Yoe Distro CI node for Github actions died last week. We're working on
reviving it but it's not yet available; therefore, the Yoe Distro 2022.01
release will be delayed.

There have been
[some regressions](https://community.tmpdir.org/t/read-if-you-use-btrfs-and-are-upgrading-to-5-16-kernel/413)
in btrfs with the 5.16 kernel.

A [balanced view](https://community.tmpdir.org/t/nixos-notes/188/6) of NixOS --
covers the positives and negatives well.

---

# IoT

Simple IoT
[release v0.0.42](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.42)
includes backend changes to move more of the API to NATS and avoid directly
accessing the DB store.

---

# Other

Go binaries are getting
[faster and smaller](https://community.tmpdir.org/t/go-notes/319/8) -- great
news for embedded/edge applications. Characteristics that are important for
cloud servers (like efficiency, ease of deployment, portability, etc) often
match up very well for what is needed at the edge. This is one reason why Linux
and Go work so well for edge/embedded systems – they are highly optimized for
servers where efficiency and portability matters. The middle ground of desktops
is the realm of bloatware, and thus why very little of Microsoft’s technology
has historically seen widespread use in servers or embedded systems.

Github now offers
[sponsor-only repos](https://community.tmpdir.org/t/github-introducing-sponsor-only-repositories/412).

[Things I believe](https://gist.github.com/stettix/5bb2d99e50fdbbd15dd9622837d14e2b)
-- a nice list of maxims about software development.

---

# Quote for the week

> ... the point of tracking is just as much about cultivating self-awareness as
> it is about making progress. -- Ryder Carroll, The Bullet Journal Method

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
