+++
title = "TMPDIR Weekly - #10, October 9th, 2021"
date = 2021-10-09
+++

Welcome to the 10th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# IoT

We released
[v0.0.35](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.35) of Simple
IoT. Changes:

- add placeholders for some UI forms
- add disable for Modbus and Modbus client nodes
- clean up locking issues and simplify DB code

Being able to disable a node is a handy as there are times where something may
not be working or hooked up yet, and you want to disable it so it does not fill
up logs with errors, hog a bus with transactions that timeout, etc.

---

# Other

The [ethr project](https://community.tmpdir.org/t/benchmarking-tools/325) by
Microsoft is a Comprehensive Network Measurement Tool for TCP, UDP & ICMP. As it
is written in Go with very few dependencies, it should be easy to cross compile
for any Linux (including embedded) system.

Python
[3.10 is a major release](https://community.tmpdir.org/t/python-3-10-released/329)
in python3 series. Since yocto’s build tools bitbake used python and archlinux
will perhaps land 3.10 soon, we will see how Yoe builds go.

We started a thread on
[selecting a database](https://community.tmpdir.org/t/selecting-a-database/327).
There are many database options, so sorting through them all can be a chore.
Many companies are moving toward GraphQL APIs, which is driving some database
changes.

Altera is
[switching to RISCV32](https://community.tmpdir.org/t/altera-switches-nios-line-of-fpga-processors-to-riscv/323)
for their new generation of NIOS devices. The following slide shows some of the
benefits:

![riscv benefits](https://community.tmpdir.org/uploads/default/original/1X/284f89065677f98fabea635c6ac035229e216c97.jpeg)

GCC 12
[enables autovectorization at](https://community.tmpdir.org/t/gcc-12-enables-autovectorization-at-o2/330)
-O2. The compiler geek inside me is excited about this change in GCC. This is a
ton of work to pull something like this and make it default. ARM chips with neon
SIMD benefits and so do all modern chips. I will do some benchmarking with gcc
and see how it compares at O2 with gcc11

---

# Quote for the week

_Every solution to every problem is simple ... It's the distance between the two
where the mystery lies. -- Derek Landy_

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
