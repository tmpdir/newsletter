+++
title = "📆 TMPDIR Weekly - #50, Yoe 2023.01 release, Time format/storage ⌚️"
date = 2023-02-18
+++

Hello,

Welcome to the 50th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> For all men, not just architects, are capable of appreciating quality; but
> there is a difference between laymen and architects in that the former cannot
> know what a building will be like unless he has seen it completed; while the
> architect knows perfectly well what it will be like ... from the instant he
> conceives it in his mind, and before he begins it." --
> [Vitruvius](https://en.wikipedia.org/wiki/Vitruvius), 1st century B.C.

---

# Linux

## [Yoe 2023.01 release](https://github.com/YoeDistro/yoe-distro/releases/tag/2023.01) 🚀

This is the first Yoe release of 2023, and we have switched to using
`mickledone` for all layers which will be the next Yocto release tentatively in
April 2023. This release contains more fixes for supporting 64-bit `time_t` on
32-bit architectures. Rust patching has been cleaned up so the patches are
applied consistently across different Rust compiler recipes. Additionally, the
Rust compiler is upgraded to 1.66.1. C++17 is the default standard for GCC 12
and the upcoming Clang 16, and there are several patch fixes for packages to get
them going with the C++17 standard so that when Yoe gets the Clang 16 upgrade,
these fixes will have already been applied. Binutils have been upgraded to 2.40
which is also a major toolchain component. Provisions to enable 64-bit `time_t`
at the distro level have been added to the core layer, and a variable
`GLIBC_64BIT_TIME_FLAGS` is introduced to control the flags.

---

# IoT

## [SIOT ADR-4: Time storage/format considerations](https://docs.simpleiot.org/docs/adr/4-time.html) ⌚️

There are many ways to store time. Some have resulted in serious issues in the
past and near future:

- two-digit year ->
  [Y2K problem in the year 2000](https://en.wikipedia.org/wiki/Year_2000_problem)
- 32bit seconds since Unix Epoch ->
  [Y2038 problem](https://en.wikipedia.org/wiki/Year_2038_problem)

We have finished our analysis of time storage and decided to stick with the
standard protobuf time format for NATS messages and a single 64-bit ns since
Unix epoch for time in the database. See the above ADR for a review of various
ways time is stored and more discussion.

---

# Other

## [Configuring TIO serial setup](https://community.tmpdir.org/t/tio-an-excellent-serial-terminal/551/3?u=cbrake) 📜

Khem shared his tio config, which illustrates an excellent way to configure all
the USB serial devices you might want to attach to without remembering long
names or looking in `dmesg` for the name of the device you just plugged in.

## [Demo of Neovim Kickstart](https://community.tmpdir.org/t/demo-of-neovim-kickstart/907)

![neovim](https://community.tmpdir.org/uploads/default/original/2X/f/f0f316c7aaab3cf3cf7dcc0dd48ede3036ce0fc6.png)

This video is a demonstration of Cliff's Neovim configuration and illustrates
how you can get close to having all the Visual Studio language server
functionality yet retain the speed and efficiency of Vim. The nvim-lua/kickstart
plugin provides an easy way to get started with modern Neovim with Language
Server integration and the latest Lua plugins. Using Lua for Neovim plugins
seems to be working out well:

- many of the new plugins are being written in Lua now.
- it is fast.
- Lua is a capable language, so complex logic is now being implemented in
  plugins.
- Lua is increasingly used as the Neovim config file syntax.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
