+++
title = "TMPDIR Weekly - #36, May 28th, 2022"
date = 2022-05-28
+++

Welcome to the 36th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## [Nvidia Opensource Linux GPU Driver](https://community.tmpdir.org/t/nvidia-is-open-sourcing-more-of-their-software-stack/514)

For many years, we have used binary drivers on Linux when using Nvidia GPU
hardware and it has worked relatively well. Nvidia took another step to open
source the kernel module piece of the GPU stack, which is a welcome change, this
perhaps is to reassure their data center customers who might find binary drivers
not attractive from security point of view. Though gaming on Linux though on the
rise, it is not a signifiant share of the total market as of now -- perhaps this
might help improve the status quo as well, time will only tell.

## [Yocto Project Summit 2022.05](https://pretalx.com/yocto-project-summit-2022-05/)

Yocto project summit is happened on May 17-19. If you are working on
OpenEmbedded/Yocto based system development then it is the event to attend, the
event covers courses for new beginners starting fresh with project and
intermediate program covers the talk on advanced topics covering wide range of
topics from using specific features of project, and best practices. There are
speakers who have deployed projects in production and it's good learning for
others to share the experiences. Complete schedule is
[here](https://pretalx.com/yocto-project-summit-2022-05/schedule/).

---

# IoT

Simple IoT
[v0.2.0 has been released](https://github.com/simpleiot/simpleiot/releases/tag/v0.2.0)
with an improved UI for linking nodes to rule conditions/actions.

---

# Other

An
[interesting discussion](https://community.tmpdir.org/t/podcast-280-cristiano-amon-qualcomm-ceo-lex-fridman-podcast/515)
with the CEO of Qualcomm -- provides some good insight into 5G and where the
cellular industry is going.

---

# Quote for the week

> Better train people and risk they leave - than do nothing and risk they stay.
> -- Anonymous

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
