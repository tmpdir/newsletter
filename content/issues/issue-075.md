+++
title = "📆 TMPDIR Weekly - #75, Want an easier way to update your system?"
date = 2024-05-11
+++

Hello,

Welcome to the 75th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> If you do not change direction, you may end up where you are heading. -- Lao
> Tzu

---

# Simple IoT Release v0.16.0 -- System update support 🚀

The SIOT
[v0.16.0 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.16.0)
contains a new System Update client designed to be used with the
[Yoe Updater](https://github.com/YoeDistro/yoe-distro/blob/master/docs/updater.md).
This is a easy way to deploy updates to your system with a simple web server.
The process can be executed manually, or there are options to automatically
download and install new updates.

![updater ui](https://docs.simpleiot.org/docs/user/assets/update.png)

---

# Perplexity.ai

I've been using [Perplexity](https://www.perplexity.ai/) more in recent months.
I don't have the Pro subscription yet, but considering it. Perplexity seems
seems really good at summarizing search results. Not sure exactly why I like it,
but it seems to provide me the information I need in a form that is very usable

Apparently, I'm not the only one:

[How Perplexity builds product](https://www.lennysnewsletter.com/p/how-perplexity-builds-product)

> Founded less than two years ago, [Perplexity](https://www.perplexity.ai/) has
> become a many-times-a-day-use product for me, replacing many of my Google
> searches—and I’m not alone. With fewer than 50 employees, the company has a
> user base that’s grown to tens of millions. They’re also generating over $20
> million ARR and taking on both Google and OpenAI in the battle for the future
> of search. Their
> [recent fundraise of $63m](https://x.com/AravSrinivas/status/1782784338238873769)
> values the company at more than $1 billion, and their investors include
> Nvidia, Jeff Bezos, Andrej Karpathy, Garry Tan, Dylan Field, Elad Gil, Nat
> Friedman, Daniel Gross, and Naval Ravikant (but sadly not me 😭). Nvidia CEO
> Jensen Huang said he uses the product
> “[almost every day](https://arc.net/l/quote/uglckdse).”

Are you using Perplexity? If so, what do you think of it?

---

# [Why you probably shouldn’t add a CLA to your open source project](https://ben.balter.com/2018/01/02/why-you-probably-shouldnt-add-a-cla-to-your-open-source-project/)

This article discusses the balance between consideration for your project users
and legal protection. It does not always make sense to go for maximum legal
protection.

> The question then becomes, if the project’s open source license isn’t
> sufficient enough for maintainers to feel comfortable accepting code, why
> should end users be expected to feel comfortable enough to do so? Put another
> way, why should project maintainers be granted rights and protections beyond
> those granted to end users (and other contributors)? Most often, this is a
> sign that the license the project’s using isn’t sufficient for the project’s
> industry or the maintainer’s legal posture, and moving to a more patent-aware
> license like Apache, may resolve the need for a CLA entirely.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
