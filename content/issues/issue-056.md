+++
title = "📆 TMPDIR Weekly - #56, Shelly IoT support, Yoe 2023.04 release, Mountain Streams 🚀"
date = 2023-05-12
+++

Hello,

Welcome to the 56th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> The impediment to action advances action. What stands in the way becomes the
> way. - Marcus Aurelius

---

# Yoe 2023.04 release 🚀

Latest monthly
[release of yoe distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/24),
coincides with yocto 4.2 ( mickledore ) release Yoe distro's living-on-trunk
still carries on. We have switched to using the hashserver equivalence feature,
which should improve reproducability and reuse of sstate cashe overall. Several
key recipes are upgraded e.g. `clang` 16.0.1, `rust` 1.68.2, `systemd` 253.3,
`ffmpeg` 6.0 to name a few. Several packages have been fixed to run ptests when
using Musl C library to help run ptests in CI for key reference images e.g.
`core-image-ptest-fast`. `visionfive2` support is in meta-riscv even though
project is not yet added to yoe distro yet.

---

# Simple IoT now supports Shelly Devices 🚀

Simple IoT [v0.10.1](https://community.tmpdir.org/t/simple-iot-releases/214/56)
has support for various Shelly IoT devices. See the documentation for more
details.

Shelly IoT devices provide excellent value. They are reasonably priced and have
a lot of nice features:

- an open API support multiple protocols (HTTP, CoAP, MQTT, mDNS, etc)
- nicely packaged
- very small such that you can embed them directly in existing outlet and switch
  boxes.
- well documented

This is just the start – we plan to add support for more Shelly devices in the
coming months – let us know what you’d like to use. Help testing devices I don’t
personally own is greatly appreciated.

![shelly device](https://community.tmpdir.org/uploads/default/original/2X/5/53e6d9810161c7eba4c6ae816c47cbfb44817df7.jpeg)

---

# Podcast Episode #19 -- Release Early and Often

Which do you find more inspiring – a stagnant pond covered in algae or a
fast-flowing mountain stream? Learn the benefits of sharing your work early and
often. This is especially important in team environments where trust, momentum,
and efficient communication are desired.

Listen [here](https://tmpdir.org/019/) or on your favorite
[podcast platform](https://tmpdir.org/about/).

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
