+++
title = "TMPDIR Weekly - #44, November 4th, 2022"
date = 2022-11-04
+++

Hello,

Welcome to the 44th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Linux

## [Yocto 4.1 (langdale) Released](https://lists.yoctoproject.org/g/yocto-announce/message/259)

The Yocto project released version 4.1 (October release), it is tested against
5.19 kernel, glibc 2.36, key recipes among 250 odd recipe updates, rust compiler
and infrastructure supports building target rust, rust SDKs and crate fetcher
supports externalsrc, which should help developers. Initial new layer setup
tooling has been added which can help in setting up workspace.

CPU, I/O and memory pressure regulation is added in bitbake, which should help
in adjusting system load during builds and avoid overloading the system which
can cause random build failure due to build pressure.

---

# IoT

## XOR Hash Algorithm

In SimpleIoT we are continuing to explore hashing algorithms. The XOR operation
has some interesting proprieties:

- **Commutative: A ⊕ B = B ⊕ A** (the ability to process elements in any order
  and get the same answer)
- **Associative: A ⊕ (B ⊕ C) = (A ⊕ B) ⊕ C** (we can group operations in any
  order)
- **Identity: A ⊕ 0 = A**
- **Self-Inverse: A ⊕ A = 0** (we can back out an input value by simply applying
  it again)

An XOR hash is being used in the
[upstream syncing algorithm](https://docs.simpleiot.org/docs/ref/sync.html).
This big advantage to this hash algorithm is that it can be applied
incrementally instead of recalculating the hash from scratch every time there is
an update, so it is blazing fast.

Implementation is in process and should be finished soon.

## SIOT releases

A handful of minor releases which include cleanup of SIOT main.go, switch to
go-embed for embedded assets, etc. Now it's possible to do:
`go run github.com/simpleiot/simpleiot/cmd/siot@latest`

- [v0.5.2](https://github.com/simpleiot/simpleiot/releases/tag/v0.5.2)
- [v0.5.3](https://github.com/simpleiot/simpleiot/releases/tag/v0.5.3)
- [v0.5.4](https://github.com/simpleiot/simpleiot/releases/tag/v0.5.4)

---

# Other

## [The problem with Threads](https://community.tmpdir.org/t/the-problem-with-threads/678)

Many modern languages and language runtimes have started to include concurrency
as standard primitive making it part of the language. A typical example is the
Go language which implements the concurrency primitives in the Go runtime and
provides features like channels/goroutines which releases the developers of the
cognitive load of thinking in terms of threads. This enables writing complex
concurrent programs without many of the pitfalls traditional threading
approaches have.

---

# Quote for the week

> Perfection is achieved not when there is nothing more to add, but rather when
> there is nothing more to take away. – Antoine de Saint-Exupery

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
