+++
title = "📆 TMPDIR Weekly - #79, AI is in trouble!, Terminal tech"
date = 2024-06-21
+++

Hello,

Welcome to the 79th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> One of my most productive days was throwing away 1000 lines of code. -- Ken
> Thompson

---

# AI is in Trouble

Jeff, one of our community members living in Taiwan, attended the recent
Computex show and wrote up a
[detailed review](https://community.tmpdir.org/t/ai-is-in-trouble/1316). Power
usage is one of the major challenges with AI and is a hard constraint. Taiwan is
the center of AI hardware as much of the hardware that runs AI systems is
manufactured there. Jeff summarizes the approach by a number of difficult
companies as they look at the power problem in AI.

---

# Terminal technology

We continue to explore the latest terminal technology that can make your life as
a developer more efficient and pleasant.

- [Helix editor](https://community.tmpdir.org/t/helix-editor/1244/37)
- [Zellij](https://community.tmpdir.org/t/zellij-notes/1325/3)
- [WezTerm](https://community.tmpdir.org/t/wezterm-notes/1328)
- [Alacritty](https://community.tmpdir.org/t/alacritty-notes/1324/2)

All of the above are written in Rust and are blazing fast.

![terminal](https://community.tmpdir.org/uploads/default/optimized/2X/a/a510965c67224c4fc9ceb867bec5ea73293e8ed3_2_1116x1000.png)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
