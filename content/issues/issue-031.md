+++
title = "TMPDIR Weekly - #31, April 2nd, 2022"
date = 2022-04-02
+++

Welcome to the 31st issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## [Systemd is getting a system updater](https://community.tmpdir.org/t/systemd-is-getting-system-updater/464)

The next systemd release 251 is in the RC stages. Among many interesting
features is a tool called `systemd-sysupdate`. This can do file, directory, or
partition based updates using an A/B scheme. This is interesting given that
there are multiple solutions in place to update systems today -- especially
embedded Linux systems -- e.g. RAUC, Swupdate, Mender to name a few. This will
be interesting option since systemd is shipped as default init system in many
embedded Linux systems, it could offer another integrated solution to solve the
update issue.

## [Yoe Distribution 2022.03 - "Orkney" Released](https://community.tmpdir.org/t/yoe-distribution-releases/239/11)

A New release of the Yoe Distribution is available for use. This release comes
with the new Clang 14.0.0 release, PEP-517 packaging fixes and many other major
changes. This release also aligns well with Yocto Kirkstone (3.5) release.

## Content Addressable Data Synchronizer

We have been investigating delta updates for the Yoe Distribution as an
enhancement to current Yoe system updater and many technologies are interesting,
especially `casync`, which has multiple implementations as described
[here](https://community.tmpdir.org/t/casync-content-addressable-data-synchronizer/460)
Some existing updaters like RAUC already provide options to use `casync`. There
is [desysnc](https://github.com/folbricht/desync) which is a `casync`
implementation in Go and is used in the Valve Steam Deck along with RAUC.

---

# IoT

## Simple [IoT v0.0.44](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.44) is released

- UI: fix bug where copy node crashes UI if not on secure URL or localhost
  (#341)
- support clone/duplicate node (as well as mirror) operation (#312). Now when
  you copy and paste a node, you will be presented with a list of options as
  shown below. The new duplicate option allows you to easily replicate complex
  setups (for instance a bunch of modbus points) from an existing site to a new
  site.

![copy options](https://user-images.githubusercontent.com/402813/153455487-66bc2699-1026-40de-9ca6-4f30f91aeff9.png)

See
[documenation](https://docs.simpleiot.org/docs/user/ui.html#deleting-moving-mirroring-and-duplicating-nodes)
or a [demo video](https://youtu.be/ZII9pzx9akY) for more information.

---

# Other

A [blog post](http://bec-systems.com/site/1563/business-contracts) detailing the
real world challenges of business contracts and presents some interesting
alternatives to a legal arms race based on based on fostering collaboration and
building trust.

[Tips for combatting binge watching/reading](https://community.tmpdir.org/t/tips-for-combatting-binge-watching-reading/462)

---

# Quote for the week

> "The most damaging phrase in the language is.. it's always been done this
> way" - Grace Hopper

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
