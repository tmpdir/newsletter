+++
title = "📆 TMPDIR Weekly - #52, Yoe Updater, SIOT Shelly Device Support"
date = 2023-03-11
+++

Hello,

Welcome to the 52nd issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> The best writing is rewriting. --E. B. White

---

# This newsletter flagged as malware

A kind reader notified us that Google has flagged Tinyletter, the email service
we use, as distributing malware. If you get nasty malware messages when clicking
on links in this email, rest assured we are not sending malware, we have just
been lumped in with someone who is abusing the Tinyletter service. We will
likely be moving to a new newsletter service in the new future -- see
[this page](https://community.tmpdir.org/t/when-google-blocks-your-newsletter-links/952)
for more details.

# Linux

## [Yoe Updater and Factory Install](https://github.com/YoeDistro/yoe-distro/pull/766) 🚀

The Yoe Distribution uses the
[Yoe updater](https://github.com/YoeDistro/yoe-distro/blob/master/docs/updater.md)
for managing the system firmware. It has two modes:

1. prepare a system's default installer media e.g. eMMC and installs a factory
   image into it.
2. update or OTA (over-the-air) mode in which it can find updates on the data
   partition or media like USB disks or SD cards and then apply the updates to
   system firmware without touching the content of data partition.

There is a special image target `yoe-installer-image` where
`FACTORY_INSTALL_IMAGE` is made part of installer which is the initial image
that will be installed. This image can be written to removable media (USB/SD)
and used to partition and install software to a soldered-down eMMC device.
Later, different images can be applied by installing update images.

We are also working on options to make rootfs changes temporary (disappear after
the next reboot) or persistent via various overlay filesystem configurations.

---

# IoT

## [Airgain NL-SW-LTE-TG1WWG Modem](https://community.tmpdir.org/t/airgain-nimbelink-nl-sw-lte-tg1wwg-modem-notes/942)

We are qualifying a new Airgain Cat-M modem for projects. The above page will
detail our progress. Excited to try native USB ECM mode to see if we can get
away from using PPP over serial for the network interface ...

This module uses a Telit modem. Supposedly the modem is compatible with Cat-1,
Cat-4 modules, so can more easily scale to different modem speeds without
changing software -- that would be nice!

## Shelly Automation device support

We are in the process of adding support for
[Shelly IoT devices](https://www.shelly.cloud/). Shelly devices have open APIs,
a built-in web server, and are reasonably priced. This is the way home
automation should work. Stay tuned for updates soon ...

---

# Other

Lots of news and updates on the [community site](https://community.tmpdir.org/).
A few samples:

- [Chat GPT’s description of the Yoe Distribution](https://community.tmpdir.org/t/chat-gpts-description-of-the-yoe-distribution/960)
- [Actions beat arguments](https://world.hey.com/dhh/actions-beat-arguments-2aa1da34)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
