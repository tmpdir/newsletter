+++
title = "📆 TMPDIR Weekly - #49, Clang 16, Simple IoT Metrics 📊"
date = 2023-02-04
+++

Hello,

Welcome to the 49th issue of TMPDIR, a weekly newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote of the week

> Make it work, then make it beautiful, then if you really, really have to, make
> it fast. 90 percent of the time, if you make it beautiful, it will already be
> fast. So really, just make it beautiful! -- Joe Armstrong

---

# Linux

## LLVM/Clang 16 💻

Yoe Distro uses Clang as default compiler; therefore it's interesting to see a
new major release building up in 16.0.0 which branched last week, scheduled to
be released in fist week of March. It has switched to using C++17 as default
standard. This will require some porting for few packages, a notable change is
for packages defining own `alignof` is flagged as
[undefined behavior](https://www.open-std.org/jtc1/sc22/wg14/www/docs/n2350.htm).
A way to fix it is to use `_Alignof`, it was also a
[problem in gnulib](https://git.savannah.gnu.org/cgit/gnulib.git/commit/?id=2d404c7dd974cc65f894526f4a1b76bc1dcd8d82)
which is used by several downstream projects.

---

# IoT

## [Simple IoT v0.8.0 release](https://github.com/simpleiot/simpleiot/releases/tag/v0.8.0) 🚀

This release adds system, app, and process metrics. See
[documentation](https://docs.simpleiot.org/docs/user/metrics.html) for more
details.

![metrics](https://docs.simpleiot.org/docs/user/images/metrics-app.png)

---

# Other

## [Go 1.20 release](https://go.dev/blog/go1.20) 🚀

A new `errors.Join()` function has been added to concatenate errors. You can
later `errors.Unwrap()` to see the individual errors, or test for the presence
of an error with `errors.Is()`.

> The go tool no longer relies on pre-compiled standard library package archives
> in the $GOROOT/pkg directory, and they are no longer shipped with the
> distribution, resulting in smaller downloads. Instead, packages in the
> standard library are built as needed and cached in the build cache, like other
> packages.

This is good -- the less we ship around pre-compiled binaries for tooling, the
better. The requirement for pre-compiled binaries often leads to brittle
tooling.

There are many other improvements -- read the
[release post](https://go.dev/blog/go1.20)!

---

Thoughts, feedback? Let us know: [info@tmpdir.org](mailto:info@tmpdir.org).

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).
