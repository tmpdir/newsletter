+++
title = "📆 TMPDIR Weekly - #72, Yoe 2024.03 release 🚀, XZ attacks"
date = 2024-04-06
+++

Hello,

Welcome to the 72nd issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> True intuitive expertise is learned from prolonged experience with good
> feedback on mistakes. -- Daniel Kahneman

---

# Yoe 2024.03 release 🚀

[March release of Yoe Distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/36)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components. This release has many new changes.

- Yoe Distro defaults to LLVM/clang for C/C++ compiler and this release upgrades
  the compiler to 18.x major release, some package recipes needed to be patched
  to work with the new compiler, however these changes has been trickling in for
  past few releases already.
- SimpleIOT, one of the core components of Yoe Distro, has been waiting on
  latest go compiler since it has been needing latest golang features, therefore
  in this release go compiler has been upgraded to 1.22 major version
- LVGL components have moved to 9.x major release
- Yoe Kiosk browser is ported to work with QT 6.7
- Variscrite SOM based projects e.g. var-som-mx8 has been upgraded to use kernel
  6.1 there are issues e.g. WiFi bringup on Symphony reference board
- nocrypto is used with -mcpu compiler options for rpi3/rpi4, this fixes illegal
  instruction errors with QTWebengine
- simpleIOT upgraded to 0.15.3
- SystemD upgrade to 255.4
- libxml2 was upgraded to 2.12+ in Core layer, which resulted in several
  packages being fixed for API breakage due in this version of libxml2

---

# [The XZ open source attack](https://community.tmpdir.org/t/everything-i-know-about-the-xz-backdoor/1271)

A few articles about the recent xz open source attack:

- [Everything I know about the XZ backdoor](https://boehs.org/node/everything-i-know-about-the-xz-backdoor)
- [Timeline of the xz open source attack](https://research.swtch.com/xz-timeline)

This incident illustrates the real problem of maintainer burnout and how
nefarious individuals can take advantage of this. We also see how the OSS
advantage of many eyes also works and this problem was caught fairly early.

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
