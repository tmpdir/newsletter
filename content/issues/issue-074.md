+++
title = "📆 TMPDIR Weekly - #74, Yoe 2024.04 release 🚀, Frictionless time tracking"
date = 2024-05-03
+++

Hello,

Welcome to the 74th issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> There is no greater impediment to the advancement of knowledge than the
> ambiguity of words. -– Thomas Reid

---

# Linux

# Yoe 2024.04 release 🚀

[April release of Yoe Distro](https://community.tmpdir.org/t/yoe-distribution-releases/239/37)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components. This release has many new changes.

- Go compiler upgraded to 1.22.2
- Clang C/C++ compiler upgraded to 18.1.4
- Gstreamer upgraded to 1.22.11
- QT6 is upgraded to 6.7.1
- Package tests ( ptests ) fixes for many recipes in meta-openembedded
- Added OpenSSH fido2 support
- Make predictable name mac policy opt-out in systemd, Fixes WIFI issues on imx8
  platforms
- Switch to using https/http instead of ftp protocol in SRC_URIs

---

# [Is WireGuard more secure than SSH for your public servers?](https://community.tmpdir.org/t/is-wireguard-more-secure-than-ssh-for-your-public-servers/1288)

In the past, I've typically disabled password login on my servers, used SSH
keys, and called that "good enough" security for what I'm doing. Security must
be pragmatic -- it makes no sense to go to extraordinary lengths to secure
things when the value of what you are securing is not all that high. Security is
a tradeoff -- typically it comes with a loss of convenience.

![image|690x132, 75%](https://community.tmpdir.org/uploads/default/original/2X/1/1c2045098880aaf3d1b75eb8d82f37654399c525.png)

One of the problems with an SSH port exposed to the Internet is you constantly
have people attacking it, trying to get in with various passwords -- never mind
you disabled password login. The logs are full of this:

`Failed password for invalid user jms from 43.153.175.28 port 34778 ssh2`

You can move your SSH port to something other than port 22, but hackers
eventually figure this out as it is easy to do a port scan.

In the past, VPNs were not all that attractive as they were complex to set up
and questionable if they were really more secure than SSH itself -- complexity
is the enemy of security. However, with Wireguard, this has all changed.

> WireGuard has been designed with ease-of-implementation and simplicity in
> mind. It is meant to be easily implemented in very few lines of code, and
> easily auditable for security vulnerabilities. Compared to behemoths like
> \*Swan/IPsec or OpenVPN/OpenSSL, in which auditing the gigantic codebases is
> an overwhelming task even for large teams of security experts, WireGuard is
> meant to be comprehensively reviewable by single individuals.

So, I'm reading through the Wireguard documentation. In the
[whitepaper](https://www.wireguard.com/papers/wireguard.pdf), I learned that
Wireguard does not even respond if a packet does not authenticate.

> 5.1 Silence is a Virtue
>
> One design goal of WireGuard is to avoid storing any state prior to
> authentication and to not send any responses to unauthenticated packets. With
> no state stored for unauthenticated packets, and with no response generated,
> **WireGuard is invisible to illegitimate peers and network scanners.** Several
> classes of attacks are avoided by not allowing unauthenticated packets to
> influence any state.

Therefore, it is not even possible to know if a Wireguard VPN server exists on a
machine by probing it, unlike a SSH port which requires the use of stateful TCP
(WireGuard by default uses UDP).

Additionally, Wireguard requires the use of keys, so there is no incentive to
try passwords. Thinking through this a bit more, closing the external SSH ports
and using Wireguard gives you two levels of security:

![image|611x500, 50%](https://community.tmpdir.org/uploads/default/original/2X/7/7cc0dd40e8dc9adae82073e8151e48faaa084147.png)

You have to break through both Wireguard and SSH to gain shell access to the
machine. You don't lose any security by adding Wireguard, other than it might
provide access to ports behind your firewall if Wireguard is compromised. Given
that Wireguard is relatively easy to set up, I'm starting to think it makes
sense.

There are still tradeoffs ... one of the biggest risks of advanced security
mechanisms, like MFA, is you will lock yourself out. In the case of a Wireguard
VPN server, there is now a single point of failure if you have a single VPN
server, or rely on a single provider like
[Tailscale](https://community.tmpdir.org/t/have-you-tried-tailscale/493).
However, given how reliable cloud servers are at companies like Linode, it seems
this risk is small, especially if you back up the keys and configuration. In a
pinch, you could also log in through the console (most cloud server hosting
providers offer this) and re-enable external SSH access.

---

# Kairos -- an easy way to track time for your team and generate invoices

![kairos logo](https://raw.githubusercontent.com/kairosdotapp/kairos/main/logo.png)

Do you need a tool to track time for a distributed team and easily generate
invoices? Kairos is a tool that generates beautiful invoices from time log files
stored in Git. This makes it easy for your team to log their time using their
standard tools (Git/Text Editor) and then generate HTML invoices using a simple
template. Advanced reports can be generated using
[hledger](https://hledger.org/).

I wrote this tool after looking at many other options and concluding there has
to be a better way. Having recently switched to hledger command line accounting,
this seemed like a logical step.

Even if you don't do hourly billing, there are many benefits to tracking how
time is spent. If you don't measure it, it isn't easy to improve it -- this is
especially important for time management.

[Learn more ...](https://github.com/kairosdotapp/kairos)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
