+++
title = "📆 TMPDIR Weekly - #73, How Yocto Dynamic Layers Help You Build Your Platform"
date = 2024-04-19
+++

Hello,

Welcome to the 73rd issue of TMPDIR, a weekly newsletter 📰 helping teams build
connected, data-intensive products by exploring the latest open source Linux and
IoT technologies, workflows, and best practices. Please pass this on to anyone
else who is interested in unlocking the power of open source.

Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Good software, like good wine, takes time. -- unknown

---

# [How Yocto Dynamic Layers Help You Build Your Platform](https://tmpdir.org/029/)

In [this espisode](https://tmpdir.org/029/), we discuss what Yocto Dynamic
Layers are, how they can help you manage changes for multiple layers, and how
this helps you build an Embeded Linux Platform that you can scale accross time
and products.

[Documentation](https://docs.yoctoproject.org/ref-manual/variables.html?highlight=bbfiles_dynamic#term-BBFILES_DYNAMIC)

See
[how we use them ](https://github.com/YoeDistro/yoe-distro/tree/master/sources/meta-yoe/dynamic-layers)
in the Yoe Distribution for an example of how you might use dynamic layers.

[Another discussion](https://community.tmpdir.org/t/how-to-use-dynamic-layers-in-yocto/446)

---

# [Celebrate Small Things](https://himvis.com/2024/04/09/celebrate-small-things/)

I contribute to a weekly newsletter on embedded linux and IOT, invariably, I
find it challenging to write since there is not much that might have happened in
a weeks time which my mind considers worth noting, we tried to delay the
newsletter in hope of collecting splashy content, yet another possible way out
is to start referencing other articles, news or write ups since there might be a
bigger sense of splash in them, however, this does not create new value for
newsletter subscribers are looking for. If they were looking for aggregation
then they would use many fancier tools out there.

In a fast paced environment, many things are happening around us we fail to take
notice of, due to distractions or constant context and task switching that our
mind fails to register them. This can be even more, where work is happening in a
rolling release mode. Sometimes, it is dictated by the nature of work e.g.
support roles in a software product organization. This can be quite draining if
this continues for a long period of time. Our brain needs feedback and a sense
of accomplishment to keep the energy level and return to the task list.

So what is the way out ?

[read more ...](https://himvis.com/2024/04/09/celebrate-small-things/)

---

# [The Ability to Improve](https://bec-systems.com/2267/the-ability-to-improve/)

![improve](https://bec-systems.com/wp-content/uploads/2024/02/image-6-2048x697.png)

**If you don’t continually improve, you soon lose the ability to do so …**

This thought came after an associate described a company that is struggling to
manufacture one of their products:

> They have been making a product since way back in the last century. EOL
> (end-of-life) is catching up with them. The circuit is really hairy analog
> stuff with ridiculously high precision. Pots everywhere to get it dialed in.
> But their voltage regulator is drifting in value significantly, whereas the
> old parts are rock steady.

How do we get into situations like this, and how can they be avoided?

Below are a few more examples of situations that are very difficult to deal with
in the lifecycle of a product:

[read more ...](https://bec-systems.com/2267/the-ability-to-improve/)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics.

Find past issues of TMPDIR 📰 [here](https://newsletter.tmpdir.org/).

Listen to podcasts at 🎙 [https://tmpdir.org/](https://community.tmpdir.org/).
