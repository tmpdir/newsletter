+++
title = "TMPDIR Weekly - #17, December 4th, 2021"
date = 2021-12-04
+++

Welcome to the 17th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Yoe distribution release
[v2021.11 (langhe) is now released](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.11).
We now support PowerVR drivers on BeagleBone Black. Github SRC_URI links have
been changed to HTTPS as Github as deprecated git access. WiFi drivers for rPI
have been upgraded to support for CM4 and PI400. Many other upgrades including
Go 1.17 -- see release notes for more.

---

# Other

Interesting discussion on the Gotime podcast about event driven systems. In
[this section](https://changelog.com/gotime/179#transcript-28), they discuss
eventual consistency, idempotency, the snake-oil of exactly-once delivery, and
using timestamps for merging -- all this sounds very familiar as we're dealing
with all this in the Simple IoT project. Thus far, I'm liking the event driven
architecture (using NATS) a lot.

Good [article](https://itnext.io/go-does-not-need-a-java-style-gc-ac99b8d26c60)
on various garbage collection techniques. This article explains why Go does not
need a Java style garbage collector.

We started a new project named
[GitPLM](https://github.com/git-plm/gitplm#readme), which contains tooling and a
collection of best practices for managing information needed to manufacture
products.

---

# Quote for the week

> If you get tired, learn to rest, not to quit. ― Banksy

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
