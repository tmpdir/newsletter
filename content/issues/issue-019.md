+++
title = "TMPDIR Weekly - #19, December 18th, 2021"
date = 2021-12-18
+++

Welcome to the 19th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

There is an
[interesting discussion](https://community.tmpdir.org/t/raspberry-pi-4-uefi-booting/377/9)
about UEFI on ARM systems. Will this eventually become the preferred boot
solution for embedded?

Will Vulkan
[be the answer](https://www.phoronix.com/scan.php?page=news_item&px=CS-GO-Vulkan-Experimental)
to advanced 3D graphics on Linux? Here is a
[performance comparison](https://www.phoronix.com/scan.php?page=article&item=cs-go-vulkan&num=1).

There is a [new linker available](https://github.com/rui314/mold) that is even
faster. _"mold is so fast that it is only 2x slower than cp on the same
machine"_ Shaving a minute off linking time during builds is a huge reduction.

![mold performance](https://raw.githubusercontent.com/rui314/mold/main/docs/comparison.png)

In the upcoming Yoe release, we will start publishing images for common
platforms such as the Raspberry PI and BeagleBone Black so you can quickly
evaluate Yoe.

---

# IoT

[v0.0.39 of Simple IoT was released](https://github.com/simpleiot/simpleiot/releases/tag/v0.0.39)
with bug fixes for upstream support. Now if you delete a device in the upstream,
it will be recreated if the downstream is still communicating. This is helpful
so devices don't get lost.

We also [released a video](https://youtu.be/-1PuBoTAzPE) showing how Simple IoT
can be used to remotely access a Velocio PLC.

---

# Other

An official Aarch64 release for the Elm compiler does not exist, so
[we have published](https://github.com/YoeDistro/compiler/releases/tag/0.19.1)
our own binary release. This is a statically linked binary, so should work on
any 64-bit ARM Linux system.

A few more
[interesting links](https://community.tmpdir.org/t/notes-on-writing/247/3) on
writing. If you want to scale, you need to write.

I listed to [two podcasts](https://community.tmpdir.org/t/vim-notes/375) on
Vim/Neovim. Lots of interesting points, but one that resonated with me is that
Vim is a very efficient way to edit code and this is helpful if you want to
avoid RSI injuries.

---

# Quote for the week

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
