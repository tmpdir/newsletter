+++
title = "TMPDIR Weekly - #3, August 7th, 2021"
date = 2021-08-07
+++

Welcome to the 3rd issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

## Embedded Linux

### [Yoe Distro 2021.07 (hebridean) release](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.07)

- libseccomp is now supported on RISCV32 platforms
- Added native builds for LLVM runtime (libcxx and compiler-rt), which helps in
  building chromium using pure clang and llvm runtime
- Fix gimp build race, which has been evading for long time especially seen on
  musl
- Fix install race on python3
- Fix packages to build with glibc 2.34, there are few changes in glibc 2.34
  which requires changes in packages e.g. close_range API is available and some
  compile time defines have become runtime sysconf entries e.g.
  PTHREAD_STACK_MIN
- SOURCE_MIRROR_URL now supports S3 buckets, this lets you use S3 for creating
  source mirrors
- Fix freedom-u540 BSP support
- Bitbake is fixed to not spawn into zombie processes on early termination, this
  was often seen when using Ctrl + C to cancel bitbake during parse stage
- binutils is upgraded to 2.37, systemd is upgraded to 249.1 along with many
  package version upgrades and newly added recipes

See [release](https://github.com/YoeDistro/yoe-distro/releases/tag/2021.07) for
full changelog.

---

## IoT

On the Simple IoT project, still working on the
[schedule implementation](https://github.com/simpleiot/simpleiot/pull/214). It
is more work than anticipated, but it is turning out well. One technique that
has been clarified through this feature implementation is how to keep track of
state in a UI widget when a value is partially entered. For example, when a time
is entered in local time in the browser, but stored as UTC in the frontend
model/backend DB, you can't do the UTC conversion when the time is partially
entered. How do you keep track of state while the time is being entered? You
could add another state variable to the model. However, a far simpler technique
is to simply store the state in the variable you are entering (perhaps prefix it
with a special letter or something. We
[already do this](https://github.com/simpleiot/simpleiot/blob/master/frontend/src/UI/NodeInputs.elm#L303)
when entering floating point numbers. In this case, we store a magic value in
the point _Text_ field when the number is blank. This eliminates automatically
turn a blank into a zero and handles the case when you have a '.' entered
without any decimal numbers yet. Any time you can avoid adding additional state,
this seems to be a good thing. This also illustrates a common theme of the SIOT
project -- do more with fewer types.

---

## Other

### [The career-changing art of reading the docs](https://community.tmpdir.org/t/the-career-changing-art-of-reading-the-docs/286)

Recently read an
[article](https://acloudguru.com/blog/engineering/the-career-changing-art-of-reading-the-docs)
and listened to a [podcast](https://changelog.com/gotime/167) that explains the
benefits of reading documentation. A quote from the article:

_If you read a targeted set of docs consistently over a sustained period — say,
a couple of years — while actively practicing on that technology, you will be
able to perform magic. That’s a promise._

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Listen to previous podcasts at https://tmpdir.org/.

Thanks for reading! Khem and Cliff
