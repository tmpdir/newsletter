+++
title = "TMPDIR Weekly - #27, March 5th, 2022"
date = 2022-03-05
+++

Welcome to the 27th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

Yoe 2022.02 was released with many interesting
[changes](https://community.tmpdir.org/t/yoe-distribution-releases/239/10). It
includes inclusive language changes across multiple layers especially in core
and meta-openembedded and upgrades toolchain components to major new releases:

- Clang 14.0.0 release candidate
- Glibc 2.35
- Binutils 2.38
- GDB 11.2
- Go - 1.17.7

All layers are primarily using kirkstone (3.5) yocto project ready branches.
This is first Yoe release of 2022!

QT company opensources
[5.15.3 LTS release](https://community.tmpdir.org/t/qt-company-opensources-5-15-3-lts-release/443)
after almost 2 years. The `meta-qt5` layer, which provides QT5 for
Yocto/Openembedded projects, is currently using 5.15.2 release. It would be
interesting to upgrade to 5.15.3 for Yoe Distro as well, since the
`yoe-qt5-image` reference image is based on QT5

---

# IoT

Ongoing work continues in Simple IoT to support a
[duplicate node feature](https://github.com/simpleiot/simpleiot/pull/333) and
better understand node lifecycle issues
[ADR-3](https://github.com/simpleiot/simpleiot/pull/334). They are related as
the duplicate features has the potential to create a large number of nodes
quickly if the duplicated node has a lot of descendants.

The new Simple IoT [documentation site](https://docs.simpleiot.org/) has been
deployed using mdBook -- it is working great! Will have more user documentation
updates soon after the duplicate feature is finished.

---

# Other

Trying [Julia](https://julialang.org/) in a
[Pluto](https://github.com/fonsp/Pluto.jl) notebook to run calculations for a
circuit design. Initial impressions are positive -- seems to be well suited for
the task. While reviewing the language,
[I've noted](https://community.tmpdir.org/t/a-review-of-the-julia-programming-language/442)
a number of interesting features of the language.

If you are looking for a simple personal/small business email server alternative
to Postfix, check out
[Maddy](https://community.tmpdir.org/t/the-maddy-email-server/439). Maddy is a
single binary mail server written Go that has support for all the standard stuff
like SMTP, IMAP, TLS, DKIM, etc -- initial experiences are good.

---

# Quote for the week

“Good design adds value faster than it adds cost.”

- Thomas C. Gale

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
