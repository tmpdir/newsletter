+++
title = "📆 TMPDIR Weekly - #68, Yoe 2024.01 Release"
date = 2024-02-03
+++

Hello,

Welcome to the 68th issue of TMPDIR, a weekly newsletter 📰 covering Embedded
Linux, IoT systems, and technology in general. Please pass this on to anyone
else you think might be interested. Suggestions and feedback are welcome at ✉️
[info@tmpdir.org](mailto:info@tmpdir.org).

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

Thanks for reading!

Khem and Cliff

---

# Quote for the week

> Great acts are made up of small deeds. -- Lao Tzu

---

# Yoe 2024.01 release 🚀

[First release of 2024](https://community.tmpdir.org/t/yoe-distribution-releases/239/34)
is available. As is usual with rolling release, useful features and fixes keep
rolling in along with important security fixes implicitly fixed in newer
components.

- Python upgraded to 3.12 major version, there are several fixes to other
  packages to work with new python due to obsoleted distutils and unittest
  modules being removed in this release. Distutils functionality is still
  provided by setuptools.
- Kernel 6.6 is available for Qemu based projects as default kernel
- A bunch of new machines are available, notable among them is RaspberryPi 5,
  orangepi-5-plus
- New yoe project for 32-bit RaspberryPi4 is added

---

# How is scc so fast?

[Scc](https://github.com/boyter/scc) is a super fast source code counter. In the
below example, we can count the lines in the 79,102 files in the Linux kernel
source in ~1 second. Scc is written in Go, but
[is faster](https://github.com/boyter/scc?tab=readme-ov-file#performance) than
similar projects written in C and Rust. For the Linux kernel:

> ```
> Summary
>   'scc -c linux' ran
>     1.07 ± 0.22 times faster than 'loc linux'
>     1.39 ± 0.05 times faster than 'tokei linux'
>     1.41 ± 0.05 times faster than 'scc linux'
>     2.35 ± 0.07 times faster than 'polyglot linux'
> ```

How does it do this? This is the core of the program:

![image|690x83](https://community.tmpdir.org/uploads/default/optimized/2X/b/bbe1ca67168def93d572c23ea453cfe241a84118_2_1035x124.png)
[read more ...](https://community.tmpdir.org/t/how-is-scc-so-fast/1231/1)

---

Thoughts, feedback? Let us know: ✉️ [info@tmpdir.org](mailto:info@tmpdir.org).

Join our 💬 [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR 📰
[here](https://newsletter.tmpdir.org/). Listen to previous podcasts at 🎙
[https://tmpdir.org/](https://community.tmpdir.org/).
