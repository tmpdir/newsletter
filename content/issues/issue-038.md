+++
title = "TMPDIR Weekly - #38, Sept 3rd, 2022"
date = 2022-09-03
+++

Welcome to the 38 issue of TMPDIR Weekly, a newsletter covering Embedded Linux,
IoT systems, and technology in general. Please pass it on to anyone else you
think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

The TMPDIR forum
[now has chat functionality](https://community.tmpdir.org/t/this-community-now-has-chat-functionality/590).
Check it out!

---

# Linux

# Yoe Releases

[2022.07](https://community.tmpdir.org/t/yoe-distribution-releases/239/15#h-202207-sakiz-1)
and
[2022.08](https://community.tmpdir.org/t/yoe-distribution-releases/239/16#h-202208-targhee-1)
releases of Yoe came along after some slowdown due to summer holidays and
travel. This release has updated Go compiler to 1.19 release and GCC to 12.2
which brings critical bugfixes. There was a boot time regression where rng-tools
was taking a long time, but this has been also fixed in 2022.08 release. Systemd
253.4 upgrade regresses when built with clang -- a workaround of using Os
instead of O2 optlevel fixes the issue and has been added to 2022.08.

## Glibc 2.36 upgrade

Glibc 2.36 upgrade is part of 2022.08 release as well. This initiated several
fixes where kernel UAPI `linux/mount.h` started to conflict with `sys/mount.h`.
This problem showed up in obscure places where `linux/mount.h` was being brought
in indirectly via other kernel UAPIs. This was
[documented](https://sourceware.org/glibc/wiki/Release/2.36#Usage_of_.3Clinux.2Fmount.h.3E_and_.3Csys.2Fmount.h.3E)
and later on there has been patches to Glibc to solve the situation.

## Using poky with Yoe

Yoe has switched to using Poky to provide core metadata and Bitbake now. This
should improve working closely with the Yocto community and CI integration with
Yocto autobuilder. The change is transparent to users and should not cause any
changes that users need to make.

---

# IoT

[v0.4.3](https://github.com/simpleiot/simpleiot/releases/tag/v0.4.3) release of
Simple IoT is out with
[serial MCU client](https://docs.simpleiot.org/docs/user/mcu.html) support (see
also [ref docs](https://docs.simpleiot.org/docs/ref/serial.html)). We also added
an origin field to the point type so we can track who made changes to a point.

We're currently working on
[switching the SIOT store to SQLite](https://github.com/simpleiot/simpleiot/milestone/9).
We're also rolling a number of other architecture changes into this release that
should greatly improve the experience of writing new SIOT clients.

## Use simple storage and wire data structures

We tend to create too many types which can cause our application to become
brittle and inflexible. Types stored on disk (database) are important, but as
soon as you go distributed and have communication between systems, types sent
over the wire are even more critical. Changing types is no longer a simple DB
migration, but now you have to coordinate multiple systems if a data-type sent
over the wire changes. Each additional type is a liability – it requires special
logic to handle, communicate, store, query, etc. Getting to the correct data
structures is one of the hard parts of programming and it sometimes takes a few
iterations. If things are getting simpler (at least for your users) as you go
along, that is usually a good sign.

Here is
[an example](https://github.com/pocketbase/pocketbase/discussions/225#discussioncomment-3536924)
of the process in the Pocketbase community.

Thoughts?
[Discuss here](https://community.tmpdir.org/t/use-simple-storage-and-wire-data-structures/620).

---

# Other

[Trunk Based Development](https://community.tmpdir.org/t/trunk-based-development/568)
is a good resource for optimizing your Git workflow. This happens to be pretty
much how we do things on most projects. An important concept is to make changes
on the main branch and cherry-pick them if needed to release branches -- not the
other way around.

Several good articles with thoughts about product business models:

- [Start with selling a Product not a Platform](https://community.tmpdir.org/t/start-with-selling-a-product-not-a-platform/567/3)
- [The Future of Open Source, or Why Open Core Is Dead](https://community.tmpdir.org/t/the-future-of-open-source-or-why-open-core-is-dead/564/2)

---

# Quote for the week

> Smart people don't always know everything but often don't realize it.
> Effective people acknowledge what they don't know and connect with those who
> do.

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
