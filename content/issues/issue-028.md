+++
title = "TMPDIR Weekly - #28, March 12, 2022"
date = 2022-03-12
+++

Welcome to the 28th issue of TMPDIR Weekly, a newsletter covering Embedded
Linux, IoT systems, and technology in general. Please pass it on to anyone else
you think might be interested and send any tips or feedback to our
[forum](https://community.tmpdir.org/) or news@tmpdir.org.

[Subscribe Here](https://tmpdir.ck.page/196d1fb480)

---

# Linux

## Yocto `kirkstone` release is coming along

OpenEmbedded Core has received timely update to support
[poetry](https://python-poetry.org/) which is another packaging and dependency
management system for python modules. Since the upcoming `kirkstone` release is
an LTS, it was a good to get this in along with PEP-517 packaging changes, which
has been vastly improved since it landing in OE-Core few weeks ago. Additionally
Powerpc32 and powerpc64 builds have been fixed. Double float support is not same
when it comes to musl vs glibc so this has also warranted a re-org of site files
which now ensures that autoconf caches are accurately feeding the size of long
doubles on musl.

## QT 5.15.3 in Yocto

meta-qt5 has
[updated](https://github.com/meta-qt5/meta-qt5/commit/74c27fa59b329f1210d39e8cd2904acf30d10838)
the recipes to recently opensourced 5.15.3-lts-lgpl release

## SPDX license naming convention

OpenEmbedded core has now moved to use SPDX naming conventions for LICENSE field
in recipes, several layers e.g. meta-openembedded, meta-clang, meta-riscv,
meta-qcom to name few have also made the change, this will enable further
tooling to consume this data for creating SBOMs (software BOMs) and license
manifests. There is a
[script](https://git.openembedded.org/openembedded-core/tree/scripts/contrib/convert-spdx-licenses.py)
to help convert the layers, the script can be easily run on top directory of a
layer

## [Tow-Boot - An opinionated u-boot distribution](https://github.com/Tow-Boot/Tow-Boot)

Having a standard boot environment is priceless and especially when it comes to
embedded world where the fragmentation is maximum, this project is trying to
standardize boot on SBCs and bridge the gaps and special considerations that
each board might have. This could be a great asset in Embedded space to have a
standard way to boot the system and load any OS on top of it could become very
trivial. There is a good initial list of SBCs already
[supported](https://tow-boot.org/devices/index.html) most of these are 64bit ARM
boards, hoping to see other architectures like RISCV64 there soon.

## Some [notes](https://community.tmpdir.org/t/how-to-use-dynamic-layers-in-yocto/446) on using Yocto dynamic layers

---

# Other

The MacroFab platform
[now supports](https://help.macrofab.com/knowledge/macrofab-platfrom-release-march-11-2022)
KiCad 6 files. MacroFab offers digital manufacturing services including PCB
assembly. Their platform can provide a real-time quote -- simply upload your
design files.

---

# Quote for the week

> What information consumes is rather obvious: it consumes the attention of its
> recipients. Hence a wealth of information creates a poverty of attention, and
> a need to allocate that attention efficiently among the overabundance of
> information sources that might consume it. -- Herbert Simon

---

Join our [Discourse forum](https://community.tmpdir.org/) to discuss these or
new topics. Find past issues of TMPDIR [here](https://newsletter.tmpdir.org/).
Listen to previous podcasts at
[https://tmpdir.org/](https://community.tmpdir.org/).

Thanks for reading!

Khem and Cliff
