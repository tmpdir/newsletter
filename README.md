# newsletter

Source code for the tmpdir newsletter

## Process to create a new newletter

1. copy the last newsletter in the `content` directory to a new file.
1. update with new content
1. `zola serve` (review newsletter in browser to make sure it looks good)
1. `zola build`
1. start a new newsletter in https://app.tinyletter.com
1. fill in title
1. switch to raw html view (`<>` toolbar option to the right)
1. copy body content of `public/<episode>/index.html` to the message box
1. click on `<>` toolbar button again and verify formatting
1. deploy newsletter to website using Ansible
